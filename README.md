# SingWork PHP hosting framework #
========

SingWork is framework in form of a shared library on our server. It provides all necessary functions and executions for website backend. Builded as a lite MVC architecture, with focus to allow fast creation of website without the need to worry about backend functions.
Library is not meant to be used for the general public. It heavily relies on specific Apache/Nginx (or any other http server) configuration.

Documentation is still a mess. Slowly working on phpDoc

Features
--------

- Ready-to-use with just simple index.php file
- Website configuration and management is done via DEEP CMS systems provided by our company
- Allows fast and scalable HTML Form builder and server-side form data verification via JSON file settings, all in one file for each form
- Protected website parts with user registration and management
- E-commerce plugin with full checkout process on-site
- (Soon) Multiple DB engines

Installation
------------




Support
-------

If you are having issues, please let me know at matej.smisek@gmail.com

License
-------

The project is licensed under the BSD license.