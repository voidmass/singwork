<?php
namespace Application;

use Singwork\Content\Controller;

use Application\Table as AppTable;
use Singwork\Table as Table;
/**
 * Central controller for global Layout, it's used to alter the appereance of Layout.phtml file
 *
 * @author Matej Smisek
 */
class LayoutController extends Controller {

    public function run() {
      
    }

}