<div id="questionForm" class="form">
<div class="line full">
        <label for="jmeno"><? $this->t->getWord('contact_name'); ?></label>
        <br />
        <input id="jmeno" type="text" name="jmeno" class="text save required" />
        <br />
      </div>
      <div class="line full">
        <label for="prijmeni"><? $this->t->getWord('contact_surname'); ?></label>
        <br />
        <input id="prijmeni" type="text" name="prijmeni" class="text save required" />
        <br />
      </div>
      <div class="line full">    
        <label for="email"><? $this->t->getWord('contact_email'); ?></label>
        <br />
        <input id="email" type="text" name="email"  class="text save required" />
        <br />
      </div> 
      <div class="line full"> 
        <label for="dotaz"><? $this->t->getWord('contact_email'); ?></label><br />  
        <textarea id="dotaz" name="dotaz" rows="5" cols="40" class="text save required" ></textarea>
        <br />
      </div>
    <div class="line full"> 
      <input id="odeslatDotaz" class="submit" type="submit" name="submit" value="Odeslat dotaz" />
    </div>
      <div class="result">
        <span class="confirm"><? $this->t->getWord('contact_spam'); ?></span>
  </div>
</div>