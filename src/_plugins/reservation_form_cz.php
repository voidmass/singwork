<div id="reservationForm" class="form">
    <div class="line full">
        <label for="name">Vaše celé jméno:</label>
        <br />
        <input id="name" type="text" name="name" class="text save required" />
        <br />
      </div>
      <div class="line full">
        <label for="phone">Telefon:</label>
        <br />
        <input id="phone" type="text" name="phone" class="text save required" />
        <br />
      </div>
      <div class="line full">    
        <label for="email">E-mail:</label>
        <br />
        <input id="email" type="text" name="email"  class="text save required" />
        <br />
      </div> 
    <div class="line full">    
        <label for="self">Zájezd si zařídím sám</label>
        <input id="self" type="radio" name="trip"  class="radio save" checked />
        <label for="company">Chci ho mít zprostředkován</label>
        <input id="company" type="radio" name="trip"  class="radio save" />
        
    </div>
    <div class="line full">
        <div class="box">
            <span><i>Upozornění: zájezdy lze realizovat pouze z území České republiky</i></span>
        </div>
    </div>
    <div class="line half">    
        <label for="datum_start">Datum příjezdu:</label>
        <br />
        <input id="date_start" type="text" name="datum_start"  class="text save required datepicker" />
        <br />
    </div>
    <div class="line half">    
        <label for="datum_end">Datum odjezdu:</label>
        <br />
        <input id="date_end" type="text" name="datum_end"  class="text save required datepicker" />
        <br />
    </div>
        <div class="line full">    
        <label for="course">Vybraný kurz:</label>
        <br />
        <select id="course" type="text" name="course"  class="combo save required">
            <option value="0">Vyberte kurz</option>
            <?php 
                $result = mysql_query('SELECT `pricelist`.`id`,`pricelist`.`title`,`pricelist`.`price`,`pricelist_category`.`name`  FROM `pricelist` JOIN `pricelist_category` ON(`pricelist`.`category` = `pricelist_category`.`id`) WHERE `pricelist_category`.`group` = "kurzy_kitesurf" ORDER BY `pricelist_category`.`order` ASC,`pricelist`.`order` ASC;');
                while ($row = mysql_fetch_array($result)) {
                    echo '<option value="'.$row['id'].'">Kitesurfing - '.$row['name'].' - '.$row['title'].' ('.$row['price'].')</option>';
                }
                $result = mysql_query('SELECT `pricelist`.`id`,`pricelist`.`title`,`pricelist`.`price`,`pricelist_category`.`name`  FROM `pricelist` JOIN `pricelist_category` ON(`pricelist`.`category` = `pricelist_category`.`id`) WHERE `pricelist_category`.`group` = "kurzy_windsurf" ORDER BY `pricelist_category`.`order` ASC,`pricelist`.`order` ASC;');
                while ($row = mysql_fetch_array($result)) {
                    echo '<option value="'.$row['id'].'">Windsurfing - '.$row['name'].' - '.$row['title'].' ('.$row['price'].')</option>';
                }
                
                $result = mysql_query('SELECT `pricelist`.`id`,`pricelist`.`title`,`pricelist`.`price`,`pricelist_category`.`name`  FROM `pricelist` JOIN `pricelist_category` ON(`pricelist`.`category` = `pricelist_category`.`id`) WHERE `pricelist_category`.`group` = "pujceni_kitesurf" ORDER BY `pricelist_category`.`order` ASC,`pricelist`.`order` ASC;');
                while ($row = mysql_fetch_array($result)) {
                    echo '<option value="'.$row['id'].'">Kitesurfing '.$row['name'].' - '.$row['title'].' ('.$row['price'].')</option>';
                }
                $result = mysql_query('SELECT `pricelist`.`id`,`pricelist`.`title`,`pricelist`.`price`,`pricelist_category`.`name`  FROM `pricelist` JOIN `pricelist_category` ON(`pricelist`.`category` = `pricelist_category`.`id`) WHERE `pricelist_category`.`group` = "pujceni_windsurf" ORDER BY `pricelist_category`.`order` ASC,`pricelist`.`order` ASC;');
                while ($row = mysql_fetch_array($result)) {
                    echo '<option value="'.$row['id'].'">Windsurfing '.$row['name'].' - '.$row['title'].' ('.$row['price'].')</option>';
                }
                
                $result = mysql_query('SELECT `pricelist`.`id`,`pricelist`.`title`,`pricelist`.`price`,`pricelist_category`.`name`  FROM `pricelist` JOIN `pricelist_category` ON(`pricelist`.`category` = `pricelist_category`.`id`) WHERE `pricelist_category`.`group` = "uschovna" ORDER BY `pricelist_category`.`order` ASC,`pricelist`.`order` ASC;');
                while ($row = mysql_fetch_array($result)) {
                    echo '<option value="'.$row['id'].'">'.$row['name'].' - '.$row['title'].' ('.$row['price'].')</option>';
                }
                $result = mysql_query('SELECT `pricelist`.`id`,`pricelist`.`title`,`pricelist`.`price`,`pricelist_category`.`name`  FROM `pricelist` JOIN `pricelist_category` ON(`pricelist`.`category` = `pricelist_category`.`id`) WHERE `pricelist_category`.`group` = "special" ORDER BY `pricelist_category`.`order` ASC,`pricelist`.`order` ASC;');
                while ($row = mysql_fetch_array($result)) {
                    echo '<option value="'.$row['id'].'">'.$row['name'].' - '.$row['title'].' ('.$row['price'].')</option>';
                }
            ?>
        </select>
        <br />
    </div>
    <div class="line full"> 
        <label for="note">Poznámka:</label><br />  
        <textarea id="note" name="note" rows="5" cols="40" class="save" ></textarea>
        <br />
    </div>
    <div class="line full"> 
      <input id="send Rezervation" class="submit" type="submit" name="submit" value="Odeslat dotaz" />
      </div>
      <div class="result">
        <span class="confirm"></span>
      </div>
</div>