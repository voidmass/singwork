    <!-- Just some heading -->
<div style='font-size: 16px; font-weight: bold; margin: 0 0 10px 0;'>
    This album is synchronized with this
    <a href='https://www.facebook.com/pages/COAN-Dummy-Page/221167777906963?sk=photos'>
        Dummy Page Album
    </a>
</div>
<?php
//include the fb php sdk
require '_conf/sdk/facebook.php';

$facebook = new Facebook(array(
    'appId'  => '347426998707432',
    'secret' => '71381216c802dc182da23e113640571e',
    'cookie' => true, // enable optional cookie support
));
    
//defining action index
isset( $_REQUEST['action'] ) ? $action = $_REQUEST['action'] : $action = "";

/*
 * This will show 
 */
if( $action == ''){
    //echo "<p>COAN Dummy Page Albums</p>";
    
    // select albums from our dummy page
    $fql    =   "SELECT 
                    aid, cover_pid, name 
                FROM 
                    album 
                WHERE owner=221167777906963";
                
    $param  =   array(
        'method'    => 'fql.query',
        'query'     => $fql,
        'callback'  => ''
    );
    
    $fqlResult   =   $facebook->api($param);
    
    foreach( $fqlResult as $keys => $values ){

        //to get album cover
        $fql2    =   "select src from photo where pid = '" . $values['cover_pid'] . "'";
        $param2  =   array(
            'method'    => 'fql.query',
            'query'     => $fql2,
            'callback'  => ''
        );
        $fqlResult2   =   $facebook->api($param2);
        
        foreach( $fqlResult2 as $keys2 => $values2){
            $album_cover = $values2['src'];
        }
        
        //show the album
        echo "<div class='fbAlbum'>";
            echo "<a href='index.php?action=list_pics&aid=" . $values['aid'] . "&name=" . $values['name'] . "'>";
                echo "<img src='$album_cover' border='1'>";
            echo "</a>";
            echo "<div>{$values['name']}</div>";
        echo "</div>";
        
    }
}

/*
 * This will show the photo(s) on the clicked album.
 */
if( $action == 'list_pics'){

    isset( $_GET['name'] ) ? $album_name = $_GET['name'] : $album_name = "";
    
    echo "<div><a href='index.php'>Back To Albums</a> | Album Name: <b>" . $album_name . "</b></div>";
    
    // query all the images in the album
    $fql = "SELECT 
                pid, src, src_small, src_big, caption 
            FROM 
                photo 
            WHERE 
                aid = '" . $_REQUEST['aid'] ."' ORDER BY created DESC";
    
    $param  =   array(
        'method'    => 'fql.query',
        'query'     => $fql,
        'callback'  => ''
    );
    
    $fqlResult   =   $facebook->api($param);
    
    echo "<div id='gallery'>";
    
    foreach( $fqlResult as $keys => $values ){
        
        if( $values['caption'] == '' ){
            $caption = "";
        }else{
            $caption = $values['caption'];
        }   
        
        echo "<div class='fbAlbumImage'>";
            echo "<a href=\"" . $values['src_big'] . "\" title=\"" . $caption . "\">";
                echo "<img src='" . $values['src'] . "' />";
            echo "</a>"; 
        echo "</div>";
    }
    
    echo "</div>";
}
?>
    
    
<!-- activate jQuery lightbox -->
<script type="text/javascript" src="jQuery-lightbox/js/jquery.js"></script>
<script type="text/javascript" src="jQuery-lightbox/js/jquery.lightbox-0.5.js"></script>
<link rel="stylesheet" type="text/css" href="jQuery-lightbox/css/jquery.lightbox-0.5.css" media="screen" />

<script type="text/javascript">
$(function() {
    $('#gallery a').lightBox();
});
</script>
