<div id="reservationForm" class="form">
    <div class="line full">
        <label for="name">Your full name:</label>
        <br />
        <input id="name" type="text" name="name" class="text save required" />
        <?php printHelper('reservation', 1, $GLOBALS['page']['lang']) ?>
        <br />
    </div>
    <div class="line full">
        <label for="phone">Cellphone number:</label>
        <br />
        <input id="phone" type="text" name="phone" class="text save required" />
        <?php printHelper('reservation', 2, $GLOBALS['page']['lang']) ?>
        <br />
    </div>
    <div class="line full">    
        <label for="email">E-mail:</label>
        <br />
        <input id="email" type="text" name="email"  class="text save required" />
        <?php printHelper('reservation', 3, $GLOBALS['page']['lang']) ?>
        <br />
    </div> 
    <div class="line full panel">    
        <label for="self">I'll get trip on my own</label>
        <input id="self" type="radio" name="trip"  class="radio save" checked />
        <label for="company">I want you to manage my trip</label>
        <input id="company" type="radio" name="trip"  class="radio save" />
        <?php printHelper('reservation', 4, $GLOBALS['page']['lang']) ?>
    </div>
    <div class="line full">
        <div class="box">
            <span class="warning_text">Notice! Trips can be managed only from Czech republic</span>
        </div>
    </div>
    <div class="line half">    
        <label for="datum_start">Date of arrival:</label>
        <br />
        <input id="date_start" type="text" name="datum_start"  class="text save required datepicker" />
        <br />
    </div>
    <div class="line half">    
        <label for="datum_end">Date of departure:</label>
        <br />
        <input id="date_end" type="text" name="datum_end"  class="text save required datepicker" />
        <?php printHelper('reservation', 5, $GLOBALS['page']['lang']) ?>
        <br />
    </div>
    <div class="line full">    
        <label for="course">I am interested in</label>
        <br />
        <select id="course" type="text" name="course"  class="combo save required">
            <option value="0">Choose ...</option>
            <?php
            $result = mysql_query('SELECT `pricelist`.`id`,`pricelist`.`title`,`pricelist`.`price`,`pricelist_category`.`name`  FROM `pricelist` JOIN `pricelist_category` ON(`pricelist`.`category` = `pricelist_category`.`id`) WHERE `pricelist_category`.`group` = "kurzy_kitesurf" ORDER BY `pricelist_category`.`order` ASC,`pricelist`.`order` ASC;');
            while ($row = mysql_fetch_array($result)) {
                echo '<option value="' . $row['id'] . '">Kitesurfing - ' . $row['name'] . ' - ' . $row['title'] . ' (' . $row['price'] . ')</option>';
            }
            $result = mysql_query('SELECT `pricelist`.`id`,`pricelist`.`title`,`pricelist`.`price`,`pricelist_category`.`name`  FROM `pricelist` JOIN `pricelist_category` ON(`pricelist`.`category` = `pricelist_category`.`id`) WHERE `pricelist_category`.`group` = "kurzy_windsurf" ORDER BY `pricelist_category`.`order` ASC,`pricelist`.`order` ASC;');
            while ($row = mysql_fetch_array($result)) {
                echo '<option value="' . $row['id'] . '">Windsurfing - ' . $row['name'] . ' - ' . $row['title'] . ' (' . $row['price'] . ')</option>';
            }
            ?>
        </select>
        <br />
    </div>
    <div class="line full"> 
        <label for="note">Note:</label><br />  
        <textarea id="note" name="note" rows="5" cols="40" class="save" ></textarea>
        <br />
    </div>
    <div class="line full"> 
        <input id="send Rezervation" class="submit" type="submit" name="submit" value="Odeslat dotaz" />
    </div>
    <div class="result">
        <span class="confirm"></span>
    </div>
</div>