<?php
define('SINGWORK_PATH', $singwork_path);
include_once $singwork_path.'/functions.php';
/*
 * Standalone PSR-4 autoloader
 * include_once 'Psr4Autoloader.php';

$autoloader = new Psr4Autoloader();
$autoloader->register();
$autoloader->addNamespace('Singwork', $singwork_path.'/Singwork');


$autoloader->addNamespace('Application', PUBLIC_PATH.'/_application');
*/

$loader = require __DIR__ . '/vendor/autoload.php';
$loader->addPsr4('Singwork\\', $singwork_path.'/Singwork');
$loader->addPsr4('Application\\', PUBLIC_PATH.'/Application');

$core = \Singwork\Core::getInstance();
$core->setPublicPath(PUBLIC_PATH);
$core->setFrameworkPath($singwork_path);

if (php_sapi_name() == "cli") {
    $core->cliRun($cli_command, $cli_args);
} else {   
    $core->run();
}