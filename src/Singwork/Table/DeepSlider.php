<?php 
namespace Singwork\Table;

use Singwork\Database\Table;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DeepSlider
 *
 * @author Matej Smisek
 */
class DeepSlider  extends Table {
    
    
    public function __construct() {
        $this->_table = 'slider';
        
    }
    public function getId($id) {
        $r = $this->get('id', $id);
        return $r->fetch();
    }
    public function getSlider($lang) {
        $r = $this->simpleSelect('`active` = 1 AND `lang` LIKE "'.$lang.'"');
        return $r;
    }
}

?>