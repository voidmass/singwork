<?php 
namespace Singwork\Table;

use Singwork\Database\Table;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GalleryImages
 *
 * @author Matej Smisek
 */
class GalleryImages extends Table {
    
    
    public function __construct() {
        $this->_table = 'sw_gallery_images';        
    }
    
    public function getId($id) {
        $r = $this->get('id', $id);
        return $r->fetch();
    }
    /**
     * 
     * @param type $id
     * @return SelectResult
     */
    public function getGallery($id) {
        return $this->simpleSelect('`gallery_id` LIKE "'.$id.'" ORDER BY `order` ASC');              
    }
    
}

?>