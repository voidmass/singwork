<?php 
namespace Singwork\Table;

use Singwork\Database\Table;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SingworkIdSequence
 *
 * @author Matej Smisek
 */
class SingworkIdSequence extends Table {
    
    
    public function __construct() {
        parent::__construct('main');
        $this->_table = 'singwork_id_sequence';        
    }
    
    public function getId($id) {
        return $this->get('id', $id)->fetch();         
    }
    public function deleteRow($id) {
        return $this->delete('id', $id);
    }
    
}
