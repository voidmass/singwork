<?php 
namespace Singwork\Table\User;

use Singwork\Database\Table;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LemonadaUserSession
 *
 * @author Matej Smisek
 */
class UserSession extends Table {
    
    
    public function __construct() {
        parent::__construct('main');
        $this->_table = 'singwork_user_session';        
    }
    
    public function getId($user_id, $session_id) {
        $r = $this->simpleSelect('`user_id` = "'.$user_id.'" AND `session_id` = "'.$session_id.'"');
        return $r->fetch();
    }
    public function deleteRow($user_id, $session_id) {
        return $this->delete(['user_id' => $user_id, 'session_id' => $session_id]);
    }
    public function insertRow($user_id, $session_id, $auth_key, $agent) {
        return $this->insert(['user_id', 'session_id', 'auth_key', 'agent', 'creation','last_update','ip'], [[$user_id, $session_id, $auth_key, $agent,date('Y-m-d H:i:s',time()),date('Y-m-d H:i:s',time()),$_SERVER['REMOTE_ADDR']]]);
    }
    public function setOnline($user_id, $session_id) {
        return $this->updateMulti(['online' => '1', 'last_update' => date('Y-m-d H:i:s',time())], '`user_id` = "'.$user_id.'" AND `session_id` = "'.$session_id.'"');
    }
    
}
