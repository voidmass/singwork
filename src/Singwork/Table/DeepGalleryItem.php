<?php 
namespace Singwork\Table;

use Singwork\Database\Table;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DeepGalleryItem
 *
 * @author Matej Smisek
 */
class DeepGalleryItem extends Table {
    
    
    public function __construct($connection = 'main') {
        parent::__construct($connection);
        $this->_table = 'deep_gallery_item';        
    }
    
    public function getId($id) {
        return $this->get('id', $id)->fetch();
    }
    public function deleteRow($id) {
        return $this->delete('id', $id);
    }
    public function getGallery($gallery_id) {
        return $this->get('gallery_id',$gallery_id);
    }
    
}
