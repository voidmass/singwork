<?php 
namespace Singwork\Table;

use Singwork\Database\Table;
/**
 * Description of Settings
 *
 * @author Matej Smisek
 */
class Settings extends Table {
        
    public function __construct($connection = 'main') {
        parent::__construct($connection);
        $this->_table = 'singwork_settings';        
    }    
    public function getId($id) {
        $r = $this->get('id', $id);
        return $r->fetch();
    }
}

?>
