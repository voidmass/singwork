<?php 
namespace Singwork\Table;

use Singwork\Database\Table;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Gallery
 *
 * @author Matej Smisek
 */
class Gallery extends Table {
    
    
    public function __construct() {
        $this->_table = 'sw_gallery';        
    }
    
    public function getId($id) {
        $r = $this->get('id', $id);
        return $r->fetch();
    }
    public function getIdentifier($id) {
        $r = $this->get('identifier', $id);
        return $r->fetch();
    }
    
}

?>