<?php

namespace Singwork\Database;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\DriverChain;
use Doctrine\ORM\Configuration;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ManagerContainer
 *
 * @author Balgor
 */
class ManagerContainer
{

    /**
     *
     * @var EntityManager[]
     */
    protected $_managers;

    /**
     * @var EntityManager
     */
    protected $_defaultManager = null;

    public function __construct()
    {
        foreach ($_ENV['config']['database'] as $identifier => $connection) {
            $manager = $this->createEntityManager($connection);
            if ($this->_defaultManager === null) {
                $this->_defaultManager = $manager;
            }
            $this->_managers[$identifier] = $manager;
        }
    }

    /**
     * 
     * @param type $config_data
     * @return null
     */
    protected function createEntityManager($config_data)
    {
        if (getEnv('APPLICATION_MODE') == "development") {
            $cache = new \Doctrine\Common\Cache\ArrayCache;
        } else {
            $cache = new \Doctrine\Common\Cache\ArrayCache;
//            $cache = new \Doctrine\Common\Cache\ApcCache;
        }
        $config = new Configuration;
        $config->setMetadataCacheImpl($cache);

        $chain = new DriverChain();
        $chain->addDriver($config->newDefaultAnnotationDriver(SINGWORK_PATH . '/Singwork/Model/Entities'), 'Singwork\Model\Entities');
        $chain->addDriver($config->newDefaultAnnotationDriver(PUBLIC_PATH . '/Application/Model/Entities'), 'Application\Model\Entities');
        $config->setMetadataDriverImpl($chain);
        $config->setQueryCacheImpl($cache);
        $config->setProxyDir(PUBLIC_PATH . '/Application/Model/Proxies');
        $config->setProxyNamespace('Application\Model\Proxies');

        $config->setEntityNamespaces([
            'Singwork' => '\Singwork\Model\Entities',
            'App' => '\Application\Model\Entities',
            'Base' => '\Singwork\Model\Entities',
            'Application' => '\Application\Model\Entities',
            'Shop' => '\Singwork\Model\Entities\Shop'
        ]);

        if (getEnv('APPLICATION_MODE') == "development") {
            $config->setAutoGenerateProxyClasses(true);
        } else {
            $config->setAutoGenerateProxyClasses(true); //false
        }

        $config_data['charset'] = 'utf8';

        return EntityManager::create($config_data, $config);
    }

    /**
     * 
     * @param string $identifier
     * @return EntityManager
     * @throws \RuntimeException
     */
    public function getManager(string $identifier = null): EntityManager
    {
        if ($identifier === null) {
            return $this->_defaultManager;
        }
        if (array_key_exists($identifier, $this->_managers)) {
            return $this->_managers[$identifier];
        }
        throw new \RuntimeException('No such manager defined');
    }

    public function selectDatabase(string $database, string $identifier = null)
    {
        if (preg_match("#[0-9a-zA-Z_]+#", $database)) {
            return $this->getManager($identifier)->getConnection()->executeQuery('USE ' . $database . ';');
        }
    }

}
