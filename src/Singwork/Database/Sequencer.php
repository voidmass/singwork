<?php

namespace Singwork\Database;

use Singwork\Exception\RuntimeSingException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sequencer
 *
 * @author Matej Smisek
 */
class Sequencer implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * 
     * @param \Singwork\Model\Entities\Sequence $sequence 
     * @return type
     */
    protected function constructId($sequence)
    {
        $final_id = $sequence->getFormat();
        self::injectDate($final_id);
        $count = substr_count($final_id, '#') - strlen($sequence->getCurrentId());
        $string_id = '';
        for ($i = 0; $i < $count; $i++) {
            $string_id .= '0';
        }
        $string_id .= ((string) $sequence->getCurrentId());
        $final_id = preg_replace('/#+/', $string_id, $final_id);
        return $final_id;
    }

    protected static function injectDate(&$string)
    {
        $string = preg_replace('/YYYY/', date('Y'), $string);
        $string = preg_replace('/YY/', date('y'), $string);
        $string = preg_replace('/MM/', date('m'), $string);
    }

    public function requestId($sequence_id)
    {
        $sequence = $this->container->get('manager_container')->getManager(null)->getRepository('Base:Sequence')->find($sequence_id);
        if ($sequence === null) {
            throw new RuntimeSingException('Sequence ' . $sequence_id . ' not defined');
        }
        $sequence->setCurrentId($sequence->getCurrentId() + 1);   
        $this->container->get('manager_container')->getManager(null)->flush();
        return $this->constructId($sequence);
    }

}
