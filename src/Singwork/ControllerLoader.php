<?php

namespace Singwork;

use Singwork\Table;
use Singwork\Content\DynamicController;
use Singwork\Exception\SetupException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControllerLoader
 *
 * @author Matej Smisek
 */
class ControllerLoader implements ControllerResolverInterface, ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     *
     * @var ContainerInterface
     */
    protected $container;

    public function __construct()
    {
        
    }

    public function load($page)
    {
        if ($page->getType() == "dynamic_page") {
            $table = new Table\DynamicPage();
            $dynamic_content = $table->getId($page->getValue('dynamic_id'));
            if (!$dynamic_content) {
                throw new \Exception("Dynamic page content missing", "401", null);
            }
            core()->setController(new DynamicController($dynamic_content));
            core()->getController()->_view->setPage('DynamicView.phtml');
        } else {
            $class = '\Application\Controller\\' . $page->getValue('controller_class') . 'Controller';
            $this->checkIfControllerExists($class);
            core()->setController(new $class());
            if (!core()->getController() instanceof \Singwork\Content\Controller) {
                throw new SetupException('Not a Controller');
            }
            core()->getController()->_view->setPage($page->getValue('view_path'));
        }
        core()->getController()->param = & core()->getParams();
        core()->getController()->_param = & core()->getParams();
    }

    protected function checkIfControllerExists($controller)
    {
//        if (!file_exists(core()->getWebsitePath() . '/_controller/' . $controller . '.php') && !file_exists(core()->getFrameworkPath() . '/_default/' . $controller . '.php')) {
        if (!class_exists($controller)) {
            throw new SetupException('Controller ' . $controller . ' does not exists. Path: ' . core()->getWebsitePath() . '_controller/' . $controller . '.php', 500);
        }
    }

    public function getArguments(Request $request, $controller): array
    {
        
    }

    public function getController(Request $request)
    {
        if (!$controller = $request->attributes->get('_controller')) {
            if (null !== $this->logger) {
                $this->logger->warning('Unable to look for the controller as the "_controller" parameter is missing.');
            }
            return false;
        }
        if (is_callable($controller)) {
            $callable = $controller;
        } else {
            $callable = $this->createController($controller);
        }
        if (!is_callable($callable)) {
            throw new \InvalidArgumentException(sprintf('The controller for URI "%s" is not callable. %s', $request->getPathInfo(), ''));
        }

        return $callable;
    }

    protected function createController($controller)
    {
        if (false === strpos($controller, '::')) {
            throw new \InvalidArgumentException(sprintf('Unable to find controller "%s".', $controller));
        }

        list($class, $method) = explode('::', $controller, 2);
        $class = '\Application\Controller\\' . $class . 'Controller';
        if (!class_exists($class)) {
            return false;
        }
        $object = new $class();
        if (!$object instanceof \Singwork\Content\Controller) {
            return false;
        }
        if ($object instanceof ContainerAwareInterface) {
            $object->setContainer($this->container);
        }
        return [$object, $method];
    }

}
