<?php

namespace Singwork;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Attribute\NamespacedAttributeBag;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Singwork\Util\Util;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RequestProcessor
 *
 * @author Matej Smisek
 */
class RequestProcessor implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     *
     * @var EventDispatcher
     */
    protected $_dispatcher;

    /**
     * @param ContainerBuilder $container
     * @param EventDispatcher $dispatcher
     */
    public function __construct(EventDispatcher $dispatcher)
    {
//        var_dump($container->has('twig'));
        $this->_dispatcher = $dispatcher;
//        $this->container = $container;
    }
    protected function prepareSession(Request $request) {
        $storage = new NativeSessionStorage([
            'cookie_lifetime' => Util::decodeTime($_ENV['config']['session']['lifetime']) 
        ]);
        $storage->setName($_ENV['config']['session']['name']);
        $session = new Session($storage);
        $bag = new \Symfony\Component\HttpFoundation\Session\Attribute\NamespacedAttributeBag('application');
        $bag->setName('application');
        $session->registerBag($bag);
        $session->start();
        $request->setSession($session);
    }
    public function process()
    {
        $request = Request::createFromGlobals();
        
        $this->prepareSession($request);


        $this->container->set('request', $request);

        $this->container->get('application')->start();

        $this->container->get('user_handler')->initialize($request);



        $this->_dispatcher->addSubscriber(new RouterListener($this->container->get('router'), new RequestStack()));
        $this->_dispatcher->addSubscriber(new KernelListener($this->container));
        $this->_dispatcher->addListener(KernelEvents::REQUEST, [$this, 'onKernelRequest']);

        $controllerLoader = new ControllerLoader();
        $controllerLoader->setContainer($this->container);
        $argumentResolver = new ArgumentResolver();

        $kernel = new HttpKernel($this->_dispatcher, $controllerLoader, new RequestStack(), $argumentResolver);

        $response = $kernel->handle($request);

        $this->insertCookiesIntoResponse($response);

        $response->send();

        $kernel->terminate($request, $response);
    }

    protected function insertCookiesIntoResponse(Response $response)
    {
        foreach ($this->container->get('cookies')->getSetCookies() as $name => $data) {
            if ($data === false) {
                $response->headers->clearCookie($name);
            } elseif (is_array($data)) {
               
                $response->headers->setCookie(new \Symfony\Component\HttpFoundation\Cookie($name, $data['content'], $data['expiration']));
                
            } else {
                $response->headers->setCookie(new \Symfony\Component\HttpFoundation\Cookie($name, $data));
            }
        }
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        
        if (!$this->container->get('user_handler')->verifyAccess($event->getRequest()->attributes)) {
            if ($event->getRequest()->request->get('method') == 'ajax') {
                $event->setResponse(new \Symfony\Component\HttpFoundation\JsonResponse('Restricted access', Response::HTTP_FORBIDDEN));
            } else {
                $event->setResponse(new RedirectResponse($this->container->get('router')->generate($_ENV['config']['security']['fallback_route']), Response::HTTP_FOUND));
            }
//            echo 'REDIRECT TO '.$this->container->get('router')->generate($_ENV['config']['security']['fallback_route']).' FROM KERNEL';
        }
    }

}
