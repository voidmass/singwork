<?php
namespace Singwork\Exception;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DbException
 *
 * @author Matej Smisek
 */
abstract class DbException extends SingException {
    
    protected $_db_version;
    protected $_connection;            
    
    public function __construct($message, $code, $previous, $connection = 'main') {
        parent::__construct($message, $code, $previous);
        $this->_connection = core()->getConnection($connection);
        $this->_db_version = $this->_connection->getVersion();
    }
    public function getVersion() {
        return $this->_db_version;
    }
    public function getFormattedMessage() {
        parent::getFormattedMessage();
        $this->formattedMsg['Connection'] = $this->_connection->getName();
        $this->formattedMsg['Database Ver.'] = $this->_db_version;
        return $this->formattedMsg;
    }
}

?>
