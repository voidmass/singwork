<?php
namespace Singwork\Exception;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QueryException
 *
 * @author Matej Smisek
 */
class QueryException extends DbException {
    
    protected $_query;
    /**
     * 
     * @param String $message
     * @param Integer $code
     * @param String $query
     * @param Expcetion $previous
     * @param String $connection
     */
    public function __construct($message,$code, $query, $previous, $connection = 'main') {
        parent::__construct($message, $code, $previous, $connection);
        $this->_query = $query;
    }    
    public function getFormattedMessage() {
        parent::getFormattedMessage();
        $this->formattedMsg['Query'] = $this->_query;
        return $this->formattedMsg;
    }
}

?>