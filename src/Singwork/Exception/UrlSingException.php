<?php
namespace Singwork\Exception;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UrSinglException
 *
 * @author Matej Smisek
 */
class UrlSingException extends SingException {
    
    public function __construct($message, $code, $previous) {
        parent::__construct($message, $code, $previous);
    }
}
