<?php
namespace Singwork\Exception;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SingException
 *
 * @author Matej Smisek
 */
abstract class SingException extends \Exception {
    protected $formattedMsg;
    public function __construct($message, $code, $previous) {
        parent::__construct($message, $code, $previous);
        
    }
    public function getFormattedMessage() {
        $this->formattedMsg = array();
        $this->formattedMsg['Message'] = $this->message;
        $this->formattedMsg['Code'] = $this->code;
        $this->formattedMsg['File'] = $this->file;
        return $this->formattedMsg;
    }
}

?>
