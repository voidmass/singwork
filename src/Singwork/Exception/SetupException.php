<?php
namespace Singwork\Exception;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of QueryException
 *
 * @author Matej Smisek
 */
class SetupException extends SingException {
    
    protected $_query;
    /**
     * 
     * @param String $message
     * @param Integer $code
     * @param Expcetion $previous
     * @param String $connection
     */
    public function __construct($message,$code = 500, $previous = null) {
        parent::__construct($message, $code, $previous);
    }    
    public function getFormattedMessage() {
        return parent::getFormattedMessage();
    }
}

?>
