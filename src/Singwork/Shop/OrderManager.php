<?php

namespace Singwork\Shop;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Singwork\Traits\EntityManagerAwareTrait;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Singwork\Model\Entities\Cart\Order;
use Singwork\Model\Entities\Cart\Cart;
use Singwork\Model\Entities\Cart\CartLog;
use Singwork\Event\OrderNextStateEvent;
use Singwork\Event\OrderCancelStateEvent;
use Doctrine\Common\Collections\Criteria;

/**
 * Description of OrderManager
 *
 * @author matej.smisek
 */
class OrderManager implements ContainerAwareInterface
{

    use ContainerAwareTrait;
    use EntityManagerAwareTrait;

    /**
     * @var EventDispatcher
     */
    protected $eventDispatcher;

    public function __construct(EventDispatcher $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function advanceOrder(Order $order, $sendNotification = true)
    {
        $this->container->get('order_manager')->saveState($order->getCart());
        switch ($order->getState()) {
            case Order::PLACED:
                if ($order->getCart()->getPayment()->getType() === 'wire') {
                    $state = Order::PAYMENT_PENDING;
                } elseif ($order->getCart()->getDelivery()->getType() === 'pickup') {
                    $state = Order::PICKUP;
                } else {
                    $state = Order::PROCESSED;
                }
                break;
            case Order::PAYMENT_PENDING:
            case Order::PROCESSED:
                switch ($order->getCart()->getDelivery()->getType()) {
                    case 'pickup':
                        $state = Order::PICKUP;
                        break;
                    case 'mail':
                    case 'dpd_pickup':
                    default:
                        $state = Order::SENT;
                        break;
                }
                break;
            case Order::SENT:
            case Order::PICKUP:
            case Order::COMPLETED:
                $state = Order::COMPLETED;
                break;


            default:
                throw new \Singwork\Exception\RuntimeSingException('Unknown state of order');
        }

        $event = new OrderNextStateEvent($order, $order->getState(), $state, $sendNotification);
        $state = $this->eventDispatcher->dispatch(OrderNextStateEvent::NAME . '_generic', $event)->getNextState();
        $state = $this->eventDispatcher->dispatch(OrderNextStateEvent::NAME . $state, $event)->getNextState();
        $order->setState($state);
        $this->em()->flush();
        $this->container->get('order_manager')->logCartAction($order->getCart(), 'order_' . $state);
        return $state;
    }

    public function setOrderStatus($state, Order $order, $sendNotification = true)
    {
        $this->container->get('order_manager')->saveState($order->getCart());

        $event = new OrderNextStateEvent($order, $order->getState(), $state, $sendNotification);
        $state = $this->eventDispatcher->dispatch(OrderNextStateEvent::NAME . '_generic', $event)->getNextState();
        $state = $this->eventDispatcher->dispatch(OrderNextStateEvent::NAME . $state, $event)->getNextState();
        $order->setState($state);
        $this->em()->flush();
        $this->container->get('order_manager')->logCartAction($order->getCart(), 'order_' . $state);
        return $state;
    }

    public function sendOrderNotification(Order $order)
    {
        $event = new OrderNextStateEvent($order, $order->getState(), null, true);
        $this->eventDispatcher->dispatch(OrderNextStateEvent::NAME . '_generic', $event)->getNextState();
        $this->eventDispatcher->dispatch(OrderNextStateEvent::NAME . $order->getState(), $event)->getNextState();
    }

    public function cancelOrder(Order $order)
    {
        $this->container->get('order_manager')->saveState($order->getCart());
        switch ($order->getState()) {
            case Order::CANCELED:
            case Order::EXPIRED:
                return $order->getState();
            case Order::PICKUP:
            case Order::SENT:
            case Order::PAYMENT_PENDING:

                $state = Order::EXPIRED;
                break;
            case Order::PLACED:
            case Order::PROCESSED:
            default:
                $state = Order::CANCELED;
                break;
        }
        $event = new OrderCancelStateEvent($order, $order->getState(), $state);
        $state = $this->eventDispatcher->dispatch(OrderCancelStateEvent::NAME . '_generic', $event)->getNextState();
        $state = $this->eventDispatcher->dispatch(OrderCancelStateEvent::NAME . $state, $event)->getNextState();
        $order->setState($state);
        $this->em()->flush();
        $this->container->get('order_manager')->logCartAction($order->getCart(), 'order_' . $state);
        return $state;
    }

    public function placeOrder(Order $order)
    {
        $event = new OrderNextStateEvent($order, 'none', Order::PLACED);
        $this->eventDispatcher->dispatch(OrderNextStateEvent::NAME . '_generic', $event);
        $this->eventDispatcher->dispatch(OrderNextStateEvent::NAME . Order::PLACED, $event);
        $this->container->get('order_manager')->logCartAction($order->getCart(), 'order_placed');
    }

    public function diffCarts(Cart $cart1, Cart $cart2)
    {
        $diff = [
            'cart' => $this->checkProperties(['name', 'surname', 'phone', 'email', 'companyName', 'companyId', 'companyTaxId', 'note'], $cart1, $cart2),
            'invoice' => $this->checkProperties(['street', 'zipcode', 'city', 'country'], $cart1->getInvoiceAddress(), $cart2->getInvoiceAddress()),
            'shipping' => $this->checkProperties(['name', 'street', 'zipcode', 'city', 'country'], $cart1->getShippingAddress(), $cart2->getShippingAddress()),
            'payment' => $this->compareProperties('title', $cart1->getPayment(), $cart2->getPayment()),
            'delivery' => $this->compareProperties('title', $cart1->getDelivery(), $cart2->getDelivery()),
            'products' => $this->checkProducts($cart1->getProducts(), $cart2->getProducts())
        ];
        return $diff;
    }

    protected function checkProperties($properties, $object1, $object2)
    {
        $result = [];
        foreach ($properties as $key => $value) {
            $result = array_merge($result, $this->compareProperties($value, $object1, $object2));
        }
        return $result;
    }

    protected function checkProducts($collection1, $collection2)
    {
        $changes = [];
        $checked = [];
        foreach ($collection1 as $product1) {
            $product2 = $collection2->matching(Criteria::create()->where(Criteria::expr()->eq("product", $product1->getProduct())))->current();
            $checked[] = $product1->getProduct();
            if (empty($product2)) {
                $changes[$product1->getProduct()->getId()] = -$product1->getAmount();
            } else {
                if ($product2->getAmount() - $product1->getAmount() != 0) {
                    $changes[$product1->getProduct()->getId()] = $product2->getAmount() - $product1->getAmount();
                }
            }
        }
        if (!empty($checked)) {
            $collectionToIterate = $collection2->matching(Criteria::create()->where(Criteria::expr()->notIn("product", $checked)));
        } else {
            $collectionToIterate = $collection2;
        }
        foreach ($collectionToIterate as $product2) {
            $changes[$product2->getProduct()->getId()] = $product2->getAmount();
        }
        return $changes;
    }

    protected function compareProperties($property, $object1, $object2)
    {
        if ($object1 === null && $object2 === null) {
            return [];
        }
        if ($object1 === null) {
            return [
                $property => [null, $object2->{'get' . ucfirst($property)}()]
            ];
        }
        if ($object2 === null) {
            return [
                $property => [$object1->{'get' . ucfirst($property)}(), null]
            ];
        }
        if ($object1->{'get' . ucfirst($property)}() != $object2->{'get' . ucfirst($property)}()) {
            return [
                $property => [$object1->{'get' . ucfirst($property)}(), $object2->{'get' . ucfirst($property)}()]
            ];
        }
        return [];
    }

    public function saveState(Cart $cart)
    {
        $cart->oldState = clone $cart;
    }

    public function logCartAction(Cart $cart, $action, $time = null)
    {
        if ($time == null) {
            $time = new \DateTime('now');
        }
        $diff = $cart->oldState === null ? "{}" : json_encode($this->diffCarts($cart->oldState, $cart));
        $log = new CartLog($cart, $action, $diff, $time);
        $this->em()->persist($log);
        $this->em()->flush();
    }

}
