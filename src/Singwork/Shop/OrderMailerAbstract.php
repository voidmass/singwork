<?php
namespace Singwork\Shop;

use Singwork\Content\Fragment;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrderMailer
 *
 * @author Matej Smisek
 */
abstract class OrderMailerAbstract {

    /**
     *
     * @var Order
     */
    protected $_order;

    public function __construct(Order $order) {
        $this->_order = $order;
    }

    public function getOrder() {
        return $this->_order;
    }

    public function setOrder(Order $order) {
        $this->_order = $order;
        return $this;
    }

    public function sendOrderEmail() {
        $this->sendEmail('order_placed')->sendMail('matej@lemonada.cz');
    }

    public function sendPickupEmail() {
        $this->sendEmail('order_ready_pickup');
    }

    public function sendProcessedEmail() {
        $this->sendEmail('order_processed');
    }

    public function sendPaymentPendingEmail() {
        $this->sendEmail('order_payment_pending');
    }

    public function sendShippedEmail() {
        $this->sendEmail('order_shipped');
    }

    public function sendEmail($type) {
        $m = new Mailer();

        $m->loadTemplate($type);
        $vars = $this->getEmailVars($type);
        $vars['main_text'] = $this->htmlEmailMainText($type, $vars);
        $vars['main_text_plaintext'] = strip_tags($this->htmlEmailMainText($type, $vars));
        $vars['title'] = $this->htmlEmailTitle($type, $vars);
        $content['content'] = (new Fragment('emailTemplate'))->setVariables($vars)->render();
        $content['order_id'] = $this->_order->getOrder()['id'];
        $content['plaintext'] = (new Fragment('emailTemplatePlaintext'))->setVariables($vars)->render();
        $m->prepareTemplate($content);
        $m->sendMail($this->_order->getOrder()['email']);
        return $m;
    }

    protected function getEmailVars($type) {
        $vars = [
            'order_id' => $this->_order->getOrder()['id'],
            'delivery_type' => $this->_order->getSelectedDelivery()['title'],
            'payment_type' => $this->_order->getSelectedPayment()['title'],
            'phone' => $this->_order->getOrder()['phone'],
            'email' => $this->_order->getOrder()['email'],
            'invoice_name' => $this->_order->getOrder()['name'] . ' ' . $this->_order->getOrder()['surname'],
            'invoice_street' => $this->_order->getAddress('invoice')['street'],
            'invoice_city' => $this->_order->getAddress('invoice')['city'],
            'invoice_zipcode' => $this->_order->getAddress('invoice')['zipcode'],
            'shipping' => $this->getShippingEmail(),
            'shipping_plaintext' => $this->getShippingEmail(true),
            'table_items' => $this->getItemsEmail(),
            'table_items_plaintext' => $this->getItemsEmail(true),
            'total' => $this->_order->getTotalCartPrice(true)
        ];
        $this->injectCompanyInfo($vars);
        if ($type !== 'order_shipped' && $type !== 'order_ready_pickup') {
            $this->injectPaymentInfo($vars);
        }

        return $vars;
    }

    protected function getShippingEmail($plaintext = false) {
        if ($this->_order->getSelectedDelivery()['id'] == 1) {
            return ($plaintext === true ? $this->htmlEmailPickupAddressPlaintext([]) : $this->htmlEmailPickupAddress([]));
        } else {
            if ($this->_order->getAddress('shipping') === false) {
                $d = $this->_order->getAddress('invoice');
                $d['name'] = $this->_order->getOrder()['name'] . ' ' . $this->_order->getOrder()['surname'];
            } else {
                $d = $this->_order->getAddress('shipping');
            }
            return ($plaintext === true ? $this->htmlEmailDeliveryAddressPlaintext($d) : $this->htmlEmailDeliveryAddress($d));
        }
    }

    protected function getItemsEmail($plaintext = false) {
        $ret = '';
        $odd = false;
        foreach ($this->_order->getItems() as $i) {
            $i['odd'] = $odd;
            $i['total_price'] = $i['amount'] * $i['price'];
            $i['url'] = core()->getUrlProcessor()->getDomain() . $i['url'];
            $ret .= ($plaintext === true ? $this->htmlEmailItemTablePlaintext($i) : $this->htmlEmailItemTable($i));
            if ($odd === false) {
                $odd = true;
            } else {
                $odd = false;
            }
        }
        return $ret;
    }

    protected function injectCompanyInfo(&$vars) {
        if (!empty($this->_order->getOrder()['company_ic'])) {
            $vars['company_ic'] = '<strong>IČ: </strong>' . $this->_order->getOrder()['company_ic'] . '<br />';
            $vars['company_ic_plaintext'] = 'IČ: ' . $this->_order->getOrder()['company_ic'] . "\r\n";
        }
        if (!empty($this->_order->getOrder()['company_dic'])) {
            $vars['company_dic'] = '<strong>DIČ: </strong>' . $this->_order->getOrder()['company_dic'] . '<br />';
            $vars['company_dic_plaintext'] = 'DIČ: ' . $this->_order->getOrder()['company_dic'] . "\r\n";
        }
        if (!empty($this->_order->getOrder()['company_name'])) {
            $vars['company_name'] = '<strong>Název: </strong>' . $this->_order->getOrder()['company_name'] . '<br />';
            $vars['company_name_plaintext'] = 'Název: ' . $this->_order->getOrder()['company_name'] . "\r\n";
        }
    }

    protected function injectPaymentInfo(&$vars) {
        if ($this->_order->getSelectedPayment()['id'] == 2) {
            $vars['payment_instructions'] = $this->htmlEmailPaymentInfo($vars);
            $vars['payment_instructions_plaintext'] = $this->htmlEmailPaymentInfoPlaintext($vars);
        }
    }

    public abstract function htmlEmailPaymentInfo($data);

    public abstract function htmlEmailPaymentInfoPlaintext($data);

    public abstract function htmlEmailItemTablePlaintext($data);

    public abstract function htmlEmailItemTable($data);

    public abstract function htmlEmailDeliveryAddressPlaintext($data);

    public abstract function htmlEmailDeliveryAddress($data);

    public abstract function htmlEmailPickupAddressPlaintext($data);

    public abstract function htmlEmailPickupAddress($data);

    public abstract function htmlEmailMainText($type, $data);

    public abstract function htmlEmailTitle($type, $data);
}
