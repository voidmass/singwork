<?php
namespace Singwork\Shop;

use Singwork\Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Shop
 *
 * @author Matej Smisek
 */
class Shop {

    protected $category;
    protected $product;
    protected $image;
    protected $price;
    protected $urls = [];

    /**
     *
     * @var Cart
     */
    protected $cart;

    public function __construct() {
        $this->category = new Table\Shop\SingshopCategory();
        $this->product = new Table\Shop\SingshopProduct();
        $this->image = new Table\Shop\SingshopProductImage();
        $this->price = new Table\Shop\SingshopProductPrice();

//        $this->cart = new Cart();

        $this->urls['detail'] = core()->getUrlProcessor()->constructUrl(core()->getConfig()['shop']['detail_url']);
        $this->urls['category'] = core()->getUrlProcessor()->constructUrl(core()->getConfig()['shop']['category_url']);
    }

    public function getCategories() {
        $rows = $this->category->getVisible();
        $categories = [];
        while ($d = $rows->fetch()) {
            $d['url'] = $this->urls['category'];
            $categories[] = $d;
        }
        return $categories;
    }

    public function getCategory($category) {
        return ['name' => $this->category->getId($category)['name'], 'products' => $this->getProducts($category)];
    }

    public function getProducts($category = null) {
        $raw_products = $this->product->getByCategory($category);
        $products = [];
        while ($d = $raw_products->fetch()) {            
            $products[] = $this->getProduct($d['id']);
        }
        return $products;
    }

    public function getProduct($id) {
        $product = $this->product->getId($id);
        $product['main_image'] = $this->image->getFirstImage($id)['uri'];
        $product['images'] = $this->image->getByProduct($id);
        $product['prices'] = $this->price->getByProduct($id);
        $product['url'] = $this->urls['detail'] . '!/' . $product['id'] . '/' . core()->getUrlProcessor()->convertToUrlString($product['name']) . '/';
        return $product;
    }

}
