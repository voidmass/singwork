<?php
namespace Singwork\Shop;

use Singwork\Table;
use Singwork\Database\Sequencer;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cart
 *
 * @author Matej Smisek
 */
class Order {

    protected $_user;
    protected $_items;
    protected $_address_shippping;
    protected $_address_invoice;
    protected $_order;
    protected $_order_id;

    /**
     *
     * @var Table_SingshopOrder
     */
    protected $t_order;
    protected $t_item;
    protected $t_action;
    protected $t_address;
    protected $t_product;
    protected $t_image;
    protected $t_delivery_type;
    protected $t_payment_type;
    protected $t_delivery_payment;
    protected $t_user_address;
    protected $_url_detail;

    /**
     *
     * @var String
     */
    protected $_old_state = '{}';

    /**
     *
     * @var Order
     * 
     */
    protected static $instance = null;

    /**
     * 
     * @return Core
     */
    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new Order();
        }
        return self::$instance;
    }

    public function __construct($autocreate = true, $connection = 'main') {
        $this->_user = core()->getUserHandler()->getUser();
        $this->t_order = new Table\Shop\SingshopOrder($connection);
        $this->t_item = new Table\Shop\SingshopOrderItem($connection);
        $this->t_action = new Table\Shop\SingshopOrderAction($connection);
        $this->t_address = new Table\Shop\SingshopOrderAddress($connection);
        $this->t_product = new Table\Shop\SingshopProduct($connection);
        $this->t_image = new Table\Shop\SingshopProductImage($connection);
        $this->t_user_address = new Table\Shop\SingshopUserAddress($connection);

        $this->t_delivery_type = new Table\Shop\SingshopDeliveryType($connection);
        $this->t_payment_type = new Table\Shop\SingshopPaymentType($connection);
        $this->t_delivery_payment = new Table\Shop\SingshopDeliveryPaymentType($connection);
        $this->_url_detail = core()->getUrlProcessor()->constructUrl(core()->getConfig()['shop']['detail_url']);
        if ($autocreate === true) {
            $this->createCart();
        }
    }

    public function createCart($order_id = null) {
        $this->_order = $this->createOrder($order_id);
        $this->_items = $this->t_item->getFromOrder($this->_order['id']);
        $this->_address_shippping = self::filterResult($this->t_address->getShipping($this->_order['id']));
        $this->_address_invoice = self::filterResult($this->t_address->getInvoice($this->_order['id']));
    }

    public static function filterResult($result) {
        if ($result === false) {
            return [];
        } else {
            return array_filter($result);
        }
    }

    protected function createOrder($order_id = null) {
        if ($order_id !== null) {
            return $this->t_order->getId($order_id);
        }
        $order = $this->t_order->getOpen($this->_user->getId());
        if ($order === false) {
            $id = (new Sequencer('ORDER'))->requestId();
            $this->t_order->insertOrder($id, $this->_user->getId());
            $this->logOrderAction('created', $id);
            return $this->t_order->getId($id);
        }
        return $order;
    }

    public function toJson() {
        $data = [];
        $data['order'] = $this->_order;
        $data['invoice'] = $this->_address_invoice;
        $data['shipping'] = $this->_address_shippping;
        $data['items'] = $this->getItems(true);
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }

    public function getAddress($type) {
        if ($type === 'invoice') {
            return $this->_address_invoice;
        } else {
            return $this->_address_shippping;
        }
    }

    public function getFormattedData() {
        $data = [];
        $data['contact']['name'] = $this->_order['name'];
        $data['contact']['surname'] = $this->_order['surname'];
        $data['contact']['email'] = $this->_order['email'];
        $data['contact']['phone'] = $this->_order['phone'];
        $data['contact']['permission'] = 'true';
        $data['invoice']['street'] = $this->_address_invoice['street'];
        $data['invoice']['city'] = $this->_address_invoice['city'];
        $data['invoice']['zipcode'] = $this->_address_invoice['zipcode'];
        $data['invoice']['country'] = $this->_address_invoice['country'];

        $data['shipping']['street'] = $this->_address_shippping['street'];
        $data['shipping']['city'] = $this->_address_shippping['city'];
        $data['shipping']['zipcode'] = $this->_address_shippping['zipcode'];
        $data['shipping']['country'] = $this->_address_shippping['country'];

        $data['company']['name'] = $this->_order['company_name'];
        $data['company']['ic'] = $this->_order['company_ic'];
        $data['company']['dic'] = $this->_order['company_dic'];

        return $data;
    }

    public function getItems($cutText = false) {
        $items = [];
        foreach ($this->_items as $k => $v) {
            $item = $this->getFullItem($v['product_id'], $v['amount']);
            if ($cutText === true) {
                unset($item['description']);
                unset($item['description_short']);
                unset($item['text_detail']);
                unset($item['keywords']);
            }
            $items[] = $item;
        }
        return $items;
    }

    public function addItem($product_id, $amount = 1) {
        $cart_item = & $this->getItemByProductId($product_id);
        if ($cart_item === false) {
            $this->recordState();
            $id = $this->t_item->insertItem($this->_order['id'], $product_id, $amount);
            $this->_items[] = $this->t_item->getId($id);
            $this->logOrderAction('item_add');
            return true;
        } else {
            $this->t_item->updateAmount($cart_item['id'], $cart_item['amount'] + $amount);
            return true;
        }
    }

    public function updateAmount($product_id, $amount) {
        if ($amount <= 0) {
            return $this->removeItem($product_id);
        }
        $this->recordState();
        $cart_item = & $this->getItemByProductId($product_id);
        $this->t_item->updateAmount($cart_item['id'], $amount);
        $cart_item['amount'] = $amount;
        $this->logOrderAction('item_change');
        return true;
    }

    public function removeItem($product_id) {
        $this->recordState();
        $cart_item = $this->getItemByProductId($product_id, true)[0];
        $this->t_item->deleteRow($cart_item['id']);
        $this->logOrderAction('item_remove');
        return true;
    }

    public function getFullItem($product_id, $amount) {
        $item = $this->t_product->getId($product_id);
        $item['amount'] = $amount;
        $item['total'] = $item['amount'] * $item['price'];
        $item['image'] = $this->t_image->getFirstImage($product_id)['uri'];
        $item['url'] = $this->_url_detail . '!/' . $item['id'] . '/' . core()->getUrlProcessor()->convertToUrlString($item['name']) . '/';
        return $item;
    }

    public function & getItemByProductId($product_id, $splice = false) {
        foreach ($this->_items as $k => &$v) {
            if ($v['product_id'] === $product_id) {
                if ($splice === true) {
                    return array_splice($this->_items, $k, 1);
                } else {
                    return $v;
                }
            }
        }
        return false;
    }

    public function getTotalCartPrice($including_shipping = false) {
        $total = 0;
        foreach ($this->getItems() as $k => $v) {
            $total += $v['price'] * $v['amount'];
        }
        if ($including_shipping === true) {
            $total += $this->getSelectedDelivery()['price'] + $this->getSelectedPayment()['price'];
        }
        return $total;
    }

    public function getTotalItemPrice($product_id) {
        $item = $this->getItemByProductId($product_id);
        $item_template = $this->getFullItem($product_id, $item['amount']);
        return $item_template['price'] * $item['amount'];
    }

    /**
     * 
     * @param String $action Type of action to be logged
     * @param String $old_object Json encoded object of object before change
     * @param String $new_object Json encoded object of object after change
     * @param String $id Order id
     * @param String $comment optional comment
     */
    public function logOrderAction($action, $id = null, $comment = '') {
        if ($id === null) {
            $id = $this->_order['id'];
        }
        $this->t_action->insertAction($id, $action, $this->_old_state, $this->toJson(), $comment);
        $this->_old_state = '{}';
    }

    public function insertAddress($type, $street, $city, $zipcode, $country, $name = null) {
        $address = $this->t_address->getType($this->_order['id'], $type);
        if ($address === false) {
            $this->t_address->insertAddress($this->_order['id'], $type, $street, $city, $zipcode, $country, $name);
        } else {
            $this->t_address->updateAddress($address['id'], $street, $city, $zipcode, $country, $name);
        }
    }

    public function insertCompany($name, $ic, $dic) {
        $this->t_order->updateCompany($this->_order['id'], $name, $ic, $dic);
    }

    public function insertContact($name, $surname, $email, $phone) {
        $this->t_order->updateContact($this->_order['id'], $name, $surname, $email, $phone);
    }

    public function setDeliveryAndPayment($delivery_type_id, $payment_type_id) {
        $this->t_order->updateDeliveryAndPayment($this->_order['id'], $delivery_type_id, $payment_type_id);
    }

    public function getOrder() {
        return $this->_order;
    }

    public function getSelectedDelivery() {
        return $this->t_delivery_type->getId($this->_order['delivery_type_id']);
    }

    public function getSelectedPayment() {
        return $this->t_payment_type->getId($this->_order['payment_type_id']);
    }

    

    public function copyDataFromUser() {
        $this->insertContact($this->_user->getName(), $this->_user->getSurname(), $this->_user->getEmail(), $this->_user->getValue('phone'));
        $invoice = $this->t_user_address->getInvoice($this->_user->getId());
        $this->insertAddress('invoice', $invoice['street'], $invoice['city'], $invoice['zipcode'], $invoice['country']);
        $this->insertCompany($this->_user->getValue('company_name'), $this->_user->getValue('company_ic'), $this->_user->getValue('company_dic'));
    }

    public function progressOrder() {
        switch ($this->_order['status']) {
            case 'placed':
                if ($this->getSelectedPayment()['type'] === 'wire') {
                    return $this->paymentPendingOrder();
                } else {
                    return $this->processOrder();
                }
            case 'payment_pending':
                return $this->processOrder();
            case 'processed':
                if ($this->getSelectedDelivery()['type'] === 'pickup') {
                    return $this->pickupReadyOrder();
                } elseif ($this->getSelectedDelivery()['type'] === 'mail') {
                    return $this->shipOrder();
                }
            case 'sent':
            case 'pickup':
                return $this->completeOrder();
            default:
                throw new RuntimeSingException('cannot progress order more');
        }
    }

    public function placeOrder($note = null) {
        $this->recordState();

        $this->t_order->update(['note' => $note], ['id' => $this->_order['id']]);
        $this->_order['note'] = $note;
        $this->_order['status'] = 'placed';
        $this->t_order->placeOrder($this->_order['id']);
        $this->t_order->update(['price' => $this->getTotalCartPrice()], ['id' => $this->_order['id']]);
        $this->_order['price'] = $this->getTotalCartPrice();
        (new \Application\OrderMailer($this))->sendOrderEmail();
        $this->logOrderAction('order_placed');
    }

    public function processOrder() {
        $this->t_order->setStatus($this->_order['id'], 'processed');
        $this->logOrderAction('order_processed');
//        (new OrderMailer($this))->sendProcessedEmail();
        return 'processed';
    }

    public function paymentPendingOrder() {
        $this->t_order->setStatus($this->_order['id'], 'payment_pending');
        $this->logOrderAction('order_payment_pending');
        (new \Application\OrderMailer($this))->sendPaymentPendingEmail();
        return 'payment_pending';
    }

    public function pickupReadyOrder() {
        $this->t_order->setStatus($this->_order['id'], 'pickup');
        $this->logOrderAction('order_pickup_ready');
        $this->logOrderAction('pickup_ready_mail');
        (new \Application\OrderMailer($this))->sendPickupEmail();
        return 'pickup';
    }

    public function shipOrder() {
        $this->t_order->setStatus($this->_order['id'], 'sent');
        $this->logOrderAction('order_shipped');
        (new \Application\OrderMailer($this))->sendShippedEmail();
        return 'sent';
    }

    public function completeOrder() {
        $this->t_order->setStatus($this->_order['id'], 'completed');
        $this->logOrderAction('order_completed');
        return 'completed';
    }

    public function cancelOrder($comment) {
        switch ($this->_order['status']) {
            case 'pickup':
            case 'sent':
            case 'payment_pending':
                $this->t_order->setStatus($this->_order['id'], 'expired');
                $this->logOrderAction('order_expired', null, $comment);
                return 'expired';
            case 'placed':
            case 'processed':
                $this->t_order->setStatus($this->_order['id'], 'canceled');
                $this->logOrderAction('order_canceled', null, $comment);
                return 'canceled';
            default:
                $this->archiveOrder($comment);
                return 'archived';
        }
    }

    protected function archiveOrder($comment) {
        $this->logOrderAction('order_archived', null, $comment);
        $order = $this->_order;
        $order_data = json_encode($order, JSON_UNESCAPED_UNICODE);
        $invoice_address = json_encode($this->_address_invoice, JSON_UNESCAPED_UNICODE);
        $shipping_address = json_encode($this->_address_shippping, JSON_UNESCAPED_UNICODE);
        $items_data = json_encode($this->getItems(true), JSON_UNESCAPED_UNICODE);
        $actions_data = json_encode($this->t_action->getOrder($this->_order['id'])->fetchAll(), JSON_UNESCAPED_UNICODE);
        $id = (new Table\Shop\SingshopOrderArchive())->insertOrder($this->_order['id'], $order_data, $invoice_address, $shipping_address, $items_data, $actions_data, $comment);
        if (is_numeric($id)) {
            $this->t_order->deleteRow($this->_order['id']);
        }
    }

    public function verifyOrder() {
        if (!$this->required($this->_order['delivery_type_id'])) {
//            slog_debug('Order Verify: delivery_type_id');
            return false;
        }
        if (!$this->required($this->_order['payment_type_id'])) {
//            slog_debug('Order Verify: payment_type_id');
            return false;
        }
        if (!$this->required($this->_order['email'])) {
//            slog_debug('Order Verify: email');
            return false;
        }
        if (!$this->required($this->_order['phone'])) {
//            slog_debug('Order Verify: phone');
            return false;
        }
        if (!$this->required($this->_order['name'])) {
//            slog_debug('Order Verify: name');
            return false;
        }
        if (!$this->required($this->_order['surname'])) {
//            slog_debug('Order Verify: surname');
            return false;
        }
        if (!$this->required($this->_address_invoice['street'])) {
//            slog_debug('Order Verify: street');
            return false;
        }
        if (!$this->required($this->_address_invoice['city'])) {
//            slog_debug('Order Verify: city');
            return false;
        }
        if (!$this->required($this->_address_invoice['zipcode'])) {
//            slog_debug('Order Verify: zipcode');
            return false;
        }
        if (!$this->required($this->_address_invoice['country'])) {
//            slog_debug('Order Verify: country');
            return false;
        }
        return true;
    }

    public function recordState() {
        $this->_old_state = $this->toJson();
    }

    protected function required($value) {
        if (empty($value)) {
            return false;
        }
        return true;
    }

    public static function serializeResult($array, $column) {
        $result = [];
        foreach ($array as $v) {
            $result[] = $v[$column];
        }
        return $result;
    }

}
