<?php

namespace Singwork\Shop;

use Singwork\Table;
use Singwork\Content\Controller;
use Singwork\Form\FormBuilder;
use Singwork\Model\Entities\Cart\Cart;
use Singwork\Model\Entities\Cart\CartProduct;
use Doctrine\Common\Collections\Criteria;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShopControllerController
 *
 * @author Matej Smisek
 */
abstract class ControllerOrder extends Controller
{

    /**
     *
     * @var Order
     */
    protected $_order;

    /**
     * 
     * @return Cart
     */
    protected function getCart()
    {
        return $this->getApplication()->getCart();
    }

    protected function isCartOpen()
    {
        return $this->getApplication()->isCartOpen();
    }

    protected function recalculateCartProducts()
    {
        foreach ($this->getCart()->getProducts() as $cartProduct) {
            $cartProduct->setPrice($cartProduct->getProductVariant() !== null ? $cartProduct->getProductVariant()->getCalculatedPrice() : $cartProduct->getProduct()->getCalculatedPrice())->setDiscount($cartProduct->getProduct()->getSaleAmount());
        }
        $this->em()->flush();
    }

    protected function addProductToCart($product_id, $amount = 1, $variant = null)
    {
        $this->container->get('order_manager')->saveState($this->getCart());
        if ($amount > $_ENV['config']['shop']['max_product_amount']) {
            $amount = $_ENV['config']['shop']['max_product_amount'];
        }
        if (($this->getCart()->getProductCount() + $amount) > $_ENV['config']['shop']['max_cart_amount']) {
            $amount = ($_ENV['config']['shop']['max_cart_amount'] - $this->getCart()->getProductCount());
        }
        $productTemplate = $this->getProductTemplateById($product_id);
        if ($variant !== null) {
            $variantTemplate = $this->em()->find('Base:Shop\ProductVariant', $variant);
            $current_product = $this->getCart()->getProductByTemplateAndVariant($productTemplate, $variantTemplate);
        } else {
            $current_product = $this->getCart()->getProductByTemplate($productTemplate);
        }
        if (empty($current_product) && $amount > 0) {
            $product = new CartProduct($productTemplate, $variantTemplate !== null ? $variantTemplate->getCalculatedPrice() : $productTemplate->getCalculatedPrice(), $productTemplate->getSaleAmount(), $amount);
            $this->getCart()->addProduct($product);
            $this->em()->persist($product);
        } else {
            if ($amount > 0) {
                $current_product->setAmount($current_product->getAmount() + $amount);
            }
            $product = $current_product;
        }
        if ($variant !== null) {
            $product->setProductVariant($variantTemplate);
        }
        $this->container->get('order_manager')->logCartAction($this->getCart(), 'cart_modify_product');

        return $product;
    }

    protected function removeFromCart($cart_product_id)
    {
        $this->container->get('order_manager')->saveState($this->getCart());
        $product = $this->getCart()->getProducts()->matching(Criteria::create()->where(Criteria::expr()->eq("id", $cart_product_id)))->current();
        $product->setCart(null);
        $this->getCart()->getProducts()->remove($cart_product_id);
        $this->em()->remove($product);
        $this->container->get('order_manager')->logCartAction($this->getCart(), 'cart_modify_product');
        return $product;
    }

    protected function modifyCart($cart_product_id, $amount)
    {
        $this->container->get('order_manager')->saveState($this->getCart());
        if ($amount <= 0) {
            return $this->removeFromCart($cart_product_id);
        }
        if ($amount > $_ENV['config']['shop']['max_product_amount']) {
            $amount = $_ENV['config']['shop']['max_product_amount'];
        }

        $product = $this->getCart()->getProducts()->matching(Criteria::create()->where(Criteria::expr()->eq("id", $cart_product_id)))->current();
        $product->setAmount($amount);
        if (($this->getCart()->getProductCount()) > $_ENV['config']['shop']['max_cart_amount']) {
            $product->setAmount($amount - ($this->getCart()->getProductCount() - $_ENV['config']['shop']['max_cart_amount']));
        }
        $this->container->get('order_manager')->logCartAction($this->getCart(), 'cart_modify_product');
        return $product;
    }

    protected function getProductTemplateById($product_id)
    {
        return $this->em()->getRepository('Base:Shop\Product')->find($product_id);
    }

    public function getAvailableShipping()
    {
        return [
            'delivery' => $this->getAvailableDelivery(),
            'payment' => $this->getAvailablePayment()
        ];
    }

    public function getAvailableDelivery()
    {
        return $this->em()->getRepository('Base:Shop\DeliveryType')->findBy(['visible' => 1], ['order' => 'ASC']);
    }

    public function getAvailablePayment()
    {
        return $this->em()->getRepository('Base:Shop\PaymentType')->findBy(['visible' => 1], ['order' => 'ASC']);
    }

    protected function getEmailPermission()
    {
        if ($this->getCart()->getEmail() == null) {
            return new \Singwork\Model\Entities\UserEmailPermission('EMPTY', false, new \DateTime('now'), '');
        }
        $permission = $this->em()->getRepository('Base:UserEmailPermission')->find($this->getCart()->getEmail());
        if ($permission === null) {
            return new \Singwork\Model\Entities\UserEmailPermission('EMPTY', false, new \DateTime('now'), '');
        } else {
            return $permission;
        }
    }

    protected function saveEmailPermission($email, $permission)
    {
        $this->container->get('email_permission_manager')->setPermission($email, $permission);
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function preInit()
    {
        parent::preInit();
        $tracking = $this->getGaProducts();
        $url_id = core()->getUrlProcessor()->getPage()->getId();
        $config = core()->getConfig()['shop'];
        switch ($url_id) {
            case $config['order_cart_url']:
                $tracking .= 'ga("ec:setAction", "checkout", {"step": 1});';
                break;
            case $config['order_address_url']:
                $tracking .= 'ga("ec:setAction", "checkout", {"step": 2});';
                break;
            case $config['order_shipping_url']:
                $tracking .= 'ga("ec:setAction", "checkout", {"step": 3});';
                break;
            case $config['order_confirm_url']:
                $tracking .= 'ga("ec:setAction", "checkout", {"step": 4});';
                break;
            case $config['order_summary_url']:
                $tracking = '';
                break;
        }
        core()->header()->addAnalyticsTracking($tracking);
    }

    protected function getGaProducts()
    {
        $return = '';
        foreach ($this->_order->getItems() as $item) {
            $return .= "ga('ec:addProduct',";
            $return .= "{
            'id': '" . $item['catalog_number'] . "',
            'name': '" . $item['name'] . "',
            'category': '" . $item['category_id'] . "',            
            'price': '" . $item['price'] . "',
            'quantity': '" . $item['amount'] . "'
            }";

            $return .= ");";
        }
        return $return;
    }

    public function displayAddressOLD()
    {
        if (core()->getUserHandler()->isLoggedIn()) {
            $this->_view->form_url = $this->constructUrl(core()->getConfig()['shop']['order_address_url']) . '!/form/shopAddressUser/';
            $data['shipping'] = array_merge((new Table\Shop\SingshopUserAddress())->getShipping($this->getUser()->getId(), $this->_order->getAddress('shipping')));
            $this->_view->form = (new FormBuilder('singshopAddressUser', $data, 'LayoutController::HTML_FORM_SECTION', 'LayoutController::HTML_FORM_INPUT'))->construct();
            $this->_view->user = $this->getUser()->getRawData();
            $this->_view->invoice = (new Table\Shop\SingshopUserAddress())->getInvoice($this->getUser()->getId());
        } else {
            $this->_view->form_url = $this->constructUrl(core()->getConfig()['shop']['order_address_url']) . '!/form/shopAddress/';
            $this->_view->form = (new FormBuilder('singshopAddress', $this->_order->getFormattedData(), 'LayoutController::HTML_FORM_SECTION', 'LayoutController::HTML_FORM_INPUT'))->construct();
        }
    }

    public function addItemAjax()
    {
        $this->_order->addItem($_POST['product_id'], $_POST['amount']);
        $d = $this->_order->getFullItem($_POST['product_id'], $_POST['amount']);
        $d['cart_url'] = $this->constructUrl(core()->getConfig()['shop']['order_cart_url']);
        $modal_html = $this->htmlCartModal($d);
        $this->outputJson(['result' => 'added', 'data' =>
            [
                'total_price' => $this->_order->getTotalCartPrice(),
                'modal' => $modal_html,
                'item' => $d
        ]]);
    }

    protected function debugSendAjax()
    {
        (new \Application\OrderMailer($this->_order))->sendEmail('order_placed');
        $this->outputJson('Done');
    }

    public function removeItemAjax()
    {
        $item = $this->_order->getFullItem($_POST['product_id'], null);
        $item['amount'] = $this->_order->getItemByProductId($_POST['product_id'])['amount'];
        $this->_order->removeItem($_POST['product_id']);
        $this->outputJson(['result' => 'removed', 'data' =>
            [
                'total_price' => $this->_order->getTotalCartPrice(),
                'item' => $item
        ]]);
    }

    public function updateItemAjax()
    {
        $item = $this->_order->getFullItem($_POST['product_id'], null);
        $item['amount'] = $this->_order->getItemByProductId($_POST['product_id'])['amount'];
        $this->_order->updateAmount($_POST['product_id'], $_POST['amount']);
        if ($_POST['amount'] <= 0) {
            $this->outputJson(['result' => 'removed', 'data' => [
                    'total_price' => $this->_order->getTotalCartPrice(),
                    'item' => $item,
            ]]);
        } else {
            if ($item['amount'] > $_POST['amount']) {
                $action = 'remove';
            } elseif ($item['amount'] < $_POST['amount']) {
                $action = 'add';
            } else {
                $action = 'none';
            }
            $item['amount'] = $_POST['amount'];
            $this->outputJson(['result' => 'updated', 'data' => [
                    'total_price' => $this->_order->getTotalCartPrice(),
                    'item_price' => $this->_order->getTotalItemPrice($_POST['product_id']),
                    'item' => $item,
                    'action' => $action
            ]]);
        }
    }

    public function shopAddressForm()
    {
        if ($_POST['type'] === 'check') {
            return;
        }
        $this->_order->recordState();
        $this->_order->insertContact($_POST['fields']['contact']['name'], $_POST['fields']['contact']['surname'], $_POST['fields']['contact']['email'], $_POST['fields']['contact']['phone']);
        $this->_order->insertAddress('invoice', $_POST['fields']['invoice']['street'], $_POST['fields']['invoice']['city'], $_POST['fields']['invoice']['zipcode'], $_POST['fields']['invoice']['country']);
        if ($_POST['fields']['company']['active'] === 'true') {
            $this->_order->insertCompany($_POST['fields']['company']['name'], $_POST['fields']['company']['ic'], $_POST['fields']['company']['dic']);
        }
        if ($_POST['fields']['shipping']['active'] === 'true') {
            $this->_order->insertAddress('shipping', $_POST['fields']['shipping']['street'], $_POST['fields']['shipping']['city'], $_POST['fields']['shipping']['zipcode'], $_POST['fields']['shipping']['country'], $_POST['fields']['shipping']['name']);
        } else {
            $this->_order->insertAddress('shipping', $_POST['fields']['invoice']['street'], $_POST['fields']['invoice']['city'], $_POST['fields']['invoice']['zipcode'], $_POST['fields']['invoice']['country']);
        }
        $this->_order->logOrderAction('address_confirm');
        $this->formValid('', 'redirect', $this->constructUrl(core()->getConfig()['shop']['order_shipping_url']));
    }

    public function shopAddressUserForm()
    {
        if ($_POST['type'] === 'check') {
            return;
        }
        $this->_order->recordState();
        $this->_order->copyDataFromUser();
        if ($_POST['fields']['shipping']['active'] === 'true') {
            $this->_order->insertAddress('shipping', $_POST['fields']['shipping']['street'], $_POST['fields']['shipping']['city'], $_POST['fields']['shipping']['zipcode'], $_POST['fields']['shipping']['country'], $_POST['fields']['shipping']['name']);
        }
        $this->_order->logOrderAction('address_confirm');
        $this->formValid('', 'redirect', $this->constructUrl(core()->getConfig()['shop']['order_shipping_url']));
    }

    public function shopConfirmFormOLD()
    {
        if ($this->_order->getOrder()['id'] !== $_POST['fields']['id']) {
            return $this->outputJson(['status' => 'invalid', 'message' => 'Nastala neznáma chyba']);
        }
        if ($this->_order->verifyOrder() === false) {
            return $this->outputJson(['status' => 'invalid', 'message' => 'Nastala neznáma chyba']);
        }
        $total = $this->_order->getTotalCartPrice(true);
        $this->_order->placeOrder($_POST['fields']['note']);
        $this->formValid('', '', null, [
            'total' => $total,
            'ga_products' => $this->getGaProductsJs(),
            'order_id' => $this->_order->getOrder()['id'],
            'redirect' => $this->constructUrl(core()->getConfig()['shop']['order_summary_url']),
            'shipping_price' => ($this->_order->getSelectedDelivery()['price'] + $this->_order->getSelectedPayment()['price'])
        ]);
    }

}
