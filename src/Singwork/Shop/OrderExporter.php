<?php

namespace Singwork\Shop;

//use Singwork\Startup\Settings;
//use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Singwork\Model\Entities\Cart\Order;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrderExporter
 *
 * @author Matej Smisek
 */
class OrderExporter
{

    /**
     *
     * @var String
     */
    protected $_type;

    /**
     *
     * @var String
     */
    protected $_target;

    /**
     *
     * @var Array
     */
    protected $_orders = [];
    public static $COUNTRY = ['CZ' => 'Česká republika', 'SK' => 'Slovenská republika'];
    public static $PAYMENT = ['wire' => 'převodem', 'cash' => 'hotově', 'delivery' => 'dobírkou'];
    public static $DELIVERY = ['pickup' => 'osobní vyzvednutí na prodejně', 'mail' => 'externí dopravce'];

    public function __construct(Array $orders, String $type, String $target)
    {
        $this->_orders = $orders;
        $this->_type = $type;
        $this->_target = $target;
    }

    public function exportAsXml()
    {
        switch ($this->_target) {
            case 'moneyS3':
            default:
                return $this->exportMoneyS3();
        }
    }

    public function exportAsCsv()
    {
        switch ($this->_target) {
            case 'dpd':
            default:
                return $this->exportDpd();
        }
    }

    protected function exportMoneyS3()
    {
        switch ($this->_type) {
            case 'order':
                return $this->exportMoneyS3Order();
        }
    }

    protected function exportMoneyS3Order()
    {
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><MoneyData/>');
        $orders = $xml->addChild('SeznamObjPrij');
        /* @var $order \Singwork\Model\Entities\Cart\Order */
        foreach ($this->_orders as $order) {
            $node = $orders->addChild('ObjPrij');

            $data = [
                'Popis' => 'Eshop - ' . $order->getId() . ' ' . $order->getCart()->getName() . ' ' . $order->getCart()->getSurname(),
                'Vystaveno' => \Singwork\Util\Util::convertDate($order->getCart()->getClosedTime()),
                'Celkem' => $order->getProductsPrice() + $order->getShippingPrice(),
                'VarSymbol' => $order->getId(),
                'Polozka' => $this->getMoneyS3Items($order),
                'DodOdb' => $this->getMoneyS3Invoice($order),
                'KonecPrij' => $this->getMoneyS3Shipping($order),
                'PlatPodm' => self::$PAYMENT[$order->getCart()->getPayment()->getType()],
                'Doprava' => self::$DELIVERY[$order->getCart()->getDelivery()->getType()],
                'ZkratkaTyp' => 'ESHOP'
            ];

//            print_r($data);
            $this->arrayToXML(array_merge($data, $this->getMoneyS3Mail($order)), $node);
        }
        return html_entity_decode($xml->asXML(), ENT_QUOTES | ENT_XML1, 'UTF-8');
    }

    /**
     * 
     * @param Order $order
     */
    protected function getMoneyS3Items(Order $order)
    {
        $return = [];
        foreach ($order->getCart()->getProducts() as $product) {
            $return[] = [
                'PocetMJ' => $product->getAmount(),
                'Cena' => $product->getPrice(),
                'Sklad' => ['KodSkladu' => '001'],
                'KmKarta' => ['Katalog' => $product->getProduct()->getCatalog()]
            ];
        }
        return $return;
    }

    /**
     * 
     * @param Order $order
     */
    protected function getMoneyS3Invoice(Order $order)
    {
        $return = [];
        $name = ($order->getCart()->getCompanyName() ?? $order->getCart()->getName() . ' ' . $order->getCart()->getSurname());

        $return['Nazev'] = $return['FaktNazev'] = $return['ObchNazev'] = $name;
        $return['ICO'] = $order->getCart()->getCompanyId();
        $return['DIC'] = $order->getCart()->getCompanyTaxId();
        $return['FaktAdresa'] = $return['ObchAdresa'] = [
            'Ulice' => $order->getCart()->getInvoiceAddress()->getStreet(),
            'Misto' => $order->getCart()->getInvoiceAddress()->getCity(),
            'PSC' => $order->getCart()->getInvoiceAddress()->getZipcode(),
            'Stat' => self::$COUNTRY[$order->getCart()->getInvoiceAddress()->getCountry()]
        ];
        return $return;
    }

    /**
     * 
     * @param Order $order
     */
    protected function getMoneyS3Shipping(Order $order)
    {
        if (empty($order->getCart()->getShippingAddress())) {
            return null;
        }
        $return = [];
        $return['Nazev'] = $order->getCart()->getShippingAddress()->getName();
        $return['Adresa'] = [
            'Ulice' => $order->getCart()->getShippingAddress()->getStreet(),
            'Misto' => $order->getCart()->getShippingAddress()->getCity(),
            'PSC' => $order->getCart()->getShippingAddress()->getZipcode(),
            'Stat' => self::$COUNTRY[$order->getCart()->getShippingAddress()->getCountry()]
        ];
        return $return;
    }

    protected function getMoneyS3Mail(Order $order)
    {
        if ($order->getCart()->getDelivery()->getPartner() === 'DPD') {
            return [
                'Prepravce' => [
                    'Zkrat' => 'DPD'
                ],
                'TypZasilky' => [
                    'Zkrat' => 'NCP'
                ]
            ];
        } else {
            return [];
        }
    }

    /**
     * 
     * @param Array $data
     * @param SimpleXMLElement $xml_data
     */
    protected function arrayToXML($data, &$xml_data)
    {

        foreach ($data as $key => $value) {
            if ($value === null) {
                continue;
            }
            if (is_array($value)) {
                if (count(array_filter(array_keys($value), 'is_string')) == 0) {
                    foreach ($value as $subvalue) {
                        $subnode = $xml_data->addChild($key);
                        $this->arrayToXML($subvalue, $subnode);
                    }
                } else {
                    $subnode = $xml_data->addChild($key);
                    $this->arrayToXML($value, $subnode);
                }
            } else {
                $xml_data->addChild($key, $value);
            }
        }
    }

}
