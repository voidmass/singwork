<?php

namespace Singwork\Shop;

use Singwork\Content\Controller;
use Singwork\Exception\RuntimeSingException;
use Singwork\Util\Util;
use Doctrine\Common\Collections\Criteria;
use Singwork\Model\Entities\Shop\Product;
use Singwork\Model\Entities\Shop\Category;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShopControllerController
 *
 * @author Matej Smisek
 */
abstract class ControllerShop extends Controller
{

    protected $shop;

    public function __construct()
    {
        parent::__construct();
//        $this->shop = new Shop();
    }

    protected function getCategories()
    {
        $categories = $this->em()->getRepository('Base:Shop\Category')->findBy(['visible' => true]);
        foreach ($categories as &$category) {
            $category->_url = $this->getCategoryUrl($category);
        }
        return $categories;
    }

    protected function getProducts($category_id)
    {

        $category = $this->em()->getRepository('Base:Shop\Category')->find($category_id);

//        $products = $this->em()->getRepository('Base:Shop\Product')->findBy(['category' => $category, 'visible' => true]);
        $products = $category->getProducts(true)->matching(Criteria::create()->where(Criteria::expr()->eq("visible", true)));
        foreach ($products as &$product) {
            $product->url = $this->getProductUrl($product);
        }
        return $products;
    }

    public function getProductByUrl($product_url)
    {
        return $this->em()->getRepository('Base:Shop\Product')->findOneBy(['urlName' => $product_url]);
    }
    public function getProductById($product_id)
    {
        return $this->em()->getRepository('Base:Shop\Product')->findOneBy(['id' => $product_id]);
    }

    protected function getBreadcrumbsToProduct(Product $product)
    {
        return [
            'Home',
            $product->getMainCategory(),
            $product
        ];
    }

    protected abstract function getCategoryUrl(Category $category);

    protected abstract function getProductUrl(Product $product);
}
