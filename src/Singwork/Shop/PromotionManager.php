<?php

namespace Singwork\Shop;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Singwork\Traits\EntityManagerAwareTrait;
use Doctrine\Common\Collections\Criteria;
use Singwork\Model\Entities\Shop\Product;
use Singwork\Model\Entities\Shop\Promotion;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of OrderManager
 *
 * @author matej.smisek
 */
class PromotionManager implements ContainerAwareInterface
{

    use ContainerAwareTrait;
    use EntityManagerAwareTrait;

    public function __construct()
    {
        
    }

    public function getActivePromotions(Product $product)
    {
        $now = new \DateTime('now');
        return $product->getPromotions()->matching(Criteria::create()->where(Criteria::expr()->andX(Criteria::expr()->eq("active", true), Criteria::expr()->gte('startDate', $now), Criteria::expr()->lte('endDate', $now))));
    }

    public function getActivePromotionsByTag(String $tag)
    {
        $now = (new \DateTime('now'))->getTimestamp();
        $promotions = new ArrayCollection();
        foreach ($this->getCollectionByTag($tag) as $promotion) {
            if ($promotion->getStartDate()->getTimestamp() < $now && $now < $promotion->getEndDate()->getTimestamp()) {
                $promotions->add($promotion);
            }
        }
        return $promotions;
    }

    public function getPromotionProductsByTag(String $tag)
    {
        $now = (new \DateTime('now'))->getTimestamp();
        $products = new ArrayCollection();
        foreach ($this->getCollectionByTag($tag) as $promotion) {
            if ($promotion->getStartDate()->getTimestamp() < $now && $now < $promotion->getEndDate()->getTimestamp()) {
                foreach ($promotion->getProducts() as $product) {
                    $products->add($product);
                }
            }
        }
        return $products;
    }

    protected function getCollectionByTag(String $tag)
    {
        return $this->em()->getRepository('Base:Shop\Tag')->find(strtoupper($tag))->getPromotions()->matching(Criteria::create()->where(Criteria::expr()->eq("active", true)));
    }

}
