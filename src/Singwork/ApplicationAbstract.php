<?php

namespace Singwork;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Singwork\Model\Entities\Cart\Cart;
use Singwork\Model\Shells\Cart as CartShell;

/**
 * Description of ApplicationAbstract
 *
 * @author matej.smisek
 */
class ApplicationAbstract implements ContainerAwareInterface
{

    use ContainerAwareTrait;
    //put your code here

    /**
     *
     * @var Cart
     */
    protected $cart;

    /**
     *
     * @var bool
     */
    protected $cartOpen = false;

    /**
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    protected function em($type = null)
    {
        return $this->container->get('manager_container')->getManager($type);
    }

    protected function registerListener(EventSubscriberInterface $listener)
    {
        $this->container->get('event_dispatcher')->addSubscriber($listener);
    }

    public function registerCliCommands(\Symfony\Component\Console\Application $application)
    {
        return null;
    }

    protected function prepareCart(Model\Entities\User $user)
    {
//        var_dump($this->container->get('user_handler')->getUser());
        $current_cart = $this->em()->getRepository('Base:Cart\Cart')->findOneBy(['user' => $user, 'state' => 'open']);
        if (empty($current_cart)) {
            $this->cart = new CartShell($user);
            $this->cartOpen = false;
            /*
              $this->cart = new Cart($user);
              $this->em()->persist($this->cart);
              $this->container->get('order_manager')->logCartAction($this->cart, 'cart_created');
             */
        } else {
            $this->cartOpen = true;
            $this->cart = $current_cart;
        }
    }

    public function openCart()
    {
        $this->cart = new Cart($this->container->get('user_handler')->getUser());
        $this->em()->persist($this->cart);
        $this->cartOpen = true;
        $this->container->get('order_manager')->logCartAction($this->cart, 'cart_created', new \DateTime('- 1 seconds'));
        $this->em()->flush();
    }

    /**
     * 
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    public function isCartOpen()
    {
        return $this->cartOpen;
    }

    public function getAvatarConfig()
    {
        return [
            'driver' => 'gd',      
            'generator' => \Laravolt\Avatar\Generator\DefaultGenerator::class,            
            'ascii' => true,
            // Image shape: circle or square
            'shape' => 'circle',
            // Image width, in pixel
            'width' => 100,
            // Image height, in pixel
            'height' => 100,
            // Number of characters used as initials. If name consists of single word, the first N character will be used
            'chars' => 2,
            // font size
            'fontSize' => 48,
            // convert initial letter to uppercase
            'uppercase' => true,
            // Fonts used to render text.
            // If contains more than one fonts, randomly selected based on name supplied
            'fonts' => [SINGWORK_PATH.'/vendor/laravolt/avatar/fonts/OpenSans-Bold.ttf'],
            // List of foreground colors to be used, randomly selected based on name supplied
            'foregrounds' => [
                '#FFFFFF'
            ],
            // List of background colors to be used, randomly selected based on name supplied
            'backgrounds' => [
                '#f44336',
                '#E91E63',
                '#9C27B0',
                '#673AB7',
                '#3F51B5',
                '#2196F3',
                '#03A9F4',
                '#00BCD4',
                '#009688',
                '#4CAF50',
                '#8BC34A',
                '#CDDC39',
                '#FFC107',
                '#FF9800',
                '#FF5722',
            ],
            'border' => [
                'size' => 0,
                // border color, available value are:
                // 'foreground' (same as foreground color)
                // 'background' (same as background color)
                // or any valid hex ('#aabbcc')
                'color' => 'foreground'
            ]
        ];
    }

}
