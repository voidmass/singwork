<?php
namespace Singwork\Form;
/**
 *
 * @author Matej Smisek
 */
interface FormDriver {
    
    
    public function getField($id, $value);
    
    public function getAllFields();
            
}
