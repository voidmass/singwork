<?php

namespace Singwork\Form;

use Singwork\Util\Util;
use Symfony\Component\HttpFoundation\Request;
use Singwork\Model\Entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormJSONDriver
 *
 * @author Matej Smisek
 */
class FormJSONDriver implements FormDriver
{

    /**
     *
     * @var Array
     */
    protected $_fields;
    
    protected $_scopedFields;

    /**
     *
     * @var Array
     */
    protected $_rawData = [];

    /**
     * Array of all form fields where their ID is the key
     * @var Arrray
     */
    protected $_values = [];

    /**
     * Array of entire form tree including groups
     * @var Arrray
     */
    protected $_tree = [];
    protected $_strings = [];

    /**
     *
     * @var FormData
     */
    protected $_data;

    /**
     *
     * @var type 
     */
    protected $_scope;

    public function __construct($path, FormData $data, $scope)
    {
        $this->_rawData = Util::openJsonFile($path);
        $this->_strings = $this->_rawData['_strings'];
        $this->_scope = $scope;
        $this->prepareFields($data);
    }

    public function getField($id, $value = null, $scoped = false)
    {
        
        $field = $this->_scopedFields[$id];
        if (!empty($value)) {
            $field->value = $value;
        }
        return $field;
    }

    public function getAllFields()
    {
        return [
            'fields' => $this->_fields,
            'values' => $this->_values,
            'tree' => $this->_tree,
        ];
    }

    public function prepareFields($data)
    {
        $this->_fields = [];
        $this->_values = [];
        $this->_scopedFields = [];
        $this->_data = $data;
//        $this->_tree = new FormGroup($gn, $g['title'], $g['type'], $this->getGroup($g, $group_name_new))
        $this->_tree = $this->getGroup($this->_rawData, []);
    }

    protected function getGroup($group, $group_name)
    {

        if (empty($group['fields'])) {
            $return = [];
            foreach ($group as $gn => $g) {
                if (substr($gn, 0, 1) === "_")
                    continue;
                $group_name_new = $group_name;
                $group_name_new[] = $gn;
                $result = $this->getGroup($g, $group_name_new);
                if (isset($result['active'])) {
                    $active = $result['active'];
                    $fields = $result['fields'];
                } else {
                    $fields = $result;
                }
                $return[$gn] = new FormGroup($gn, $g['title'], $g['type'], $fields, $active, $g['class']);
            }
            return $return;
        } else {
            return $this->getFields($group, $group_name);
        }
    }

    protected function getFields($group, $group_name)
    {
        if ($group['type'] === 'regular') {
            $active = true;
        } else {
            $active = false;
        }
        $return = [];
        foreach ($group['fields'] as $name => $field) {
            $input_name = 'fields';
            $input_id = '';
            foreach ($group_name as $grp) {
                $input_name .= '[' . $grp . ']';
                $input_id .= $grp . '_';
            }
            $input_name .= '[' . $name . ']';
            if ($field['multiple'] == true) {
                $input_name .= '[]';
            }
            $input_id .= $name;
            $value = $field['value'];

            $val = $this->retrieveValue($name, $group_name, $field['entity_map']);

            if (!empty($val)) {
                $value = $val;
                $active = true;
            }
            $this->_values[$input_id] = $value;
            $this->_fields[$input_id] = $this->prepareField($this->_scope . $input_id, $input_name, $field, $value);
            $this->_scopedFields[$this->_scope . $input_id] = $this->_fields[$input_id];
            $return[$name] = $this->_fields[$input_id];
        }
        return ['fields' => $return, 'active' => $active];
    }

    protected function retrieveValue($name, $group_name, $entityMap)
    {
        if ($this->_data->getFields() !== null) {
            return Util::getNeedlesFromHaystack($this->_data->getFields(), $group_name)[$name];
        }
        if ($this->_data->getEntity() !== null && $entityMap !== null) {
            $map = explode('->', $entityMap);
            return $this->retrieveValueFromMapTraverse($this->_data->getEntity(), $map);
        }
        if ($this->_data->getEntity() !== null) {
            return $this->_data->getEntity()->getProperty($name);
        }
        if ($this->_data->getEntityMap() !== null && $entityMap !== null) {
            return $this->retrieveValueFromMap($entityMap);
        }
        return null;
        $value = Util::getNeedlesFromHaystack($this->_data, $group_name);
        if (is_array($value)) {
            return $value[$name];
        }
        if ($value instanceof FormData) {
            return $value;
        }
        if ($value instanceof Entity) {
            return $value->getProperty($name);
        }
        if (is_object($value)) {
            return $this->retrieveValueFromClass($value, $name);
        }
    }

    protected function retrieveValueFromMap($entityMap)
    {
        $map = explode('->', $entityMap);
        return $this->retrieveValueFromMapTraverse($this->_data->getEntityMap()[array_splice($map, 0, 1)[0]], $map);
    }

    protected function retrieveValueFromMapTraverse(Entity $entity = null, $map = null)
    {
        if ($entity === null) {
            return null;
        }
        $propertyName = array_splice($map, 0, 1)[0];
        $value = $entity->getProperty($propertyName);
        if (empty($map) || $value === null) {
            return $value;
        } else {
            return $this->retrieveValueFromMapTraverse($value, $map);
        }
    }

    protected function retrieveValueFromClass($instance, $name)
    {
        $reflect = new \ReflectionClass($instance);
        if ($reflect->getProperty($name)->isPublic()) {
            return $instance->{$name};
        }
        $method = 'get' . $name;
        if ($reflect->hasMethod($method)) {
            return $instance->{$method}();
        }
    }

    protected function prepareErrors($error)
    {
        if (empty($error['empty'])) {
            $error['empty'] = $this->_strings['empty'];
        }
        if (empty($error['format'])) {
            $error['format'] = $this->_strings['format'];
        }
        return $error;
    }

    /**
     * 
     * @param String[] $fieldData
     * @return \FormField
     */
    protected function prepareField($id, $name, $fieldData, $value)
    {
        $field = new FormField($id, $name, $fieldData['title'], $fieldData['placeholder'], $fieldData['autocomplete'], $fieldData['class'], $fieldData['required'], $fieldData['type'], $fieldData['datatype'], $value, $fieldData['format']['type'], $fieldData['format']['data'], $this->prepareErrors($fieldData['errors']), $fieldData['render'], $fieldData['attributes'], $fieldData['entity_map']);
        if ($field->required === "condition") {
            $field->addCondition($fieldData['condition']['id'], $fieldData['condition']['value']);
        }
        return $field;
    }

}
