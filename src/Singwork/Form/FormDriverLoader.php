<?php

namespace Singwork\Form;
/**
 * Description of FormDriverLoader
 *
 * @author matej.smisek
 */
class FormDriverLoader
{

    public static function load($name, FormData $data, $lookForXML = true, $scope = null)
    {

        if ($lookForXML === true) {
            $path = PUBLIC_PATH . '/_forms/' . $name . '.xml';
            if (file_exists($path)) {
                return new FormXMLDriver($path);
            }
        }
        $path = PUBLIC_PATH . '/_forms/' . $name . '.json';
        if (file_exists($path)) {
            return new FormJSONDriver($path, $data, $scope);
        }
        $path = SINGWORK_PATH . '/_forms/' . $name . '.json';
        if (file_exists($path)) {
            return new FormJSONDriver($path, $data, $scope);
        }

        throw new \Exception("Unable to load " . $path . " form", 400, null);
    }

}
