<?php
namespace Singwork\Form;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormXMLDriver
 *
 * @author Matej Smisek
 */
class FormXMLDriver implements FormDriver {

    /**
     *
     * @var DOMXPath
     */
    protected $_xpath;

    /**
     *
     * @var DOMDocument
     */
    protected $_form;


    /**
     *
     * @var Array
     */
    protected $_fields;

    /**
     * Array of all form fields where their ID is the key
     * @var Arrray
     */
    protected $_values = [];

    
    protected $_strings = [];
    /**
     * 
     * @param type $name
     * @throws \Exception
     */
    public function __construct($path) {
        if (file_exists($path)) {
//            $this->_form = simplexml_load_file($path);  
            $this->_form = new \DOMDocument();
            $this->_form->load($path);
            $this->validateXML();
            $this->_xpath = new \DOMXPath($this->_form);
            $this->fetchStrings();
        } else {
            throw new \Exception("Unable to load " . $path . " form", 400, null);
        }
    }

    protected function fetchStrings() {
        $strings = $this->_xpath->query('/form/strings/child::node()');
        /* @var $node DOMNode */
        foreach ($strings as $node) {
            $this->_strings[$node->nodeName] = $node->textContent;
        }
    }

    private function validateXML() {
        libxml_use_internal_errors(true);
//        die(core()->getFrameworkPath() . '/_form/form.xsd');
        if (!$this->_form->schemaValidate(core()->getFrameworkPath() . '/_form/form.xsd')) {
            print '<b>DOMDocument::schemaValidate() Generated Errors!</b>';
            $this->libxml_display_errors();
        }
    }

    public function getField($id, $value) {
        $el = $this->_xpath->query("/form//field[id='" . $id . "'][1]")->item(0);
        return $this->prepareField($el, $value);
    }

    public function getAllFields() {
        $this->_fields = [];
        $this->_values = [];
        $content = $this->_xpath->query('/form/fields/child::node()');
        $this->traverseGroups($content, $_POST['fields']);
        return [
            'fields' => $this->_fields,
            'values' => $this->_values
        ];
    }

    public function saveToSession() {
        $fields = $this->_xpath->query('/form//field');
        foreach ($fields as $v) {
            $_SESSION['forms'][(string) $this->_name][(string) $v->name]['value'] = $_POST['fields'][(string) $v->name];
        }
    }

    protected function prepareErrors($node) {
        $error = [];
        /* @var $e DOMElement */
        foreach ($this->_xpath->query('errors/error', $node) as $e) {
            $error[(string) $e->getAttribute('type')] = $e->textContent;
        }
        if (empty($error['empty'])) {
            $error['empty'] = $this->_strings['input_empty'];
        }
        if (empty($error['format'])) {
            $error['format'] = $this->_strings['input_format'];
        }
        return $error;
    }

    /**
     * 
     * @param DOMNode $node
     * @return \FormField
     */
    protected function prepareField($node, $value) {
        $id = ((string) $this->getChild('id', $node)->textContent);
        $type = ((string) $this->getChild('type', $node)->textContent);
        $required = ((string) $this->getChild('required', $node)->textContent);
        $format = $this->getChild('format/*[1]', $node);
        $format_type = ($format->nodeName);
        $format_content = ((string) $format->textContent);
        $field = new FormField($value, $id, $type, $required, $this->prepareErrors($node), $format_type, $format_content);
        if ($field->required === "condition") {
            $condition = $this->getChild('condition', $node);
            $field->addCondition($condition->attributes->getNamedItem('field-id')->nodeValue, $condition->textContent);
        }
        return $field;
    }

    /**
     * 
     * @param type $el
     * @param type $node
     * @return DOMNode
     */
    protected function getChild($el, $node) {
        return $this->_xpath->query($el, $node)->item(0);
    }

    /**
     * 
     * @param DomNodeList $groups
     */
    protected function traverseGroups($groups, $post) {
        /* @var $node DOMNode */
        foreach ($groups as $node) {
            if ($node->nodeName === 'group') {
                $this->traverseGroups($node->childNodes, $post[$node->attributes->getNamedItem('name')->nodeValue]);
            } elseif ($node->nodeName === 'field') {
                $value = $post[$this->getChild('name', $node)->textContent];
                $field = $this->prepareField($node, $value);
                $this->_values[$field->id] = $value;
                $this->_fields[] = $field;
            }
        }
    }

    /**
     * 
     * @param DOMNode $node
     * @param type $value
     * @return String
     */
    public function verify($node, $value) {
        if ($node === false) {
            return 'No such Field Defined';
        }
        $error = $this->prepareErrors($node);
        $field = $this->prepareField($node, $value);

        if (($value == '' && $value == null) && $field->required === "false") {
            return true;
        }
        if ($field->required === "true" && !self::checkValue($value)) {
            return $error['empty'];
        }
//        echo ((string)$field).'<>'.$value;
        if ($field->required === "condition") {
            $condition = $this->getChild('condition', $node);
            $field->addCondition($condition->attributes->getNamedItem('field-id')->nodeValue, $condition->textContent);
            $this->_conditional[] = $field;
        }
        switch ($field->type) {
            case 'number':
                if (!self::numberValue($value)) {
                    return $error['format'];
                }
                break;
            case 'string':
            default:
                break;
        }
        switch ($field->format_type) {
            case 'none':
            default:
                break;
            case 'regex':
                if (!preg_match('/^' . $field->format_content . '$/u', $value)) {
                    return $error['format'];
                }
                break;
            case 'email':
                if (!self::emailValue($value)) {
                    return $error['format'];
                }
                break;
        }
        /*
          if ($this->getChild('format/array', $f) !== null && $this->getChild('type', $f)->textContent == 'array') {
          $this->arrayValue($this->getChild('format/array', $f), $value, $field);
          }
         */
        return true;
    }

    private function libxml_display_errors() {
        $errors = libxml_get_errors();
        foreach ($errors as $error) {
            print $this->libxml_display_error($error);
        }
        libxml_clear_errors();
    }

    private function libxml_display_error($error) {
        $return = "<br/>\n";
        switch ($error->level) {
            case LIBXML_ERR_WARNING:
                $return .= "<b>Warning $error->code</b>: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "<b>Error $error->code</b>: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "<b>Fatal Error $error->code</b>: ";
                break;
        }
        $return .= trim($error->message);
        if ($error->file) {
            $return .= " in <b>$error->file</b>";
        }
        $return .= " on line <b>$error->line</b>\n";

        return $return;
    }

}
