<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Singwork\Form;

/**
 * Description of FormGroup
 *
 * @author matej.smisek
 */
class FormGroup
{
    public $id;
    
    public $title;
    
    public $type;
    
    public $fields;
    
    public $active;
    
    public $class;
    
    public function __construct($id, $title, $type, $fields, $active, $class)
    {
        $this->id = $id;
        $this->title = $title;
        $this->type = $type;
        $this->fields = $fields;
        $this->active = $active;
        $this->class = $class;
    }
    
}
