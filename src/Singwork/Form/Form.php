<?php

namespace Singwork\Form;

/**
 * Description of Form
 *
 * @author Matej Smisek
 */
class Form
{

    /**
     *
     * @var FormField[] 
     */
    protected $_fields;
    protected $_name;

    /**
     *
     * @var FormDriver
     */
    protected $_driver;

    /**
     *
     * @var FormData
     */
    protected $_formData;

    /**
     * 
     */
    protected $_strings;
    protected $_errorStack = [];

    /**
     * Contains FormFields that will be post-checked for conditional formatting
     * @var FormField[]
     */
    protected $_conditional = [];

    /**
     * 
     * @param type $name
     * @throws \Exception
     */
    public function __construct(FormData $data, $name, $scope = null)
    {
        $this->_name = $name;
        $this->_formData = $data;
        $this->_driver = FormDriverLoader::load($name, $data, true, $scope);
    }

    /**
     * Use verifyField instead
     * @deprecated since version 3
     *
     *      */
    public function verifyFromPost()
    {
        $this->verifyField();
    }

    public function verifyCheck()
    {

        $output = [];
        foreach ($this->_formData->getFields() as $id => $value) {
            if (true !== $result = $this->verify($this->_driver->getField($id, $value), true)) {
                $output[] = ['id' => $id, 'message' => $result, 'status' => 'invalid'];
            } else {
                $output[] = ['id' => $id, 'message' => '', 'status' => 'valid'];
            }
        }
        return $output;
    }

    /**
     * Use verifyAll instead
     * @deprecated since version 3
     */
    public function verifyAllFromPost()
    {
        $this->verifyAll();
    }

    public function verifyAll()
    {
        $fields = $this->_driver->getAllFields();
        foreach ($fields['fields'] as $field) {
            $result = $this->verify($field);
            if ($result !== true) {
                $this->errorStack(['id' => $field->id, 'message' => $result]);
            }
        }
        $this->verifyConditionals($fields['values']);
    }

    public function errorStack($data)
    {
        $this->_errorStack[] = $data;
    }

    public function checkStack($callback = false)
    {
        if (!empty($this->_errorStack)) {
            if ($this->_formData->getType() == "check") {
                $this->error($this->_errorStack[0]);
            } else {
                $this->error(array('fields' => $this->_errorStack));
            }
        } else {
            if ($callback !== false) {
                $this->valid($callback);
            }
        }
    }

    public function getErrorStack()
    {
        return $this->_errorStack;
    }

    /**
     * 
     * @param FormField $field
     * @return String
     */
    public function verify($field, $conditionRun = false)
    {


        if ($field === false) {
            return 'No such Field Defined';
        }
        if (($field->value == '' && $field->value == null) && $field->required === "false") {
            return true;
        }
        if (($field->required === "true" || $conditionRun) && !self::checkValue($field->value)) {
            return $field->error['empty'];
        }
//        echo ((string)$field).'<>'.$field->_value;
        if ($field->required === "condition" && $conditionRun === false) {
            $this->_conditional[] = $field;
            return true;
        }
        $value = null;
        switch ($field->datatype) {
            case 'integer':
                if (!self::numberValue($field->value)) {
                    return $field->error['format'];
                }
                $value = intval($field->value);
                break;
            case 'float':
                if (!self::numberValue($field->value)) {
                    return $field->error['format'];
                }
                $value = floatval($field->value);
                break;
            case 'email':
                if (!self::emailValue(strval($field->value))) {
                    return $field->error['format'];
                }
                break;
            case 'string':
            default:
                $value = strval($field->value);
                break;
        }
        switch ($field->format_type) {
            case 'none':
            default:
                break;
            case 'regex':
                if (!preg_match('/^' . $field->format_content . '$/u', $field->value)) {
                    return $field->error['format'];
                }
                break;
        }
        return true;
    }

    protected function verifyConditionals($values)
    {
        foreach ($this->_conditional as $field) {
            if ((preg_match('#' . $field->condition_value . '#ui', $values[$field->condition_id]) === 1)) {
                $result = $this->verify($field, true);
                if ($result !== true) {
                    $this->errorStack(['id' => $field->id, 'message' => $result]);
                }
            }
        }
    }

    public static function checkValue($value)
    {
        return !empty(trim($value));
    }

    public static function numberValue($value)
    {
        return is_numeric(trim($value));
    }

    public static function emailValue($value)
    {
        return (filter_var(trim($value), FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', trim($value)));
    }

    public function saveToSession()
    {
        
    }

    /**
     * 
     * @param String $err
     * @deprecated since version 3
     */
    public function error($err)
    {
        header('Content-type: application/json');
        echo json_encode(array('result' => 'success', 'status' => 'invalid', 'data' => $err));
        die();
    }

    /**
     * 
     * @param String $message
     * @deprecated since version 3
     */
    public function valid($message)
    {
        header('Content-type: application/json');
        echo json_encode(array('result' => 'success', 'status' => 'valid', 'data' => ['message' => $message]));
        die();
    }

    public function getFields(): array
    {
        return $this->_fields;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getDriver(): FormDriver
    {
        return $this->_driver;
    }

    public function getFormData(): FormData
    {
        return $this->_formData;
    }

}
