<?php

namespace Singwork\Form;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormField
 *
 * @author Matej Smisek
 * 
 * 
 * "title": "name",
  "type": "text",
  "class": "",
  "datatype": "string",
  "required": "true",
  "format": {
  "type": "regex",
  "data": "[a-zA-Z0-9_]{1,10}"
  },
  "errors": {
  "format": "Může být max 10 znaků dlouhé a obsahovat pouze alfa-numerické znaky a podtržítko"
  }
 */
class FormField
{

    public $id;
    public $name;
    public $title;
    public $placeholder;
    public $autocomplete;
    public $class;
    public $required;
    public $type;
    public $datatype;
    public $value;
    public $format_type;
    public $format_content;
    public $condition_id;
    public $condition_value;
    public $error;
    public $render;
    public $attributes;
    public $entityMap;

    public function __construct($id, $name, $title, $placeholder, $autocomplete, $class, $required, $type, $datatype, $value, $format_type, $format_content, $error, $render, $attributes, $entityMap)
    {
        $this->id = $id;
        $this->name = $name;
        $this->title = $title;
        $this->placeholder = $placeholder;
        $this->autocomplete = $autocomplete;
        $this->class = $class;
        $this->required = $required;
        $this->type = $type;
        $this->datatype = $datatype;
        $this->value = $value;
        $this->format_type = $format_type;
        $this->format_content = $format_content;
        $this->error = $error;
        $this->render = $render;
        $this->attributes = $attributes;
        $this->entityMap = $entityMap;
    }

    public function addCondition($_condition_id, $_condition_value)
    {
        $this->condition_id = $_condition_id;
        $this->condition_value = $_condition_value;
    }

    public function setSelectOptionsFromEntity($array, $titleMethod = 'getName', $valueMethod = 'getId', $default = [])
    {
        $this->attributes['options'] = $default;
        foreach ($array as $option) {
            $this->attributes['options'][] = [
                'value' => $option->{$valueMethod}(),
                'title' => $option->{$titleMethod}()
            ];
        }
    }

    public function setSelectOptionsFromArray($array)
    {
        $this->attributes['options'] = [];
        foreach ($array as $value => $option) {
            $this->attributes['options'][] = [
                'value' => $value,
                'title' => $option
            ];
        }
    }

    public function __toString()
    {
        return

                'Name: ' . $this->name . '<>' .
                'Value: ' . $this->value . '<>' .
                'Id: ' . $this->id . '<>' .
                'Req: ' . $this->required . '<>' .
                'Type: ' . $this->type . '<>' .
                'Format_type: ' . $this->format_type . '<>' .
                'Format_cont: ' . $this->format_content . '<>' .
                'Cond_id: ' . $this->condition_id . '<>' .
                'Cond_val: ' . $this->condition_value . '<>';
    }

}

?>
