<?php

namespace Singwork\Form;

use Singwork\Util\Util;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormBuilder
 *
 * @author Matej Smisek
 */
class FormBuilder
{

    protected $_data;
    public $_tree;
    /**
     *
     * @var FormField[]
     */
    public $_fields;

    public function __construct($name, FormData $data, $scope = null)
    {
        $driverData = FormDriverLoader::load($name, $data, false, $scope)->getAllFields();
        $this->_tree = $driverData['tree'];
        $this->_data = $driverData['values'];
        $this->_fields = $driverData['fields'];
    }

    public function addField($id, $field, $group = null)
    {
        $this->_fields[$id] = $field;
        if ($group === null) {
            $this->_tree[] = $field;
        } else {
            $this->_tree[$group]->fields[] = $field;
        }
    }

    public function setData($data)
    {
        $this->_data = $data;
    }

    public function getData()
    {
        return $this->_data;
    }

    public function construct()
    {
        return $this->getGroup($this->_tree)['html'];
    }

    public function getForm()
    {
        if (isset($this->_tree['fields'])) {
            return $this->_tree['fields'];
        }
        return $this->_tree;
    }

    protected function getGroup($group)
    {
        if ($group instanceof FormGroup) {
            return $this->getFields($group);
        }
        $return = '';
        foreach ($group as $g) {
            $group_result = $this->getGroup($g);
            $return .= $this->_group_template->render([
                'content' => $group_result['html'],
                'group' => $g->id,
                'type' => $g->type,
                'title' => $g->title,
                'active' => $group_result['active']
            ]);
        }
        return ['html' => $return, 'active' => true];
    }

    protected function getFields(FormGroup $group)
    {
        $return = '';
        if ($group->type === 'regular') {
            $active = true;
        } else {
            $active = false;
        }
        foreach ($group->fields as $field) {

            if (!empty($field->value)) {
                $active = true;
            }
            $return .= $this->_field_template->render([
                'field' => $field,
                'group' => $group]);
        }
        return ['html' => $return, 'active' => $active];
    }

}
