<?php

namespace Singwork\Form;

/**
 * Description of FormData
 *
 * @author matej.smisek
 */
class FormData implements \ArrayAccess
{

    /**
     *
     * @var array
     */
    protected $_fields = null;

    /**
     *
     * @var \Singwork\Model\Entity
     */
    protected $_entity = null;
    
    /**
     *
     * @var \Singwork\Model\Entity[]
     */
    protected $_entityMap = null;
    
    protected $_type;

    public function __construct($type)
    {
        $this->_type = $type;
    }

    public function setFields($fields)
    {
        $this->_fields = $fields;
    }

    public function setEntity($entity)
    {
        $this->_entity = $entity;
    }
    public static function void($type = 'new')
    {
        $obj = new static($type);
        return $obj;
    }
    public static function fromFields($type, $fields)
    {
        $obj = new static($type);
        $obj->setFields($fields);
        return $obj;
    }   
    public static function fromEntity($type, $entity)
    {
        $obj = new static($type);
        $obj->setEntity($entity);
        return $obj;
    }
    public static function fromEntityMap($type, $entities)
    {
        $obj = new static($type);
        $obj->setEntityMap($entities);
        return $obj;
    }
    
    public function getFields()
    {
        return $this->_fields;
    }

    public function getEntity()
    {
        return $this->_entity;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function offsetExists($offset): bool
    {
        if (isset($this->_entity)) {
            return $this->_entity->hasProperty($offset);
        }
        return isset($this->_fields[$offset]);
    }

    public function offsetGet($offset)
    {
        if (isset($this->_entity)) {
            return $this->_entity->getProperty($offset);
        }
        return $this->_fields[$offset];
    }

    /**
     * TODO setProperty global for Entity
     * @param type $offset
     * @param type $value
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (isset($this->_entity)) {
            return $this->_entity->hasProperty($offset);
        }
        $this->_fields[$offset] = $value;
    }

    /**
     * TODO unsetEntity from global
     * 
     * @param type $offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        if (isset($this->_entity)) {
            return $this->_entity->hasProperty($offset);
        }
        unset($this->_fields[$offset]);
    }
    
    public function getEntityMap()
    {
        return $this->_entityMap;
    }

    public function setEntityMap($entityMap)
    {
        $this->_entityMap = $entityMap;
        return $this;
    }


}
