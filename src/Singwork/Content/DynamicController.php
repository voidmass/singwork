<?php
namespace Singwork\Content;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IndexController
 *
 * @author Matej Smisek
 */
class DynamicController extends Controller {
    protected $_page;
    public function __construct($dynamic_page) {
        parent::__construct();
        $this->_page = $dynamic_page;
        
    }
    public function run() {
        $this->injectVariables();
        $this->_view->content = $this->_page['content'];
    }
    private function injectVariables() {
        $raw = $this->_page['content'];
        $matches = array();
        if (preg_match_all('/{menu:([A-Za-z0-9_]{1,})}/', $raw, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $val) {
            $raw = str_replace('{menu:'.$val[1].'}', core()->menu()->printMenu($val[1]), $raw);
            }        
        }
//        $matches = array();
//        if (preg_match('/{menu:([A-Za-z0-9_]{1,})}/', $raw, $matches)) {
//            foreach ($matches as $val) {
//            $raw = str_replace('{menu:'.$val[1].'}', core()->menu()->printMenu($val[1]), $raw);
//            }        
//        }
        $raw = str_replace('{pageClasses}', $this->_page['css_classes'], $raw);
        $raw = str_replace('{url}', core()->getUrl()->getCanon(), $raw);
        $this->_page['content'] = $raw;
    }
    
}

?>
