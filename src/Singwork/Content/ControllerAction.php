<?php
namespace Singwork\Content;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControllerAction
 *
 * @author Matej Smisek
 */
abstract class ControllerAction extends Controller {

    /**
     *
     * @var String
     */
    protected $_method;
    protected $_stoppedPropagation = false;

    public function globalRun() {
        
    }
    public function globalEnd() {
        
    }

    public function preInit() {
        parent::preInit();
        $this->_method = $this->formatAction(core()->getUrlProcessor()->getPage()->getUrl());
        if (!method_exists($this, $this->_method)) {
            throw new \Exception('Invalid Action URL', "503", null);
        }
    }

    public function run() {                
        $this->globalRun();
        if ($this->_stoppedPropagation) {
            return;
        }
        $this->{$this->_method}();
        $this->globalEnd();
    }

    protected function formatAction($url) {
        return strtr($url, array('_' => '', '-' => '', ' ' => '', '/' => 'root')) . 'Action';
    }

    protected function stopPropagation() {
        $this->_stoppedPropagation = true;
    }

}

?>
