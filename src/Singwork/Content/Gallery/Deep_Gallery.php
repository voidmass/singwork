<?php
namespace Singwork\Content\Gallery;

use Singwork\Table;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Deep_Gallery
 *
 * @author Matej Smisek
 */
class Deep_Gallery {
    
    protected  $_ajaxOutput;
    
    public function __construct($id) {
        $d = (new Table\DeepGallery())->getGallery($id);
        $i = 0;
        while ($data = $d->fetch()) {    
            $this->_ajaxOutput[$i]['small'] = $data['image_small']; 
            $this->_ajaxOutput[$i]['large'] = $data['image_large']; 
            $this->_ajaxOutput[$i]['text'] = $data['title'];
            $i++;
        }
    }
    public function getAjax() {
        return $this->_ajaxOutput;
    }

}

?>
