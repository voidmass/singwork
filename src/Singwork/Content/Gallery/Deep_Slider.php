<?php
namespace Singwork\Content\Gallery;

use Singwork\Table;
class Deep_Slider {
    
    protected  $_ajaxOutput;
    
    public function __construct($id) {
        $d = (new Table\DeepSlider())->getSlider(core()->getLanguage());
        $this->_ajaxOutput['total'] = 0;
        while ($data = $d->fetch()) {    
            $this->_ajaxOutput['panes'][$this->_ajaxOutput['total']] = '
    <div id="#p'.$this->_ajaxOutput['total'].'" class="pane">
    <div class="image">
        <img  src="'.$data['image'].'" alt="news1"/>
    </div>
    <div class="textarea">'.$data['html'].'</div>
    </div>';
            $this->_ajaxOutput['total']++;
        }
    }
    public function getAjax() {
        return $this->_ajaxOutput;
    }

}
?>
