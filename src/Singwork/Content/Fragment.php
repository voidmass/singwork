<?php
namespace Singwork\Content;

use Singwork\Util\Util;
use Singwork\Exception\RuntimeSingException;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Fragment
 *
 * @author Matej Smisek
 */
class Fragment {

    /**
     *
     * @var String
     */
    protected $_name;

    /**
     *
     * @var String
     */
    protected $_fileName;

    /**
     *
     * @var String Full path to the fragment file
     */
    protected $_fullPath;

    /**
     *
     * @var Array array of values to replace inside the fragment
     */
    protected $_variables = [];

    /**
     * 
     * @param String $name Name of the .phtml file. Expected locaions are in _fragments folder on public path eventualy on framework path
     */
    function __construct($name, $javascript = false) {
        $this->_name = $name;
        if ($javascript === true) {
            $this->_fileName = $this->_name . '.js';
        } else {
            $this->_fileName = $this->_name . '.phtml';
        }
        if (!file_exists(core()->getPublicFolder() . '/_fragments/' . $this->_fileName)) {
            if (!file_exists(core()->getFrameworkPath() . '/_fragments/' . $this->_fileName)) {
                throw new RuntimeSingException('Fragment ' . $this->_fileName . ' not found on any path');
            } else {
                $this->_fullPath = core()->getFrameworkPath() . '/_fragments/' . $this->_fileName;
            }
        } else {
            $this->_fullPath = core()->getPublicFolder() . '/_fragments/' . $this->_fileName;
        }
    }

    public function getVariables() {
        return $this->_variables;
    }

    public function setVariables(Array $variables) {
        $this->_variables = $variables;
        return $this;
    }

    public function render() {
        ob_start();
        include $this->_fullPath;
        $content = ob_get_contents();
        ob_end_clean();
        $this->injectVariables($content);
        $this->injectTranslation($content);
        return preg_replace('#{{.*?}}#ui', '', $content);
    }

    protected function injectVariables(&$content) {
        if (preg_match_all('#{{(?!t:)(.*?)}}#ui', $content, $matches) !== false) {
            foreach ($matches[1] as $word) {
                $needles = explode('.', $word);
                $content = str_replace('{{' . $word . '}}', t(Util::getNeedlesFromHaystack($this->_variables, $needles), true), $content);
            }
        }
//        foreach ($this->_variables as $variable => $value) {
//            $content = str_replace('{{' . $variable . '}}', $value, $content);
//        }
    }

    protected function injectTranslation(&$content) {
        if (preg_match_all('#{{t:(.*?)}}#ui', $content, $matches) !== false) {
            foreach ($matches[1] as $word) {
                $content = str_replace('{{t:' . $word . '}}', t($word), $content);
            }
        }
    }
    
    protected function constructUrl($id) {
        
    }
}
