<?php 
namespace Singwork\Content\Translate;


use Singwork\Table;
use Singwork\Util\Util;
use Singwork\Util\JsonFileLoader;
use Symfony\Component\HttpFoundation\Request;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Translate
 *
 * @author Matej Smisek
 */
class Translate {

    /**
     * 
     * 
     * @var Array Associative array of translated keywords
     */
    protected $_translation = [];

    public function __construct() {
//        $language = $request->getSession()->getBag('application')->get('language',$_ENV['config']['language']['default_language']);
        $language = $_ENV['config']['language']['default_language'];
        switch ($_ENV['config']['language']['language_engine']) {
            default:
            case 'file':
                $this->loadFromFile($language);
                break;
            case 'db':
                $this->loadFromDb($language);
                break;
        }
    }

    protected function loadFromFile($language) {       
        try {
            $this->_translation = JsonFileLoader::load($language . '.json', [PUBLIC_PATH.'/_config/translation']);
        } catch (Symfony\Component\Config\Exception\FileLocatorFileNotFoundException $e) {
            slog_warn('Language file for ' . core()->getLanguage() . ' does not exist.');
        }    
    }

    protected function loadFromDb($language) {
        return  [];
        $r = (new Table\Translate())->getLanguage(core()->getLanguage());
        while ($row = $r->fetch()) {
            $this->_translation[$row['id']] = $row['text'];
        }
    }

    public function getWord($word, $silent = false) {
//        var_dump($word).'<br /><br />';
        if (!isset($this->_translation[strtolower($word)]) || empty($this->_translation[strtolower($word)])) {
            if ($silent !== true) {
                slog_warn('Keyword #' . strtolower($word) . '# not defined for '  . ' language not defined');
            }
            return $word;
        }
        return $this->_translation[strtolower($word)];
    }

    public function debug() {
        echo 'Debugging translate object<br />';
        print_r($this->_translation);
    }

}

?>
