<?php
namespace Singwork\Content\Menu;

/**
 *
 * @author Balgor
 */
interface MenuInterface
{
    public function processMenu();
}
