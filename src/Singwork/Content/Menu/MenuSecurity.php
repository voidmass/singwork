<?php

namespace Singwork\Content\Menu;

use Singwork\Startup\UrlPart;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MenuSecurity
 *
 * @author Matej Smisek
 */
class MenuSecurity extends Menu
{

    public function __construct($menu)
    {
        parent::__construct($menu);
    }

    protected function walk(&$nodes)
    {
        foreach ($nodes as $key => &$node) {
            if (isset($node['nodes'])) {
                $this->walk($node['nodes']);
                if (empty($node['nodes'])) {
                    unset($nodes[$key]);
                    continue;
                }
            } else {
                if (!$this->checkAccess($node)) {
                    unset($nodes[$key]);
                    continue;
                }
                $this->constructItem($node);
            }
        }
    }

    /**
     * 
     * @param Array $item
     */
    protected function checkAccess($item)
    {        
        $bag = new \Symfony\Component\HttpFoundation\ParameterBag($this->container->get('router')->getRouteCollection()->get($item['route'])->getDefaults());
        $bag->set('_route', $item['route']);
        return $this->container->get('user_handler')->verifyAccess($bag);
    }

}

?>
