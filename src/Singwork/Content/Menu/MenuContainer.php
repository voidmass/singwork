<?php

namespace Singwork\Content\Menu;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
/**
 * Description of MenuContainer
 *
 * @author Balgor
 */
class MenuContainer implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    /**
     *
     * @var MenuInterface[]
     */
    protected $_handlers;
    protected $_menuData = [];

    public function __construct()
    {
        $this->_handlers = [
            'menu' => '\Singwork\Content\Menu\Menu',
            'security' => '\Singwork\Content\Menu\MenuSecurity',
            'security_simple' => '\Singwork\Content\Menu\MenuSecuritySimple',
        ];
    }

    public function getMenu($id)
    {
        $menuData = $this->getMenuData($id);

        if (isset($this->_handlers[$menuData['handler']])) {           
            $menu = new $this->_handlers[$menuData['handler']]($menuData);
        }
        if ($menu instanceof ContainerAwareInterface) {
            $menu->setContainer($this->container);
        }
        if ($menu instanceof MenuInterface) {
            return $menu->processMenu();
        }
    }

    public function getMenuData($id)
    {
        return ($this->_menuData[$id] ?? $this->parseMenu($id));
    }

    public function parseMenu($id)
    {
        $this->_menuData[$id] = \Singwork\Util\JsonFileLoader::load($id . '.json', [PUBLIC_PATH . '/_config/menu']);
        return $this->_menuData[$id];
    }
    protected function walkGroup() {
        
    }
    protected function walkNodes($nodes) {
        $return = [];
        foreach ($nodes as $key => $node) {
            if ($node['type'] === 'submenu') {
                
            }
        }
        return $return;
    }
    public function registerHandler($keyword, $className)
    {
        $this->_handlers[$keyword] = $className;
    }


}
