<?php
namespace Singwork\Content\Menu;

use Singwork\Startup\UrlPart;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MenuSecuritySimple
 *
 * @author Matej Smisek
 */
class MenuSecuritySimple extends Menu {


    
    public function __construct($menu) {
        parent::__construct($menu);
    }
    public function printMenu() {
        $submenu = [];
        $output = "";          
        $items = $this->_tableItems->getItems($this->_menu['id'], core()->getLanguage());
        $cnt = 0;        
        while ($item = $items->fetch()) {
            if ($item['type'] == "internal") {
                try {
                    $page = $this->_tableUrl->getId($item['url']);
                    core()->getUserHandler()->verifyAccess(new UrlPart($page));
                } catch (\Exception $e) {
                    continue;
                }
            }
            $state = "";
            if ($cnt == 0) {
                $state .= " first";                
            }
            if ($cnt == ($items->_numRows - 1)) {
                $state .= " last";
                
            }
            $output .= $this->constructItem($item, $state, $submenu);
            
            $cnt++;
        }        
        $final = str_replace(array("{content}","{menuClass}","{submenu}"),array($output,$this->_menu['id'],$this->constructSubmenu($submenu)),$this->_menu['menu_html']);
        return $final;
    }
}

?>
