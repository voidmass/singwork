<?php

namespace Singwork\Content\Menu;

use Singwork\Table;
use Singwork\Util\Util;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Description of Menu
 *
 * @author Matej Smisek
 */
class Menu implements ContainerAwareInterface, MenuInterface
{

    use ContainerAwareTrait;

    protected $_data;

//    protected $_classes;

    public function __construct($data)
    {
        $this->_data = $data;
//        $this->_classes = json_decode(Util::defaultValue($this->_menu['classes'], '{"selected": "selected", "hasdropdown": "has-dropdown", "hassubmenu": "hasSubmenu", "spacer": "spacer"}'), true);
    }

    public function processMenu()
    {
        $this->walk($this->_data['nodes']);
        return $this->_data['nodes'];
    }

    protected function walk(&$nodes)
    {
        foreach ($nodes as $key => &$node) {
            if (isset($node['nodes'])) {
                $this->walk($node['nodes']);
            } else {
                $this->constructItem($node);
            }
        }
    }

    protected function constructItem(&$item)
    {
        if (!empty($item['route'])) {
            $item['active'] = $this->isActive($item['route']);
            $item['url'] = $this->generateUrl($item['route']);
        }
    }

    protected function generateUrl($route_id)
    {
        return $this->container->get('router')->generate($route_id, []);
    }

    protected function isActive($route_id)
    {
        if (empty($this->container->get('request')->get('parent_route'))) {
            if (empty($this->container->get('request')->get('_route'))) {
                return false;
            }
            $currentRoute = $this->container->get('request')->get('_route');
        } else {
            $currentRoute = $this->container->get('request')->get('parent_route');
        }
        return $currentRoute == $route_id;
    }

}

?>
