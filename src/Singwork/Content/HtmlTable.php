<?php
namespace Singwork\Content;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HtmlTable
 *
 * @author Matej Smisek
 */
class HtmlTable {

    /**
     *
     * @var string
     */
    protected $_tableId;

    /**
     *
     * @var array
     */
    protected $_tableClass = [];

    /**
     *
     * @var array
     */
    protected $_rowClass = [];

    /**
     *
     * @var array
     */
    protected $_columnClass = [];

    /**
     *
     * @var array
     */
    protected $_columnData = [];

    /**
     *
     * @var array
     */
    protected $_rows = [];

    /**
     *
     * @var array
     */
    protected $_columnFilter = [];

    /**
     *
     * @var array
     */
    protected $_header = [];
    protected $_rowIdPrefix;
    protected $_rowIdColumn;
    protected $_rowDataName;
    protected $_rowDataColumn;

    /**
     *
     * @var Array
     */
    protected $_alternation = [];

    public function __construct($rows) {
        $this->_rows = $rows;
    }

    public function addTableClass($class) {
        $this->_tableClass = array_merge($this->_tableClass, explode(' ', $class));
    }

    public function addRowClass($class) {
        $this->_rowClass = array_merge($this->_rowClass, explode(' ', $class));
    }

    public function addColumnClass($column, $class) {
        $this->_columnClass[$column] = array_merge($this->_columnClass[$column], explode(' ', $class));
    }

    public function setRowId($prefix, $column) {
        $this->_rowIdPrefix = $prefix;
        $this->_rowIdColumn = $column;
    }

    public function setAlternation($alt) {
        $this->_alternation = $alt;
    }

    public function setHeader($header) {
        $this->_header = $header;
    }

    public function setFilter($filter) {
        $this->_columnFilter = $filter;
    }

    public function setTableId($id) {
        $this->_tableId = $id;
    }

    public function setTableData($name, $value) {
        $this->_rowDataName = $name;
        $this->_rowDataColumn = $value;
    }

    public function getTable() {
        $table = (new Tag('table'))->addClass(self::constructClass($this->_tableClass))->setId($this->_tableId);
        $head = (new Tag('thead'))->append($this->constructHeader());
        $body = $this->constructBody();
        $table->append($head)->append($body);
        return $table->toString();
    }

    protected function constructHeader() {
        $row = new Tag('tr');
        $index = 0;
        foreach ($this->_header as $k => $v) {
            $td = new Tag('th');
            $td->property('colspan', $v);
            $td->html($k);
            $td->addClass(self::constructClass($this->_columnClass[$index]));
            $row->append($td);
            $index++;
        }
        return $row;
    }

    protected function constructBody() {
        $body = new Tag('tbody');
        $alt = 0;
        foreach ($this->_rows as $dataRow) {
            $row = new Tag('tr');
            if (isset($this->_rowIdPrefix)) {
                $row->setId($this->_rowIdPrefix . $dataRow[$this->_rowIdColumn]);
            }
            if (isset($this->_rowDataName)) {
                $row->data($this->_rowDataName, $dataRow[$this->_rowDataColumn]);
                $row->addClass('data-id');
            }
            if (count($this->_alternation) > 0) {
                $row->addClass($this->_alternation[$alt]);
            }
            if (isset($this->_columnFilter)) {
                $data = self::arrayFilter($dataRow, $this->_columnFilter);
            } else {
                $data = $dataRow;
            }
            $index = 0;
            foreach ($data as $content) {
                $td = new Tag('td');

                $td->addClass(self::constructClass($this->_columnClass[$index]));
                if (is_array($content)) {
                    $td->html($content['content']);
                    foreach ($content['data'] as $key => $value) {
                        $td->data($key, $value);
                    }
                } else {
                    $td->html($content);
                }
                $row->append($td);
                $index++;
            }
            $body->append($row);
            if (count($this->_alternation) > 0) {
                if ($alt % count($this->_alternation) == count($this->_alternation) - 1) {
                    $alt = 0;
                } else {
                    $alt++;
                }
            }
        }
        return $body;
    }

    protected static function constructClass($classes) {
        if (empty($classes)) {
            return '';
        }
        return implode(' ', $classes);
    }

    /**
     * 
     * @param array $row
     * @param array $filter
     */
    public static function arrayFilter(array $row, array $filter, $extract_content = false) {
        $output = [];
        foreach ($filter as $key => $action) {
            if (is_callable($action)) {
                if (array_key_exists($key, $row)) {
                    $result = $action($row[$key]);
                } else {
                    $result = $action($row);
                }
                if ($extract_content === true && is_array($result)) {
                    $output[] = $result['content'];
                } else {
                    $output[] = $result;
                }
                continue;
            }
            $output[] = $row[$key];
        }
        return $output;
    }
    /**
     * 
     * @param array $rows
     * @param array $filter
     * @param boolean $extract_content
     * @return array
     */
    public static function arrayFilterAll(array $rows, array $filter, $extract_content = false) {
        $output = [];
        foreach ($rows as $row) {
            $output[] = self::arrayFilter($row, $filter, $extract_content);
        }
        return $output;
    }  
}

?>
