<?php
namespace Singwork\Content;

use Singwork\Table;
use Singwork\Util\Util;
/**
 * Description of Gallery
 *
 * @author Matej Smisek
 */
class Gallery {
    
    protected $_id;
    protected $_identifier;
    public function __construct($identifier) {
        $this->_identifier = $identifier;
        $this->_id = (new Table\Gallery())->getIdentifier($identifier)['id'];
    }
    public function getAll() {
        $r = (new Table\GalleryImages())->getGallery($this->_id);
        if ($r->isEmpty()) {
            throw new \Exception('No such gallery defined',500,null);
        }
        $ret = array();
        while ($d = $r->fetch()) {
            $ret [] = ['small' => $d['image_small'],'large' => $d['image_large'],'text' => $d['title']];
        }
        return $ret;
    }    
    public function getTitleImage() {
        $r = (new Table\GalleryImages())->simpleSelect('`gallery_id` = '.Util::escapeValue($this->_id).' ORDER BY `order` LIMIT 0,1');
        return $r->fetch();
    }

}

?>
