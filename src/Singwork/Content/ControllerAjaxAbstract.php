<?php
namespace Singwork\Content;
/**
 * Description of ControllerAjaxAbstract
 *
 * @author Matej Smisek
 */
abstract class ControllerAjaxAbstract extends Controller {
    
    protected $POST_PARAMS = [];
    
    public function preInit() {
        $this->disableLayout();
    }
    public function run() {
//        if (empty($this->_param[0]) && empty($this->_param['action'])) {
//            $this->root
//        } 
        if (empty($this->_param[0]) && empty($this->_param['action'])) {
            $this->root();
        }
        $method = $this->_param[0].'Action';        
        if (!method_exists($this, $method)) {
            $method = $this->_param['action'].'Action';
            if (!method_exists($this, $method)) {
                $this->error('No such method ->'.$method);
                return;
            }
        }
        $this->_view->output = $this->$method();
    }/**
     * WARNING New chagne, does not output in message but data
     * 
     * @param String $string Message to be sent 
     */
    protected function error($string) {
       $this->outputJson($string, 'error');
    }
    protected function deepgalleryAction() {   
        return (new Gallery\Deep_Gallery($_POST['id']))->getAjax();       
    }
    protected function deepSliderAction() {   
        return (new Gallery\Deep_Slider($_POST['id']))->getAjax();       
    }
    protected function swgalleryAction() {   
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
        } elseif (isset($this->_param['id'])) {
            $id = $this->_param['id'];
        } else {
            $this->error('No ID supplied. Error fetching gallery');
        }
        try {
            $data = (new Gallery($id))->getAll();       
        } catch  (\Exception $e) {
            $this->error($e->getMessage());
        }
        header('Content-type: application/json');
        echo json_encode(array('result' => 'success','data' => $data));
        die();
    }
    protected function verifyPost($action) {
        if (empty($this->POST_PARAMS[$action]) || !$this->POST_PARAMS[$action]) {
            return false;
        } else {
            foreach ($this->POST_PARAMS[$action] as $k => $values) {
                foreach ($values as $v) {
                    if (empty($_POST[$k]) || $_POST[$k] !== $v) {
                        
                    }
                }
            }
        }
    }

}

?>
