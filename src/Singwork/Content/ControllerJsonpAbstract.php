<?php
namespace Singwork\Content;
/**
 * Description of ControllerJsonpAbstract
 *
 * @author Matej Smisek
 */
class ControllerJsonpAbstract extends Controller {

    protected $_contentType = 'text/plain';

    public function preInit() {
        parent::preInit();
        $this->disableLayout();
    }

    public function process() {
//        header('Access-Control-Allow-Origin: *');
//        header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
//        header("Content-Type: text/plain");
//        
//        print_r($_SERVER);
//        die();


        $this->validateResponse();
    }

    protected function validateResponse() {
        //error_log('preparing requiest for: '.$_SERVER['HTTP_ORIGIN'].' with request type: '.$_SERVER['REQUEST_METHOD']);
        if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") {
            $allowed_origins = $this->getAllowedOrigins();
            error_log('OPTIONS arrived: ' . print_r($allowed_origins, true));
            if (in_array($_SERVER['HTTP_ORIGIN'], $allowed_origins)) {
                header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                header('Access-Control-Allow-Methods: POST, OPTIONS');
                header('Access-Control-Allow-Headers: X-CSRF-Token, X-Requested-With, X-Prototype-Version');
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 1728000');
                header("Content-Length: 0");
                header("Content-Type: text/plain");
            } else {
                header("HTTP/1.1 403 Access Forbidden");
                header("Content-Type: text/plain");
                echo "You cannot repeat this request";
            }
        } elseif ($_SERVER['REQUEST_METHOD'] == "POST") {
            $allowed_origins = $this->getAllowedOrigins();
            error_log('POST arrived: ' . print_r($allowed_origins, true));
            if (in_array($_SERVER['HTTP_ORIGIN'], $allowed_origins)) {
                $method = $this->formatAction($_POST['action']);
                if (!method_exists($this, $method)) {
                    $method = $this->formatAction($this->_param['action']);
                    if (!method_exists($this, $method)) {
                        $method = $this->formatAction($this->_param[0]);
                        if (!method_exists($this, $method)) {
                            $method = $this->formatAction($this->core()->getUrl()->getPage()->getUrl());
                            if (!method_exists($this, $method)) {
                                throw new \Exception('Invalid Action URL', "503", null);
                            }
                        }
                    }
                }
                header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                header('Access-Control-Allow-Methods: POST, OPTIONS');
                header('Access-Control-Allow-Headers: X-CSRF-Token, X-Requested-With, X-Prototype-Version');
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Max-Age: 1728000');
                header('Content-Type: ' . $this->_contentType);
                header("HTTP/1.1 200 OK");
                $response = $this->{$method}();
                echo $response;
                die();
            } else {

                die("POSTing not allowed");
            }
        } else {
            error_log('method error');
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
            header('Access-Control-Allow-Headers: X-CSRF-Token, X-Requested-With, X-Prototype-Version');
            header("Content-Type: text/html");
            $method = $this->formatAction($this->core()->getUrl()->getPage()->getUrl());
            if (!method_exists($this, $method)) {
                die('post error occured');
            } else {
               echo $this->{$method}(); 
            }
        }
    }

    protected function getAllowedOrigins() {
        
    }

    protected function formatAction($url) {
        return strtr($url, array('_' => '', '-' => '', ' ' => '')) . 'Action';
    }

}

?>
