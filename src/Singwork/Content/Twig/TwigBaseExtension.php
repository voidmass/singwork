<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Singwork\Content\Twig;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Description of TwigBaseExtension
 *
 * @author matej.smisek
 */
class TwigBaseExtension extends \Twig_Extension implements ContainerAwareInterface, \Twig_Extension_GlobalsInterface
{

    use ContainerAwareTrait;

    /**
     *
     * Registered global variables
     * @var array
     */
    protected $_globals = [];

    public function __construct()
    {
        
    }

    public function getFunctions()
    {
        return array_merge(parent::getFunctions(), [
            new \Twig_Function('menu', new TwigMenuFunction($this->container)),
            new \Twig_Function('url', new TwigUrlFunction($this->container)),
            new \Twig_Function('productUrl', new TwigProductUrlFunction($this->container)),
            new \Twig_Function('access', new TwigAccessFunction($this->container)),
        ]);
    }

    public function getFilters()
    {

        return array_merge(parent::getFilters(), [
            new \Twig_Filter('translate', new TwigTranslateFilter($this->container)),
            new \Twig_Filter('t', new TwigTranslateFilter($this->container)),
            new \Twig_Filter('route', function ($route) {
                        return $this->container->get('router')->generate($route);
                    }),
            new \Twig_Filter('urlize', function ($string) {
                        return \Singwork\Util\Util::prepareStringForUrl($string);
                    }),
            new \Twig_Filter('datetime', function ($string) {
                        return \Singwork\Util\Util::convertDate($string, 'CZ-TIME-SECONDS');
                    }),
            new \Twig_Filter('datetimebasic', function ($string) {
                        return \Singwork\Util\Util::convertDate($string, 'CZ-TIME');
                    }),
            new \Twig_Filter('date', function ($string) {
                        return \Singwork\Util\Util::convertDate($string, 'CZ');
                    }),
            new \Twig_Filter('dateiso', function ($string) {
                        return \Singwork\Util\Util::convertDate($string, 'ISO');
                    }),
            new \Twig_Filter('dateisofull', function ($string) {
                        return \Singwork\Util\Util::convertDate($string, 'ISOFULL');
                    }),
            new \Twig_Filter('time', function ($string) {
                        return \Singwork\Util\Util::convertDate($string, 'CZ-TIME-ONLY');
                    }),
            new \Twig_Filter('timeshort', function ($string) {
                        return \Singwork\Util\Util::convertDate($string, 'CZ-TIME-ONLY-SHORT');
                    }),
            new \Twig_Filter('datetimeshort', function ($string) {
                        return \Singwork\Util\Util::convertDate($string, 'CZ-TIME-SECONDS-SHORT');
                    }),
            new \Twig_Filter('json', function ($string) {
                        return json_decode($string, true);
                    }),
            new \Twig_Filter('dateago', function ($date) {
                        $now = new \DateTime();
                        $diff = $now->diff($date);
                        if (((integer) $diff->format("%a")) < 1) {
                            $minutes = (integer) (abs($now->getTimestamp() - $date->getTimestamp()) / 60);
                            if ($minutes < 1) {
                                return 'less than a minute ago';
                            } elseif ($minutes < 60) {
                                return $minutes . ' minute' . ($minutes > 1 ? 's' : '') . ' ago';
                            } else {
                                return round($minutes / 60) . ' hour' . (round($minutes / 60) > 1 ? 's' : '') . ' ago';
                            }
                        } elseif (((integer) $diff->format("%a")) < 6) {
                            return $diff->format("%a") . ' day' . (((integer) $diff->format("%a")) > 1 ? 's' : '') . ' ago';
                        } else {
                            return \Singwork\Util\Util::convertDate($date, 'CZ-TIME-SECONDS-SHORT');
                        }
                    }),
            new \Twig_Filter('dateformat', function ($string, $options = []) {
                        return \Singwork\Util\Util::convertDate($string, $options[0]);
                    }, array('is_variadic_N' => true))
        ]);
    }

    public function getGlobals(): array
    {
        return [
        ];
    }

    public function registerGlobal($variable, $value)
    {
        $this->_globals[$variable] = $value;
    }

}
