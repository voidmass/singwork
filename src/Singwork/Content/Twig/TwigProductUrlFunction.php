<?php
namespace Singwork\Content\Twig;


use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Singwork\Util\Util;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TwigMenuFunction
 *
 * @author Matej Smisek
 */
class TwigProductUrlFunction implements ContainerAwareInterface
{

    use ContainerAwareTrait;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke($id)
    {       
        $product = $this->container->get('manager_container')->getManager(null)->getRepository('Base:Shop\Product')->find($id);
        return $this->container->get('router')->generate($_ENV['config']['shop']['product_route'], ['product_url' => $product->getUrlName()]);
    }
}