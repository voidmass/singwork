<?php
namespace Singwork\Content\Twig;


use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TwigMenuFunction
 *
 * @author Matej Smisek
 */
class TwigMenuFunction implements ContainerAwareInterface
{

    use ContainerAwareTrait;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke($id)
    {
        return $this->container->get('menu_container')->getMenu($id);
    }
}