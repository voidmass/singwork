<?php
namespace Singwork\Content\Twig;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;


/**
 * Description of TwigTranslateFilter
 *
 * @author matej.smisek
 */
class TwigTranslateFilter implements ContainerAwareInterface
{

    use ContainerAwareTrait;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke($word)
    {
        return $this->container->get('translate')->getWord($word);
    }
}
