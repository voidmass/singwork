<?php
namespace Singwork\Content;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HtmlTag
 *
 * @author Matej Smisek
 */
class Tag {
    
    protected $_tag;
    
    protected $_id;
    
    protected $_classes;
    
    protected $_data = array();
    
    protected $_properties = array();
    
    protected $_children = array();
    
    protected $_html;
    
    protected $_pair = true;
    
    protected static $NON_PAIRS = ['input','br','img','link','meta'];
            
    public function __construct($tag) {
        $this->_tag = $tag;
        if (in_array($tag, self::$NON_PAIRS)) {
            $this->_pair = false;
        }
        
    }
    /**
     * 
     * @param string $classes
     * @return \Tag
     */
    public function addClass($classes) {
        $this->_classes .= ' '.trim($classes);
        $this->_classes = trim($this->_classes);
        return $this;
    }  
    /**
     * 
     * @param string $id
     * @return \Tag
     */
    public function setId($id) {
        $this->_id = $id;
        return $this;
    }
    /**
     * 
     * @param string $data
     * @param string $value
     * @return \Tag
     */
    public function data($data,$value) {
        $this->_data[$data] = $value;
        return $this;
    }
    public function property($property, $value) {
        $this->_properties[$property] = $value;
        return $this;
    }
    /**
     * 
     * @param Tag $tag
     * @return \Tag
     */
    public function append($tag) {
        $this->_children[] = $tag;
        return $this;
    }
    
    public function html($html) {
        $this->_html = $html;
        return $this;
    }
    protected function getData() {
        $output = "";
        foreach ($this->_data as $k => $v) {
            $output .= 'data-'.$k.'="'.$v.'" ';
        }
        return trim($output);
    }
    protected function getProperties() {
        $output = "";
        foreach ($this->_properties as $k => $v) {
            $output .= $k.'="'.$v.'" ';
        }
        return trim($output);
    }
    public function __toString() {
        return $this->toString();
    }
    public function toString() {
        $output = '<'.$this->_tag.' '.$this->getTagValues();
        if (!$this->_pair) {
            $output .= ' />';
            return $output;
        } else {
            $output .= '>';
        }
        $output .= $this->_html;
        foreach ($this->_children as $v) {
            $output .= $v->toString();
        }      
        $output .= '</'.$this->_tag.'>';
        return $output;
    }
    protected function getTagValues() {
        $out = "";
        if (!empty($this->_id)) {
            $out .= ' id="'.$this->_id.'" ';
        }
        if (!empty($this->_classes)) {
            $out .= ' class="'.$this->_classes.'" ';
        }
        $out .= $this->getData().' ';
        $out .= $this->getProperties().' ';
        return $out;
    }
}

?>
