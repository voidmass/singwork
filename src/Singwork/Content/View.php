<?php

namespace Singwork\Content;

use Singwork\Exception\SetupException;
use Singwork\Table;
use Symfony\Component\DependencyInjection\ContainerInterface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Layout
 *
 * @author Matej Smisek
 */
class View
{

    /**
     *
     * @var TemplateEngine
     */
    protected $_engine;

    /**
    *
    * @var ContainerBuilder
    */
    protected $_container;
    
    protected $_template;
    
    protected $_variables;
    
    public function __construct($template, $variables,ContainerInterface $container)
    {
        $this->_template = $template;
        $this->_variables = $variables;
        $this->_container = $container;
        $this->_engine = $this->_container->get('template_engine');
    }
    public function render() {
        return $this->_engine->render($this->_template, $this->_variables);
    }

}

?>
