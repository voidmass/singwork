<?php

namespace Singwork\Content;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Description of TemplateEngine
 *
 * @author Balgor
 */
class TemplateEngine implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @var \Twig_Environment
     */
    protected $_twig;

    public function __construct()
    {
        $loader = new \Twig_Loader_Filesystem([PUBLIC_PATH . '/_views', SINGWORK_PATH . '/_default_views']);
        $this->_twig = new \Twig_Environment($loader, ['cache' => PUBLIC_PATH . '/cache', 'auto_reload' => true, 'debug' => true]);
        $this->_twig->addExtension(new \Twig_Extension_Debug());
    }

    public function render(string $template, array $variables)
    {
        return $this->_twig->render($template, $variables);
    }

    public function load(string $template)
    {
        return $this->_twig->load($template);
    }

    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function registerExtension(\Twig_Extension $extension)
    {
        $this->_twig->addExtension($extension);
    }
    
    public function getTwig(): \Twig_Environment
    {
        return $this->_twig;
    }

    public function setTwig(\Twig_Environment $twig)
    {
        $this->_twig = $twig;
        return $this;
    }


}   
