<?php

namespace Singwork\Content;

use Singwork\Form\FormData;
use Singwork\Form\Form;
use Singwork\Util\Translate;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManager;
use Singwork\Event\FormCheckEvent;

/**
 * 
 *
 * @author Matej Smisek
 */
abstract class Controller implements EventSubscriberInterface, ContainerAwareInterface
{

    public static $RESULT_MAP = [
        'success' => Response::HTTP_OK,
        'error' => 480,
        'fatal' => 481,
    ];

    /**
     *
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Form
     */
    protected $_form;

    /**
     * 
     */
    protected $_method = false;

    /**
     *
     * @var String method name to run in parallel with main run
     */
    protected $_runMethod = false;

    public function __construct()
    {
        return;
        $this->_t = new Translate();
        $this->_view = new View();
        $this->_view->_t = $this->_t;
        $this->_db = core()->getConnection();
    }

    public static function getSubscribedEvents()
    {
        return [];
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * 
     * @return EntityManager
     */
    protected function entityManager($identifier = null): EntityManager
    {
        return $this->container->get('manager_container')->getManager($identifier);
    }

    /**
     * Shortcut for entityManager
     * 
     * @return EntityManager
     */
    protected function em($identifier = null)
    {
        return $this->entityManager($identifier);
    }

    /**
     * Construct url based on ID
     * 
     * @param string $id
     * @deprecated since version 4
     * @return string
     */
    protected function constructUrl($id, $slugs = [])
    {
        return $this->generateUrl($id, $slugs);
    }

    /**
     * Construct url based on ID
     * 
     * @param string $id
     * @return string
     */
    protected function generateUrl($id, $slugs = [])
    {
        return $this->container->get('router')->generate($id, $slugs);
    }

    protected function translate($word)
    {
        return $this->container->get('translate')->getWord($word);
    }

    /**
     * @return \Application\Application
     */
    protected function getApplication()
    {
        return $this->container->get('application');
    }

    public function __invoke(Request $request)
    {
        echo 'invoking';
        return new Response();
        $this->runCheck();
        if ($this->ajaxCheck() === false) {
            if ($this->formCheck() === false) {
                $this->run();
                if ($this->_runMethod !== false) {
                    $this->{$this->_runMethod}();
                }
                return;
            }
        }
        if ($this->_method !== false) {
            if ($_POST['method'] === 'ajax') {
                try {
                    $this->{$this->_method}();
                } catch (\Exception $e) {
                    $this->outputJson($e->getMessage(), 'error');
                }
            } else {
                $this->{$this->_method}();
            }
        }
    }

    /**
     * 
     * @return \Singwork\User\UserHandler
     */
    protected function getUserHandler()
    {
        return $this->container->get('user_handler');
    }

    /**
     * 
     * @return \Singwork\Model\Entities\User
     */
    protected function getUser()
    {
        return $this->getUserHandler()->getUser();
    }

    /**
     * 
     * @param type $data
     * @param type $result
     * @return JsonResponse
     */
    public function json($data, $result = 'success')
    {
        if (!is_array($data)) {
            $data = [$data];
        }
        return $this->outputJson($data, $result);
    }

    /**
     * 
     * @param type $output
     * @param type $result
     * @return JsonResponse
     */
    public function outputJson($output, $result = 'success')
    {
        if (is_numeric($result)) {
            $code = $result;
        } else {
            $code = self::$RESULT_MAP[strtolower($result)];
        }
        return new JsonResponse($output, $code);
    }

    public function jsonEvent($event, $data) {
        return $this->outputJson(['events' => [$event => $data]]);
    }
    
    public function outputRawJson($output)
    {
        return new JsonResponse($output, self::$RESULT_MAP[strtolower($result)]);
    }

    public function outputAsFile($filename, $data)
    {
        header("Content-Type: application/octet-stream");
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header("Pragma: public");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//        ob_clean();
//        flush();
        print($data);
        ob_flush();
        flush();
    }

    public function prepareForm(FormData $formData, $form, $scope = null)
    {
        $this->_form = new Form($formData, $form, $scope);
        if ($formData->getType() === 'check') {
            return $this->formFieldVerification();
        } else {
            return $this->formValidateAll();
        }
    }

    protected function formFieldVerification()
    {
        $event = new FormCheckEvent($this->_form, $this->_form->getFormData(), $this->_form->verifyCheck(), $this->container->get('request'));

        $this->container->get('event_dispatcher')->dispatch(FormCheckEvent::NAME . $this->_form->getName(), $event);
       
        return $this->outputJson(['fields' => $event->getErrorStack()]);
    }

    protected function formValidateAll()
    {
        $this->_form->verifyAll();
        if (!empty($this->_form->getErrorStack())) {
            return $this->formInvalid($this->_form->getErrorStack());
        }
        return true;
    }

    protected function formValid($message = '', $actions = [])
    {
        return $this->outputJson(['status' => 'valid', 'message' => $message, 'actions' => $actions]);
    }

    protected function formInvalid($fields, $message = '')
    {
        return $this->outputJson(['status' => 'invalid', 'fields' => $fields, 'message' => $message]);
    }

    protected function formatRun($url)
    {
        return strtr($url, array('_' => '', '-' => '', ' ' => '', '/' => 'root')) . 'Run';
    }

    /**
     * 
     * @return \Singwork\User\UserHandler
     */
    protected function userHandler()
    {
        return $this->getUserHandler();
    }
 
    
    protected function getSetting($code) { 
        return $this->em()->find('Base:Setting', $code);
    }
}

?>
