<?php
namespace Singwork\Content;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ControllerFormAbstract
 *
 * @author Matej Smisek
 */
abstract class ControllerFormAbstract extends Controller {
    
    protected $_errorStack = array();   
    
    public function preInit() {
        $this->disableLayout();
    }
    public function run() {
        $method = $this->_param[0].'Form';        
        if (!method_exists($this, $method)) {
            $method = $this->_param['form'].'Form';
            if (!method_exists($this, $method)) {
                $this->error('No such method ->'.$method);
                return;
            }
        }
        $this->_view->output = $this->$method();        
    }
        
}

?>
