<?php

namespace Singwork;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Singwork\Console\CliInterpreter;

/**
 * 
 */
class Core
{

    /**
     *
     * @var Config
     */
    protected $_config;

    /**
     *
     * @var ContainerBuilder
     */
    protected $_container;

    /**
      /**
     *
     * @var Boolean
     */
    protected $_layoutDisabled = false;
    protected $_renderDisabled = false;
    protected $_layout;

    /**
     *
     * @var Controller
     */
    protected $_controller;

    /**
     *
     * @var UrlProcessor
     */
    protected $_url;
    protected $_db;
    protected $_params;
    protected $_currentLanguage;
    protected $_menu;

    /**
     *
     * @var Header
     */
    protected $_header;
    protected $_websitePath;
    protected $_frameworkPath;

    /**
     *
     * @var Translate
     */
    public $_t;
    protected $_settings;

    /**
     *
     * @var Overrider
     */
    protected $_overrider;

    /**
     *
     * @var UserHandler
     */
    protected $_userHandler;

    /**
     *
     * @var String
     */
    protected $_layoutPath;

    /**
     *
     * @var Controller
     */
    protected $_layoutController;

    /**
     *
     * @var Session
     */
    protected $_session;

    /**
     *
     * @var Logger
     */
    protected $_logger;
    protected $_application;
    public $_error_execution = false;

    /**
     *
     * @var \Exception;
     */
    public $exception;

    /**
     *
     * @var Core
     * 
     */
    protected static $instance = null;
    public $_globals;

    /**
     *
     * @var ControllerLoader
     */
    protected $_controllerLoader;

    /**
     * 
     * @return Core
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Core();
        }
        return self::$instance;
    }

    private function __construct()
    {
        
    }

    public function cliRun($command, $args)
    {
        $_ENV['mode'] = 'CLI';
        $this->createConfig();
        $this->_logger = new Util\Logger();

        $this->_container = new ContainerBuilder();

        $this->registerServices();

        $this->_container->compile();

        $this->prepareApplication();
        $this->prepareRouter();
        $this->_container->get('application')->start();

        $cli = new CliInterpreter($this->_container);
        $cli->exec($command, $args);
    }

    public function run()
    {
        try {
            $_ENV['mode'] = 'HTTP';
            $this->createConfig();
            $this->_logger = new Util\Logger();
            slog_verbose('Running application...');

            slog_verbose('Loading ContainerBuilder');
            $this->_container = new ContainerBuilder();

            $this->registerServices();

            $this->_container->compile();

            $this->prepareApplication();

            $this->prepareRouter();

            $this->_container->get('request_proccesor')->process();
        } catch (\Exception $e) {
            echo 'ERROR: <b>' . $e->getMessage() . '</b>';
            d($e->getTrace());
        } catch (\Error $e) {
            http_response_code(500);
            if ($_POST['method'] == "ajax") {
                
                echo json_encode([$e->getMessage(), $e->getTraceAsString()]);
            } else {
                echo 'ERROR: <b>' . $e->getMessage() . '</b>';
                d($e->getTrace());
            }
        }
        
        slog_verbose('Closing Application');
    }

    protected function registerServices()
    {
        $this->_container->register('template_engine', 'Singwork\Content\TemplateEngine')
                ->addMethodCall('setContainer', array(new Reference('service_container')))
                ->addMethodCall('registerExtension', array(new Reference('twig_base_extension')))
                ->setAutowired(true);
        $this->_container->register('twig_base_extension', 'Singwork\Content\Twig\TwigBaseExtension')
                ->addMethodCall('setContainer', array(new Reference('service_container')))
                ->setAutowired(true);
        $this->_container->register('event_dispatcher', 'Symfony\Component\EventDispatcher\EventDispatcher');
        $this->_container->register('cookies', 'Singwork\Util\Cookies');
//        $this->_container->register('session', 'Singwork\Util\Session')->addArgument(config('website.name'));
//        $this->setLanguage($_SESSION['current_language']);
//        $this->_settings = new Startup\Settings(); -> Load them in some memcache
        $this->_container->register('translate', 'Singwork\Content\Translate\Translate')->setAutowired(true);
        $this->_container->register('user_handler', 'Singwork\User\UserHandler')
                ->addMethodCall('setContainer', array(new Reference('service_container')))
                ->setAutowired(true);
        $this->_container->register('manager_container', 'Singwork\Database\ManagerContainer');
        $this->_container->register('menu_container', 'Singwork\Content\Menu\MenuContainer')
                ->addMethodCall('setContainer', array(new Reference('service_container')))
                ->setAutowired(true);
        $this->_container->register('sequencer', 'Singwork\Database\Sequencer')
                ->addMethodCall('setContainer', array(new Reference('service_container')))
                ->setAutowired(true);
        $this->_container->register('request_proccesor', 'Singwork\RequestProcessor')
                ->addMethodCall('setContainer', array(new Reference('service_container')))
                ->setAutowired(true);
        $this->_container->register('mailer', 'Singwork\Util\Mailer')
                ->addMethodCall('setContainer', array(new Reference('service_container')))
                ->setAutowired(true);
        $this->_container->register('email_permission_manager', 'Singwork\User\EmailPermissionManager')
                ->addMethodCall('setContainer', array(new Reference('service_container')))
                ->setAutowired(true);
        $this->_container->register('order_manager', 'Singwork\Shop\OrderManager')
                ->addMethodCall('setContainer', array(new Reference('service_container')))
                ->setAutowired(true);
        $this->_container->register('promotion_manager', 'Singwork\Shop\PromotionManager')
                ->addMethodCall('setContainer', array(new Reference('service_container')))
                ->setAutowired(true);
        $this->_container->register('encrypter', 'Singwork\Util\Encryption')
                ->setAutowired(true);

        $this->_container->register('factory_store', 'Singwork\Factory\FactoryStore')
                ->addMethodCall('setContainer', array(new Reference('service_container')))
                ->setAutowired(true);
    }

    protected function prepareApplication()
    {
        if (!class_exists('\Application\Application')) {
            return;
        }
        $application = new \Application\Application();
        if ($application instanceof ContainerAwareInterface) {
            $application->setContainer($this->_container);
        }
        $this->_container->set('application', $application);
    }

    protected function prepareRouter()
    {
        $locator = new FileLocator(array(PUBLIC_PATH . '/_config'));
        $router = new Router(new YamlFileLoader($locator), 'routes.yml', [], new RequestContext());
        $this->_container->set('router', $router);
    }

    /**
     * 
     * @return Logger
     */
    public function getLogger()
    {
        return $this->_logger;
    }

    private function createConfig()
    {
        $_ENV['config'] = Util\Util::openJsonFile($this->_websitePath . "/_config/configuration.json");
    }

    public function getConfig()
    {
        return $_ENV['config'];
    }

    public function setPublicPath($path)
    {
        $this->_websitePath = $path;
    }

    public function setFrameworkPath($path)
    {
        $this->_frameworkPath = $path;
    }

}

?>
