<?php
namespace Singwork\Api;

use Singwork\Util\Util;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BankApi
 *
 * @author Matej Smisek
 */
class BankApi {
    /**
     *
     * @var String
     */
    protected $_key = '';
    /**
     * Default constructor that sets key from config file
     */
    public function __construct() {
        $this->_key = core()->getConfig()['shop']['bank_key'];
    }
    
    public function getTransactionsSinceLastCheck() {
        $url = 'https://www.fio.cz/ib_api/rest/last/'.$this->_key.'/transactions.json';
        return $this->translateReport(json_decode((new Poster())->sendPost($url), true));
    }
    
    protected function translateReport($report) {
        $return = [];
        foreach ($report['transactionList']['transaction'] as $transaction) {
            $return[] = [
                'transaction_id' => $transaction['column22']['value'],
                'amount' => $transaction['column1']['value'],
                'VS' => $transaction['column5']['value'],
                'KS' => $transaction['column4']['value'],
                'SS' => $transaction['column6']['value'],
                'datetime' => Util::convertDate(((int)$transaction['column0']['value'])/1000)                                
            ];
        }
        return $return;
    }   
}
