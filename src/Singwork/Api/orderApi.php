<?php
namespace Singwork\Api;

use Singwork\Shop\Order;
use Singwork\Table\Shop\SingshopOrder;
use Singwork\Table\Shop\SingshopOrderTransaction;
/**
 * Default implementation of API for order manipulation in ecomerce module
 *
 * @author Matej Smisek
 */
class OrderApi extends Api {
    /**
     * Moves order one stage forward
     * 
     * order_id to be provided in POST
     */    
    protected function progressCommand() {        
        $order = new Order(false);
        $order->createCart($_POST['order_id']);
        $newStatus = $order->progressOrder();
        $this->output(['status' => $newStatus]);
    }
    /**
     * Moves order one stage behind
     * TODO
     */
    protected function regressCommand() {
        
    }
    /**
     * Cancels order
     * 
     * order_id and comment to be provided in POST
     */
    protected function cancelCommand() {
        $order = new Order(false);
        $order->createCart($_POST['order_id']);
        $newStatus = $order->cancelOrder($_POST['comment']);
        $this->output(['status' => $newStatus]);
    }
    /**
     * Downloads daily bank statements and saves them in database.
     * Eventually progresses orders that have been fully paid
     */
    protected function bankDownloadCommand() {
        $transactions = (new BankApi())->getTransactionsSinceLastCheck();
        $t_orders = new SingshopOrder();
        $t_transactions = new SingshopOrderTransaction();
        foreach ($transactions as $transaction) {
            $order = $t_orders->getId($transaction['VS']);
            if ($order !== false) {
                $t_transactions->insertTransaction($transaction['order_id'], $transaction['amount'], $transaction['VS'], $transaction['SS'], $transaction['KS'], $transaction['datetime'], $transaction['transaction_id']);                
                $order_o = new Order(false);
                $order_o->createCart($order['id']);
                if ($t_transactions->getTotalForOrder($order['id']) == $order['price']) {
                    $order_o->processOrder();
                }
            }
        }
    }
}
