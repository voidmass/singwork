<?php
namespace Singwork\Api;

use Singwork\Exception\RuntimeSingException;
/**
 * Abstract Api which every apiClass should inherit from
 *
 * @author Matej Smisek
 */
abstract class Api {
    /**
     * Array of params from variable URL part passed from Core
     * @var Array key -> value pair of individual parameters
     */
    protected $_params;
    /**
     * Executes command, variable can be passed via POST or params
     * 
     * @param String $command name of the command to execute ( Command keyword will be appended to this)
     * @throws RuntimeSingException
     */
    public function execute($command) {
        $method_name = $command.'Command';
        if (!method_exists($this, $method_name)) {
            throw new RuntimeSingException('No such API commnand');
        }
        $this->_params = & core()->getParams();
        $this->$method_name();
    }
    /**
     * Prints JSON data to output
     * 
     * @param Array $data
     */
    protected function output($data) {
        core()->getController()->outputJson($data);
    }
    /**
     * Prints JSON data in error format to output
     * 
     * @param Array $data
     */
    protected function error($data) {
        core()->getController()->outputJson($data, 'error');
    }
    
}
