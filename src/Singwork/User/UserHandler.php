<?php

namespace Singwork\User;

use Singwork\Util\Util;
use Singwork\Util\Mailer;
use Singwork\Event\UserLoginEvent;
use Singwork\Event\UserVerifyAccessEvent;
use Singwork\Event\UserConstructEvent;
use Singwork\Model\Entities\User;
use Singwork\Model\Entities\UserSession;
use Singwork\Model\Entities\UserPasswordReset;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\Criteria;
use Singwork\Event\UserSignupEvent;
use Laravolt\Avatar\Avatar;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Security
 *
 * @author Matej Smisek
 */
class UserHandler implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     *
     * @var User
     */
    protected $_user = false;

    /**
     *
     * @var UserSession
     */
    protected $_userSession = false;

    /**
     *
     * @var string
     */
    protected $_namespace = '*';

    /**
     * 
     * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
     */
    protected $_session;
    
    protected $_sessionBag;

    /**
     *
     * @var \Symfony\Component\EventDispatcher\EventDispatcher;
     */
    protected $_dispatcher;

    public function __construct(\Symfony\Component\EventDispatcher\EventDispatcher $dispatcher)
    {
        $this->_dispatcher = $dispatcher;
    }

    public function initialize(Request $request)
    {
        $this->_sessionBag = $request->getSession()->getBag('application');
        $this->_session = $request->getSession();
        if ($request->query->get('force_nologin', 'false') == 'true' || Util::crawlerDetect($request->server->get('HTTP_USER_AGENT')) === true) {
            $this->_user = null;
        } else {
            $this->constructUser($request);
        }
    }

    protected function constructUser(Request $request)
    {
//        slog_info('Starting user construct >'.$this->_session->getId().'<'.print_r($this->_sessionBag->all(), true));
        if ($this->verifySession()) {
//             slog_info('sesssion verified');
            $this->updateUserSession($request);
            goto constructEvent;
        }
        if ($this->relogin($request)) {
            goto constructEvent;
        }
        if ($_ENV['config']['security']['use_temp_login'] === true) {
            $this->temporaryLogin();
            goto constructEvent;
        } else {
            $this->_user = null;
            goto constructEvent;
        }
        constructEvent:
        $event = new UserConstructEvent($this->_user);
        $this->_user = $this->_dispatcher->dispatch(UserConstructEvent::NAME, $event)->getUser();
    }

    public function login($username, $password)
    {
        $user = $this->processLoginCredentials($username, $password);

        return $this->doLogin($user, $username);
    }

    protected function processLoginCredentials($username, $password)
    {
        if (empty($username) || empty($password)) {
            return null;
        }
        $user = $this->retrieveUserByUsername($username);
        if (empty($user)) {
            return null;
        }
        $hash = self::hashPassword($password, (string) $user->getRegisterDate()->getTimestamp());
        if ($user->getPassword() !== $hash) {
            return null;
        }

        return $user;
    }

    protected function doLogin($user, $username)
    {

        $event = new UserLoginEvent($user, $username, true, $this->_user);
        $user = $this->_dispatcher->dispatch(UserLoginEvent::NAME, $event)->getUser();

        if (empty($user)) {
//            slog_info('User login Fail');
            return false;
        } else {
//            slog_info('User login success ('.$user->getFullName().')');
            $userSession = $session = $this->em()->getRepository('Base:UserSession')->findOneBy(['userId' => $user->getId(), 'sessionId' => $this->container->get('request')->getSession()->getId()]);
            $this->saveUserSession($user, $userSession);
            return true;
        }
    }

    protected function temporaryLogin()
    {
        $time = new \DateTime();
        $user = User::createTemporary($time, $time);
        $event = new UserLoginEvent($user, null, false, $this->getUser());
        $user = $this->_dispatcher->dispatch(UserLoginEvent::NAME, $event)->getUser();

        $this->em()->persist($user);
        $this->saveUserSession($user);
    }

    protected function relogin(Request $request)
    {
//        slog_info('Trying relogin');
        $key = $request->cookies->get('auth_key');
        if ($key === null || $key === false) {
//            slog_info('Relogin failed, no key');
            return false;
        }
        /* @var $session UserSession */
        $session = $this->em()->getRepository('Base:UserSession')->findOneBy(['authKey' => $key]);
        if (empty($session)) {
            $this->container->get('cookies')->set('auth_key', false);
//            slog_info('Relogin failed, no session');
            return false;
        }
        $user = $this->em()->getRepository('Base:User')->find($session->getUserId());
        $this->container->get('cookies')->set('auth_key', false);
        $this->saveUserSession($user, $session);
    }

    /**
     * 
     * @param User $user
     */
    protected function saveUserSession(User $user, UserSession $session = null)
    {
        if (empty($user)) {
            $this->_user = null;
            return;
        }
        $this->_sessionBag->set('security/user', $user->getId());
        $this->_sessionBag->set('security/creation', new \DateTime());
        $this->_sessionBag->set('security/user_type', $user->getType());
        $this->_sessionBag->set('security/remote', $this->container->get('request')->server->get('REMOTE_ADDR'));

        $authKey = hash('SHA256', Uuid::uuid4()->toString());
        $this->container->get('cookies')->set('auth_key', $authKey, Util::decodeTime($_ENV['config']['session']['lifetime']) * 4);
//        slog_info('Relogin success, setting new key ('.$user->getFullName().')');
        if ($session === null) {
            $this->_userSession = new UserSession($user->getId(), $this->container->get('request')->getSession()->getId(), $this->_sessionBag->get('security/creation'), $this->_sessionBag->get('security/creation'), 1, $authKey, $this->container->get('request')->server->get('HTTP_USER_AGENT'), $this->_sessionBag->get('security/remote'));
            $this->em()->persist($this->_userSession);
        } else {
            $session
                    ->setAuthKey($authKey)
                    ->setAgent($this->container->get('request')->server->get('HTTP_USER_AGENT'))
                    ->setOnline(1)
                    ->setLastUpdate($this->_sessionBag->get('security/creation'))
                    ->setIp($this->_sessionBag->get('security/remote'));
            $this->_userSession = $session;
        }


        $this->_user = $user;
        $this->_user->updateTime();

        $this->em()->flush();
    }

    protected function updateUserSession(Request $request)
    {
        $this->_user = $this->retrieveUserById($this->_sessionBag->get('security/user'));
        if (empty($this->_user)) {
            $this->logout();
            return;
        }
        $this->_userSession = $this->em()->getRepository('Base:UserSession')->findOneBy(['userId' => $this->_user->getId(), 'sessionId' => $this->container->get('request')->getSession()->getId()]);
        if ($this->_userSession === null) {
            return $this->relogin($request);
        }
        $this->_user->updateTime();
        $this->_userSession->setLastUpdate(new \DateTime())->setOnline(1);
        $this->em()->flush();
    }

    /**
     * 
     * @return boolean
     */
    protected function verifySession()
    {
        if (!$this->_sessionBag->has('security')) {
//            slog_debug('No security');
            return false;
        }
        if (!$this->_sessionBag->has('security/user') || $this->_sessionBag->get('security/user') === null) {
//            slog_debug('No user');
            return false;
        }
        return true;
    }

    /**
     * 
     * @param string $id
     * @return User
     */
    protected function retrieveUserById($id)
    {
        return $this->em()->find('Base:User', $id);
    }

    /**
     * 
     * @param string $username
     * @return User
     */
    protected function retrieveUserByUsername($username)
    {
        return $this->em()->getRepository('Base:User')->findOneBy(['email' => $username]);
    }

    public function isLoggedIn($count_temporary = false)
    {
        if (empty($this->_user)) {
            return false;
        }
        if ($this->_user->getType() === User::REGULAR) {
            return true;
        } elseif ($this->_user->getType() === User::TEMPORARY && $count_temporary === true) {
            return true;
        }
        return false;
    }

    public function logout()
    {
        if (empty($this->_user)) {
            return true;
        }
        $this->em()->remove($this->_userSession);
        $this->container->get('cookies')->set('auth_key', false);
        $this->_sessionBag->remove('security');
        $this->_user = false;
//        $this->em()->remove($this->_userSession);
//        $this->em()->flush();
        $this->_userSession = false;
        if ($_ENV['security']['use_temp_login'] === true) {
            $this->temporaryLogin();
        }
    }

    public function signup($username, $password, $login = true)
    {
        if ($username == null) {
            $result = 'empty-fields';
            return false;
        }
        if (!\Singwork\Form\Form::emailValue($username)) {
            $result = 'invalid-email';
            return false;
        }
        if ($this->retrieveUserByUsername($username) !== null) {
            $result = 'already-taken';
            return false;
        }
        $regdate = new \DateTime();
        $hash = self::hashPassword($password, (string) $regdate->getTimestamp());
        $user = User::createRegular($username, $hash, $regdate, $regdate);
        $user_id = $user->getId();
        $this->em()->persist($user);
        if ($_ENV['config']['user']['create_avatar'] == true) {
            $this->createAvatarImage($user, $user->getName() . ' ' . $user->getSurname());
        }
        $this->em()->flush();
        $event = new UserSignupEvent($user);
        $user = $this->_dispatcher->dispatch(UserSignupEvent::NAME, $event)->getUser();
        if ($login === true) {
            $this->doLogin($user, $username);
        }
        return true;
    }

    /**
     * 
     * @param type $email
     * @param type $fullName
     * @return User
     */
    public function invite($email, $fullName)
    {
        if ($email == null) {
            $result = 'empty-fields';
            return $result;
        }
        if (!\Singwork\Form\Form::emailValue($email)) {
            $result = 'invalid-email';
            return $result;
        }
        if ($this->retrieveUserByUsername($email) !== null) {
            $result = 'already-taken';
            return $result;
        }
        $user = User::createInvite($email, hash('SHA256', $email . microtime()));
        $user->setName(explode(' ', $fullName)[0]);
        $user->setSurname(explode(' ', $fullName)[1]);
        $this->em()->persist($user);
        if ($_ENV['config']['user']['create_avatar'] == true) {
            $this->createAvatarImage($user, $fullName);
        }
        $this->em()->flush();
        return $user;
    }

    protected function createAvatarImage(User $user, $string)
    {
        $avatar = new Avatar($this->container->get('application')->getAvatarConfig());
        $user->setAvatar($avatar->create($string)->toBase64());
    }

    public function createVerificationToken()
    {
        $token = hash('sha1', (new \DateTime('now'))->getTimestamp() . $this->_user->getEmail());

        $emailVerification = new \Singwork\Model\Entities\UserEmailVerification($this->_user, $token, $this->getUser()->getEmail());
        $this->em()->persist($emailVerification);
        $this->_user->addEmailVerification($emailVerification)->setModifiedEmail(new \DateTime('now'));
        $emailVerification->setLastSent(new \DateTime('now'));
        $this->em()->flush();

        return $token;
    }

    public function createNewsletterVerification($email)
    {
        $token = hash('sha1', (new \DateTime('now'))->getTimestamp() . $email);

        $emailVerification = new \Singwork\Model\Entities\NewsletterEmailVerification($email, $token);
        $this->em()->persist($emailVerification);
        $this->em()->flush();

        return $token;
    }

    /**
     * 
     * @return \Singwork\Model\Entities\UserEmailVerification
     */
    public function getCurrentVerification()
    {
        return $this->_user->getEmailVerification()->matching(Criteria::create()->where(Criteria::expr()->eq("verifiedDate", null))->andWhere(Criteria::expr()->eq("email", $this->_user->getEmail())))->current();
    }

    /**
     * 
     * @param type $subject
     * @param type $html_template
     * @param type $plain_template
     * @param type $vars
     * @param User $user
     * @return \Swift_Message
     */
    public function sendEmailToUser($subject, $html_template, $plain_template, $vars, User $user = null)
    {
        if ($user === null) {
            $user = $this->_user;
        }
        $message = new \Swift_Message();
        $message->setTo([$user->getEmail() => $user->getName() . " " . $user->getSurname()]);
        $message->setSubject($subject);

        $message->setBody($this->container->get('template_engine')->render($html_template, $vars), 'text/html');
        $message->addPart($this->container->get('template_engine')->render($plain_template, $vars), 'text/plain');

        $this->container->get('mailer')->send($message);
        return $message;
    }

    public function verifyEmail($token)
    {
        $userEmailVerification = $this->em()->getRepository('Base:UserEmailVerification')->findOneBy(['token' => $token]);
        if ($userEmailVerification === null) {
            return false;
        }
        if ($userEmailVerification->getVerifiedDate() !== null) {
            return false;
        }
        if ($userEmailVerification->getUser()->getVerifiedEmail() === true) {
            return 'already-verified';
        } else {
            $userEmailVerification->getUser()->setVerifiedEmail(true);
            $userEmailVerification->setVerifiedDate(new \DateTime('now'));
        }


        $this->em()->flush();
        return true;
    }

    /**
     * 
     * @param \Symfony\Component\HttpFoundation\ParameterBag $bag
     * @return boolean false if access is NOT granted, true otherwise
     */
    public function verifyAccess(\Symfony\Component\HttpFoundation\ParameterBag $bag)
    {

        $result = false;
        if ($bag->get('flag') === null) {
            $result = true;
            goto verifyEvent;
        }
        if (!$this->isLoggedIn()) {
            $result = false;
            goto verifyEvent;
        }
        if ($this->checkFlag($bag->get('flag'), 2)) {
            $result = true;
            goto verifyEvent;
        }

        verifyEvent:
        $event = new UserVerifyAccessEvent($this->_user, $bag, $result);
        $result = $this->_dispatcher->dispatch(UserVerifyAccessEvent::NAME, $event)->getAccess();

        return $result;
    }

    public function compareFlags($flag1, $flag2)
    {
        $flag1_parts = explode('.', $flag1);
        $flag2_parts = explode('.', $flag2);
        foreach ($flag1_parts as $k => $v) {
            if ($this->matchFlagPart($v, $flag2_parts[$k]) === false) {
             
                return false;
            }
        }
     
        return true;
    }

    protected function matchFlagPart($part1, $part2)
    {
        return ($part1 === $part2 || $part1 === '*' || $part2 === '*');
    }

    public function checkAgaintsUserFlags($flag)
    {
        $result = false;
        foreach ($this->getUserFlags(1) as $v) {
            if ($this->compareFlags($flag, $v->getFlag()) === true) {
                $result = true;
                break;
            }
        }
        foreach ($this->getUserFlags(-1) as $v) {
            if ($this->compareFlags($flag, $v->getFlag()) === true) {
                $result = false;
                break;
            }
        }
        return $result;
    }

    protected function getUserFlags($polarity)
    {
        return $this->_user->getFlags()->matching(Criteria::create()->where(Criteria::expr()->eq("polarity", $polarity)));
    }

    public function checkFlag($flag, $level = 3)
    {
        if ($level === 1) {
            $flag = $flag . '.*.*';
        }
        if ($level === 2) {
            if (strpos($flag, '.') !== false) {
                $flag = $this->_namespace . '.' . $flag;
            } else {
                $flag = $this->_namespace . '.' . $flag . '.*';
            }
        }
        if ($level === 3) {
            if (strpos($flag, '.') !== false) {
                $flag = $this->_namespace . '.' . $flag;
            } else {
                $flag = $this->_namespace . '.' . ($this->container->get('request')->attributes->get('flag') ?? '*') . '.' . $flag;
            }
        }
        return $this->checkAgaintsUserFlags($flag);
    }

    /**
     * @return \Doctrine\ORM\EntityManager Description
     */
    protected function em()
    {
        return $this->container->get('manager_container')->getManager();
    }

    /**
     * 
     * @return User
     */
    public function getUser()
    {
        return $this->_user;
    }

    protected function generatePassword($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    public static function hashPassword($password, $salt)
    {
        return hash('sha512', $password . strrev($salt));
    }

    public function createPasswordResetToken($username)
    {
        $user = $this->retrieveUserByUsername($username);
        if (empty($user)) {
            return false;
        }
        $token = hash('SHA256', $user->getId() . Uuid::uuid4());
        $userPasswordReset = new UserPasswordReset($user, $token, new \DateTime());
        $this->em()->persist($userPasswordReset);
        $this->em()->flush();
        return $userPasswordReset;
    }

    public function resetPassword($token, $password)
    {
        if (null !== ($userPasswordReset = $this->verifyResetToken($token))) {
            $user = $userPasswordReset->getUser();
            $this->setPassword($password, $user);
            $this->em()->remove($userPasswordReset);
            $this->em()->flush();
            return true;
        } else {
            return false;
        }
    }

    public function setPassword($password, User $user = null)
    {
        if (empty($user)) {
            $user = $this->_user;
        }
        $hash = self::hashPassword($password, (string) $user->getRegisterDate()->getTimestamp());
        $user->setPassword($hash);
    }

    public function verifyPassword($password, User $user = null)
    {
        if ($user === null) {
            $user = $this->_user;
        }
        $hash = self::hashPassword($password, (string) $user->getRegisterDate()->getTimestamp());
        if ($user->getPassword() === $hash) {
            return true;
        }
        return false;
    }

    /**
     * 
     * @param string $token
     * @return UserPasswordReset
     */
    public function verifyResetToken($token)
    {
        $userPasswordReset = $this->em()->getRepository('Base:UserPasswordReset')->findOneBy(['token' => $token]);
        if (empty($userPasswordReset)) {
            return null;
        }
        $now = new \DateTime();
        $now->sub(new \DateInterval('P2D'));
        if ($userPasswordReset->getDate() > $now) {
            return $userPasswordReset;
        }
        return null;
    }

    public function setNamespace($namespace)
    {
        $this->_namespace = $namespace;
        return $this;
    }

}

?>
