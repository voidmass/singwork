<?php

namespace Singwork\User;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Singwork\Traits\EntityManagerAwareTrait;
use Singwork\Model\Entities\User;
use Singwork\Model\Entities\UserEmailPermission;
use Singwork\Model\Entities\UserEmailReport;
use Singwork\Event\EmailPermissionChangeEvent;

/**
 * Description of EmailPermissionManager
 *
 * @author matej.smisek
 */
class EmailPermissionManager implements ContainerAwareInterface
{

    use ContainerAwareTrait;
    use EntityManagerAwareTrait;

    public function __construct()
    {
        
    }

    public function setPermission($email, $permission, $verified = true)
    {
        $currentPermission = $this->getPermission($email);
        if ($currentPermission === null) {
            $currentPermission = new UserEmailPermission($email, $permission, new \DateTime('now'), $this->generateToken($email));
            $this->em()->persist($currentPermission);
            $currentPermission->setVerified($verified);
        } else {
            $currentPermission->setPermission($permission)->setModified(new \DateTime('now'));
            $currentPermission->setVerified($verified);
        }
        $event = new EmailPermissionChangeEvent($email, $currentPermission);
        $this->container->get('event_dispatcher')->dispatch(EmailPermissionChangeEvent::NAME, $event);
        $this->em()->flush();
    }

    public function verifyPermission($email)
    {
        $this->getPermission($email)->setVerified(true);
        $this->em()->flush();
                
    }

    public function allow($email, $verified = true)
    {
        $this->setPermission($email, true, $verified);
    }

    public function disallow($email)
    {
        $this->setPermission($email, false);
    }

    /**
     * 
     * @param string $email
     * @return UserEmailPermission
     */
    public function getPermission($email)
    {
        return $this->em()->getRepository('Base:UserEmailPermission')->find($email);
    }

    /**
     * 
     * @param string $email
     * @return boolean
     */
    public function isAllowed($email)
    {
        return $this->getPermission($email)->isAllowed();
    }

    protected function generateToken($email)
    {
        return hash('SHA1', $email . microtime());
    }

    public function getEmailFromToken($token)
    {
        return $this->em()->getRepository('Base:UserEmailPermission')->findOneBy(['token' => $token])->getEmail();
    }

    public function reportEmail($token, $source)
    {
        $report = new UserEmailReport($this->getEmailFromToken($token), $source, new \DateTime('now'));
        $this->em()->persist($report);
        $this->em()->flush();
        return true;
    }

    public function generateVerifyUrl($email)
    {
        return $this->container->get('router')->generate($_ENV['config']['email']['verify_url'], ['token' => $this->getPermission($email)->getToken()], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    public function generateUnsubscribeUrl($email)
    {
        return $this->container->get('router')->generate($_ENV['config']['email']['unsubscribe_url'], ['token' => $this->getPermission($email)->getToken()], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    public function generateResubscribeUrl($email)
    {
        return $this->container->get('router')->generate($_ENV['config']['email']['resubscribe_url'], ['token' => $this->getPermission($email)->getToken()], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    public function generateReportUrl($email)
    {
        return $this->container->get('router')->generate($_ENV['config']['email']['report_url'], ['token' => $this->getPermission($email)->getToken()], UrlGeneratorInterface::ABSOLUTE_URL);
    }

}
