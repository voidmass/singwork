<?php
namespace Singwork\Console;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Console\Application;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;

/**
 * Description of CliInterpreter
 *
 * @author matej.smisek
 */
class CliInterpreter implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     *
     * @var CommandAbstract[]
     */
//    protected $_commands;
    /**
     *
     * @var Application
     */
    protected $_consoleApp;

    public function __construct($container)
    {
        $this->container = $container;


//        $application = $this->container->get('application');
//        $this->_commands = array_merge($application->getCliCommands(), $this->getDefaultCommands());

        $this->_consoleApp = new Application("Singwork Command Line interface");  
        $this->_consoleApp->getHelperSet()->set(new ConnectionHelper($this->container->get('manager_container')->getManager()->getConnection()), 'db');
        $this->_consoleApp->getHelperSet()->set(new EntityManagerHelper($this->container->get('manager_container')->getManager()), 'em');
        $this->_consoleApp->setCatchExceptions(true);       
    }

    protected function registerDefaultCommands(Application $application)
    {
        ConsoleRunner::addCommands($this->_consoleApp);
        $application->add(new Commands\OrderCommand($this->container));
        $application->add(new Commands\DpdCommand($this->container));
               
    }

    public function exec($command, $args)
    {
        $this->registerDefaultCommands($this->_consoleApp);
        $this->container->get('application')->registerCliCommands($this->_consoleApp);

        $this->_consoleApp->run();
    }

}
