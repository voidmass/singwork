<?php

namespace Singwork\Console;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Command\Command;

/**
 * Description of CommandAbstract
 *
 * @author matej.smisek
 */
class CommandAbstract extends Command implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

}
