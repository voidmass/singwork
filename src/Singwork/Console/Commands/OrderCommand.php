<?php

namespace Singwork\Console\Commands;

use Singwork\Console\CommandAbstract;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Singwork\Traits\EntityManagerAwareTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Singwork\Model\Entities\Cart\Order;
use Singwork\Util\Util;

/**
 * Description of DoctrineCommand
 *
 * @author matej.smisek
 */
class OrderCommand extends CommandAbstract
{

    use EntityManagerAwareTrait;

    protected function configure()
    {
        $this
                ->setName('base:order')
                ->setDescription('API for Order shop')
                ->setHelp('Access relay to the doctrine command')
                ->addArgument('subcommand', InputArgument::REQUIRED, 'Command to execute over order')
                ->addArgument('order_ids', InputArgument::IS_ARRAY, 'IDs of and order')
                ->addOption('state', 's', InputOption::VALUE_OPTIONAL, 'Specifies state to set')
                ->addOption('notification', 'o', InputOption::VALUE_OPTIONAL, 'Specifies if to send notification')
                ->addOption('json', 'j', InputOption::VALUE_NONE, 'Specifies whether the return value should be in json format');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        switch ($input->getArgument('subcommand')) {
            case 'advance':
                foreach ($input->getArgument('order_ids') as $order_id) {
                    $order = $this->em()->getRepository('Base:Cart\Order')->find($order_id);

                    if ($input->getOption('notification') === 'no' || $input->getOption('notification') === 'n' || $input->getOption('notification') === 'false') {
                        $sendNotification = false;
                    } else {
                        $sendNotification = true;
                    }
                    $this->container->get('order_manager')->advanceOrder($order, $sendNotification);

                    if ($input->getOption('json')) {
                        $output->write(json_encode(['result' => 'success', 'state' => $order->getState()]));
                    } else {
                        $output->writeln("Order \033[36m" . $order->getId() . "\033[0m advanced to \033[32m" . $order->getState() . "\033[0m state\n");
                    }
                }
                break;
            case "cancel":
                foreach ($input->getArgument('order_ids') as $order_id) {
                    $order = $this->em()->getRepository('Base:Cart\Order')->find($order_id);
                    $this->container->get('order_manager')->cancelOrder($order);
                    if ($input->getOption('json')) {
                        $output->write(json_encode(['result' => 'success', 'state' => $order->getState()]));
                    } else {
                        $output->writeln("Order \033[36m" . $order->getId() . "\033[0m canceled\n");
                    }
                }
                break;
            case "diff":
                $order1 = $this->em()->getRepository('Base:Cart\Order')->find($input->getArgument('order_ids')[0]);
                $order2 = $this->em()->getRepository('Base:Cart\Order')->find($input->getArgument('order_ids')[1]);
                $output->writeln($this->container->get('order_manager')->diffCarts($order1->getCart(), $order2->getCart()));
                break;
            case "get":
                $order = $this->em()->getRepository('Base:Cart\Order')->find($input->getArgument('order_ids')[0]);
                if ($order === null) {
                    $output->writeln('error');
                } else {
                    $output->writeln($this->getJsonOrder($order));
                }
                break;
            case "list":
                $orders = $this->em()->getRepository('Base:Cart\Order')->findAll();
                $list = [];
                foreach ($orders as $order) {
                    if (array_search($order->getState(), ['open', 'canceled', 'expired', 'completed']) !== false) {
                        continue;
                    }
                    $list[] = [
                        'id' => $order->getId(),
                        'name' => $order->getCart()->getName() . ' ' . $order->getCart()->getSurname() . '-' . $order->getId(),
                        'user' => $order->getCart()->getUser()->getId(),
                        'state' => $order->getState(),
                        'type' => $order->getCart()->getType(),
                        'closed_time' => $order->getCart()->getClosedTime()->format('Y-m-d'),
                        'delivery_date' => $order->getCart()->getDeliveryDate() !== null ? $order->getCart()->getDeliveryDate()->format('Y-m-d') : '',
                        'delivery_time' => $order->getCart()->getDeliveryTime(),
                        'delivery_type' => $order->getCart()->getDelivery()->getType(),
                        'price' => $order->getCart()->getTotalPrice(),
                        'gibon' => $order->isGibonScheduled(),
                        'gibon_status' => $order->getGibonStatus(),
                        'invoice_id' => $order->getInvoiceId(),
                    ];
                }
                $output->writeln(json_encode($list));
                break;
            case "set":
                $order = $this->em()->getRepository('Base:Cart\Order')->find($input->getArgument('order_ids')[0]);
                if ($order === null) {
                    $output->writeln('error');
                    return;
                }
                if ($input->getOption('notification') === 'yes' || $input->getOption('notification') === 'y' || $input->getOption('notification') === 'true') {
                    $sendNotification = true;
                } else {
                    $sendNotification = false;
                }
                $this->container->get('order_manager')->setOrderStatus($input->getOption('state'), $order, $sendNotification);
                if ($input->getOption('json')) {
                    $output->write(json_encode(['result' => 'success', 'state' => $order->getState()]));
                } else {
                    $output->writeln("Order \033[36m" . $order->getId() . "\033[0m advanced to \033[32m" . $order->getState() . "\033[0m state\n");
                }
                break;
            case "notification":
                $order = $this->em()->getRepository('Base:Cart\Order')->find($input->getArgument('order_ids')[0]);
                if ($order === null) {
                    $output->writeln('error');
                    return;
                }
                $this->container->get('order_manager')->sendOrderNotification($order);
                if ($input->getOption('json')) {
                    $output->write(json_encode(['result' => 'success']));
                } else {
                    $output->writeln("Order \033[36m" . $order->getId() . "\033[0m send notification for \033[32m" . $order->getState() . "\033[0m state\n");
                }
                break;
            case "set-gibon":
                foreach ($input->getArgument('order_ids') as $order_id) {
                    $order = $this->em()->getRepository('Base:Cart\Order')->find($order_id);
                    if ($input->getOption('state') === "delete") {
                        $order->setGibonScheduled(false);
                        $order->setGibonStatus(null);
                    } else {
                        $order->setGibonScheduled(true);
                        $order->setGibonStatus($input->getOption('state'));
                    }
                }
                $this->em()->flush();
                break;
            case "edit":
               
                /* @var $order \Singwork\Model\Entities\Cart\Order */
                $order = $this->em()->getRepository('Base:Cart\Order')->find($input->getArgument('order_ids')[0]);
          
                $data = json_decode($input->getOption('state'), true);
 
                $order->getCart()->setName($data['name']);
                $order->getCart()->setSurname($data['surname']);
                $order->getCart()->setEmail($data['email']);
                $order->getCart()->setPhone($data['phone']);
                if ($data['shipping']) {
                    if (empty($order->getCart()->getShippingAddress())) {
                        $order->getCart()->setShippingAddress(new \Singwork\Model\Entities\Cart\CartShippingAddress('', '', '', 'CZ', $order->getCart()));
                    }
                    $order->getCart()->getShippingAddress()->setName($data['shipping']['name']);
                    $order->getCart()->getShippingAddress()->setStreet($data['shipping']['street']);
                    $order->getCart()->getShippingAddress()->setCity($data['shipping']['city']);
                    $order->getCart()->getShippingAddress()->setZipcode($data['shipping']['zipcode']);
                }

                $order->getCart()->getInvoiceAddress()->setStreet($data['invoice']['street']);
                $order->getCart()->getInvoiceAddress()->setCity($data['invoice']['city']);
                $order->getCart()->getInvoiceAddress()->setZipcode($data['invoice']['zipcode']);

                $order->getCart()->setCompanyName($data['invoice']['company']);
                $order->getCart()->setCompanyId($data['invoice']['ico']);
                $order->getCart()->setCompanyTaxId($data['invoice']['tax']);

                $order->getCart()->setDelivery($this->em()->getRepository('Base:Shop\DeliveryType')->findOneBy(['type' => $data['delivery']]));
                $order->getCart()->setPayment($this->em()->getRepository('Base:Shop\PaymentType')->findOneBy(['type' => $data['payment']]));
                
                $order->getCart()->setDeliveryDate(new \DateTime($data['deliveryDate']));
                
                $catalog = $order->getCart()->getCataloguedProducts();
                $toRemove = array_diff_key($catalog, $data['products']);
                $toAdd = array_diff_key($data['products'], $catalog);
                foreach ($toRemove as $id => $amount) {
                    $element = $order->getCart()->getProductByTemplate($this->extractTemplateFromCatalog($id));
                    $this->em()->remove($element);
                    $order->getCart()->getProducts()->removeElement($element);
                }
                foreach ($toAdd as $id => $amount) {
                    $product = $this->extractTemplateFromCatalog($id);  
                    $cartProduct = new \Singwork\Model\Entities\Cart\CartProduct($product, $product->getCalculatedPrice(), $product->getSaleAmount(), $amount);
                    $cartProduct->setProductVariant($this->extractTemplateVariantFromCatalog($id));
                    $order->getCart()->addProduct($cartProduct);
                    $this->em()->persist($cartProduct);
                }
                foreach($data['products'] as $id => $amount) {
                    
                    $order->getCart()->getProductByTemplate($this->extractTemplateFromCatalog($id))->setAmount($amount);
                }
                $order->setProductsPrice($order->getCart()->getProductPrice());
                $this->em()->flush();

                if ($input->getOption('json')) {
                    $output->write(json_encode(['result' => 'success']));
                } else {
                    $output->writeln("Order \033[36m" . $order->getId() . "\033[0m send notification for \033[32m" . $order->getState() . "\033[0m state\n");
                }
                break;

            default:
                $output->writeln("Subcommand >" . $input->getArgument('subcommand') . "< not defined");
        }
    }
    /**
     * 
     * @param type $catalog
     * @return \Singwork\Model\Entities\Shop\Product
     */
    protected function extractTemplateFromCatalog($catalog) {
        $product = $this->em()->getRepository('Base:Shop\Product')->findOneBy(['catalog' => $catalog]);
        if (empty($product)) {
            $productVariant = $this->em()->getRepository('Base:Shop\ProductVariant')->findOneBy(['catalog' => $catalog]);
            if (empty($productVariant)) {
                return null;
            }
            
            $product = $productVariant->getProduct();
        }
        return $product;
    }
    /**
     * 
     * @param type $catalog
     * @return \Singwork\Model\Entities\Shop\ProductVariant
     */
    protected function extractTemplateVariantFromCatalog($catalog) {
        $variant = $this->em()->getRepository('Base:Shop\ProductVariant')->findOneBy(['catalog' => $catalog]);
        if (empty($variant)) {           
                return null;
        }
        return $variant;
    }
    
    protected function getJsonOrder(Order $order)
    {
        $output = [
            'id' => $order->getId(),
            'user' => $order->getCart()->getUser()->getId(),
            'status' => $order->getState(),
            'products' => [],
            'type' => $order->getCart()->getType(),
            'email' => $order->getCart()->getEmail(),
            'phone' => $order->getCart()->getPhone(),
            'closed_time' => Util::convertDate($order->getCart()->getClosedTime(), 'ISOFULL'),
            'note' => $order->getCart()->getNote(),
            'name' => $order->getCart()->getName(),
            'surname' => $order->getCart()->getSurname(),
        ];
        foreach ($order->getCart()->getProducts() as $product) {
            $output['products'][] = [
                'id' => empty($product->getProductVariant()) ? $product->getProduct()->getCatalog() : $product->getProductVariant()->getCatalog(),
                'price' => $product->getPrice(),
                'amount' => $product->getAmount(),
            ];
        }
        $output['delivery'] = [
            'type' => $order->getCart()->getDelivery()->getType(),
            'payment' => $order->getCart()->getPayment()->getType(),
            'date' => Util::convertDate($order->getCart()->getDeliveryDate(), 'ISO'),
            'time' => $order->getCart()->getDeliveryTime(),
            'fee' => 0,
            'note' => $order->getCart()->getUser()->getDeliveryNote() ?? 'Doručit na recepci, nejlépe zavolat a oni si navedou',
        ];
        if ($order->getCart()->getType() === \Singwork\Model\Entities\Cart\Cart::TYPE_SINGLE_USE && $order->getProductsPrice() < 1500) {
            $output['delivery']['fee'] = 100;
        }
        $output['invoice'] = [
            'name' => $order->getCart()->getName() . ' ' . $order->getCart()->getSurname(),
            'street' => $order->getCart()->getInvoiceAddress()->getStreet(),
            'city' => $order->getCart()->getInvoiceAddress()->getCity(),
            'zipcode' => $order->getCart()->getInvoiceAddress()->getZipcode(),
            'company' => $order->getCart()->getCompanyName(),
            'ico' => $order->getCart()->getCompanyId(),
            'tax' => $order->getCart()->getCompanyTaxId()
        ];
        if (empty($order->getCart()->getShippingAddress())) {
            $output['shipping'] = [
                'name' => $order->getCart()->getFullName(),
                'street' => $order->getCart()->getInvoiceAddress()->getStreet(),
                'city' => $order->getCart()->getInvoiceAddress()->getCity(),
                'zipcode' => $order->getCart()->getInvoiceAddress()->getZipcode(),
                'pickup' => $order->getCart()->getSelectedPickup()
            ];
        } else {
            $output['shipping'] = [
                'name' => $order->getCart()->getShippingAddress()->getName(),
                'street' => $order->getCart()->getShippingAddress()->getStreet(),
                'city' => $order->getCart()->getShippingAddress()->getCity(),
                'zipcode' => $order->getCart()->getShippingAddress()->getZipcode(),
                'pickup' => $order->getCart()->getSelectedPickup()
            ];
        }
        $output['gibon'] = [
            'scheduled' => $order->isGibonScheduled(),
            'status' => $order->getGibonStatus(),
        ];
        return json_encode($output);
    }

}
