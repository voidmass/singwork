<?php

namespace Singwork\Console\Commands;

use Singwork\Console\CommandAbstract;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Singwork\Traits\EntityManagerAwareTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Singwork\Util\Poster;
use Singwork\Model\Entities\Shop\PickupDpd;

/**
 * Description of DoctrineCommand
 *
 * @author matej.smisek
 */
class DpdCommand extends CommandAbstract
{

    use EntityManagerAwareTrait;

    protected function configure()
    {
        $this
                ->setName('base:dpd')
                ->setDescription('API for import dpd')
                ->setHelp('Access relay to the doctrine command')
                ->addOption('json', 'j', InputOption::VALUE_NONE, 'Specifies whether the return value should be in json format')
                ->addOption('force', 'f', InputOption::VALUE_NONE, 'Deletes entire database and imports it again');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $result = json_decode((new Poster())->send('GET', 'http://pickup.dpd.cz/api/get-all'), true);
        if (empty($result)) {
            $output->writeln("Remote call failed (empty)");
            return;
        }
        if ($result['code'] != 200) {
            $output->writeln("Remote call failed (code: " . $result['code'] . ")");
            return;
        }
        $hash = $this->em()->getRepository('Base:Setting')->findOneBy(['id' => 'dpd_pickup']);
        $update = false;
        if (empty($hash)) {
            $update = true;
        } elseif ($hash->getValue() != $result['hash']) {
            $update = true;
        }
        if ($input->getOption('force') || $update === true) {
            $this->savePlaces($result['data']['items']);
            if (empty($hash)) {
                $this->em()->persist(new \Singwork\Model\Entities\Setting('dpd_pickup', $result['hash']));
            } else {
                $hash->setValue($result['hash']);
            }
            $this->em()->flush();
            $output->writeln("Database list updated");
            return;
        }
        $output->writeln("Nothing to update");
        return;
    }

    protected function savePlaces($places)
    {
        $connection = $this->em()->getConnection();
        $platform = $connection->getDatabasePlatform();
        $connection->executeUpdate($platform->getTruncateTableSQL('singwork_shop_pickup_dpd', false));

        foreach ($places as $place) {
            $this->em()->persist(new PickupDpd($place['id'], $place['company'], $place['street'], $place['city'], $place['house_number'], $place['postcode'], $place['phone'], $place['cardpayment_allowed'] == 1 ? true : false, $this->combineHours($place['hours']), $place['longitude'], $place['latitude']));
        }
    }

    protected function combineHours($days)
    {
        $return = [];
        foreach ($days as $day) {
            $return [$day['dayName']] = empty($day['openMorning']) ? 'Zavřeno' : $day['openMorning'] . ' - ' . $day['closeAfternoon'];
        }
        return json_encode($return, JSON_UNESCAPED_UNICODE);
    }

}
