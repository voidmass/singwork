<?php

namespace Singwork\Event;

use Symfony\Component\EventDispatcher\Event;
use Singwork\Model\Entities\Cart\OrderState;
use Singwork\Model\Entities\Cart\Order;

/**
 * Description of OrderNextStateEvent
 *
 * @author matej.smisek
 */
class OrderNextStateEvent extends Event
{

    const NAME = 'singwork.order.next.';

    /**
     * @var Order
     */
    protected $order;

    /**
     *
     * @var string
     */
    protected $previousState;

    /**
     *
     * @var string
     */
    protected $nextState;
    
    protected $sendNotification;
    
    public function __construct(Order $order,$previousState,$nextState = null, $sendNotification = true)
    {
        $this->order = $order;
        $this->previousState = $previousState;
        $this->nextState = $nextState;
        $this->sendNotification = $sendNotification;
    }
    public function getOrder(): Order
    {
        return $this->order;
    }

    public function getPreviousState()
    {
        return $this->previousState;
    }

    public function getNextState()
    {
        return $this->nextState;
    }

    public function setOrder(Order $order)
    {
        $this->order = $order;
        return $this;
    }

    public function setPreviousState($previousState)
    {
        $this->previousState = $previousState;
        return $this;
    }

    public function setNextState($nextState = null)
    {
        $this->nextState = $nextState;
        return $this;
    }

    
    public function getSendNotification()
    {
        return $this->sendNotification;
    }

    public function setSendNotification($sendNotification)
    {
        $this->sendNotification = $sendNotification;
        return $this;
    }



}
