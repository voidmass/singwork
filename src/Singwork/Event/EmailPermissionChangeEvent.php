<?php
namespace Singwork\Event;

use Symfony\Component\EventDispatcher\Event;

use Singwork\Model\Entities\UserEmailPermission;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailPermissionChange
 *
 * @author matej.smisek
 */
class EmailPermissionChangeEvent extends Event
{
    const NAME = 'singwork.email_permission.change';

    /**
     *
     * @var string
     */
    protected $email;
    /**
     * @var UserEmailPermission
     */
    protected $permission;
    public function __construct($email, UserEmailPermission $permission)
    {
        $this->email = $email;
        $this->permission = $permission;
    }
    public function getEmail()
    {
        return $this->email;
    }

    public function getPermission(): UserEmailPermission
    {
        return $this->permission;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setPermission(UserEmailPermission $permission)
    {
        $this->permission = $permission;
        return $this;
    }










}
