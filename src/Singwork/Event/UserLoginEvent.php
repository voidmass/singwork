<?php
namespace Singwork\Event;

use Symfony\Component\EventDispatcher\Event;

use Singwork\Model\Entities\User;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserLoginEvent
 *
 * @author matej.smisek
 */
class UserLoginEvent extends Event
{
    const NAME = 'singwork.user.login';

    /**
     *
     * @var User
     */
    protected $user;
    /**
     * @var string
     */
    protected $username;
    
    /**
     * @var boolean
     */
    protected $regular;
    /**
     *
     * @var User
     */
    protected $oldUser;
    
    public function __construct($user, $username, $regular, $oldUser)
    {
        $this->user = $user;
        $this->username = $username;
        $this->regular = $regular;
        $this->oldUser = $oldUser;
    }
    
    public function getUser()
    {
        return $this->user;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }
    public function getRegular()
    {
        return $this->regular;
    }

    public function setRegular($regular)
    {
        $this->regular = $regular;
        return $this;
    }

    public function getOldUser()
    {
        return $this->oldUser;
    }

    public function setOldUser($oldUser)
    {
        $this->oldUser = $oldUser;
        return $this;
    }





}
