<?php

namespace Singwork\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\ParameterBag;
use Singwork\Model\Entities\User;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserVerifyAccessEvent
 *
 * @author matej.smisek
 */
class UserVerifyAccessEvent extends Event
{

    const NAME = 'singwork.user.access';

    /**
     *
     * @var User
     */
    protected $user;

    /**
     * @var ParameterBag
     */
    protected $bag;

    /**
     * @var boolean
     */
    protected $access;

    public function __construct($user, ParameterBag $bag, $access)
    {
        $this->user = $user;
        $this->bag = $bag;
        $this->access = $access;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getBag(): ParameterBag
    {
        return $this->bag;
    }

    public function getAccess()
    {
        return $this->access;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function setBag(ParameterBag $bag)
    {
        $this->bag = $bag;
        return $this;
    }

    public function setAccess($access)
    {
        $this->access = $access;
        return $this;
    }

}
