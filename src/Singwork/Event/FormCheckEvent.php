<?php

namespace Singwork\Event;

use Symfony\Component\EventDispatcher\Event;
use Singwork\Model\Entities\User;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormCheckEvent
 *
 * @author matej.smisek
 */
class FormCheckEvent extends Event
{

    const NAME = 'singwork.form.check.';

    /**
     *
     * @var \Singwork\Form\Form
     */
    protected $form;

    /**
     * @var \Singwork\Form\FormData
     */
    protected $formData;

    /**
     * @var array
     */
    protected $errorStack;

    /**
     *
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    public function __construct(\Singwork\Form\Form $form, \Singwork\Form\FormData $formData, $errorStack, $request)
    {
        $this->form = $form;
        $this->formData = $formData;
        $this->errorStack = $errorStack;
        $this->request = $request;
    }

    public function getForm(): \Singwork\Form\Form
    {
        return $this->form;
    }

    public function getFormData(): \Singwork\Form\FormData
    {
        return $this->formData;
    }

    public function getErrorStack()
    {
        return $this->errorStack;
    }

    public function setForm(\Singwork\Form\Form $form)
    {
        $this->form = $form;
        return $this;
    }

    public function setFormData(\Singwork\Form\FormData $formData)
    {
        $this->formData = $formData;
        return $this;
    }

    public function setErrorStack($errorStack)
    {
        $this->errorStack = $errorStack;
        return $this;
    }

    public function getRequest(): \Symfony\Component\HttpFoundation\Request
    {
        return $this->request;
    }

    public function setRequest(\Symfony\Component\HttpFoundation\Request $request = null)
    {
        $this->request = $request;
        return $this;
    }

    public function addErrorToStack($error)
    {
        foreach ($this->errorStack as &$stack) {
            if ($stack['id'] == $error['id']) {
                $stack = $error;
                return;
            }
        }
        $this->errorStack[] = $error;
    }

}
