<?php

namespace Singwork\Event;

use Symfony\Component\EventDispatcher\Event;
use Singwork\Model\Entities\Cart\OrderState;
use Singwork\Model\Entities\Cart\Order;

/**
 * Description of OrderNextStateEvent
 *
 * @author matej.smisek
 */
class OrderCancelStateEvent extends Event
{

    const NAME = 'singwork.order.cancel.';

    /**
     * @var Order
     */
    protected $order;

    /**
     *
     * @var string
     */
    protected $previousState;

    /**
     *
     * @var string
     */
    protected $nextState;
    
    
    public function __construct(Order $order,$previousState,$nextState = null)
    {
        $this->order = $order;
        $this->previousState = $previousState;
        $this->nextState = $nextState;
    }
    public function getOrder(): Order
    {
        return $this->order;
    }

    public function getPreviousState()
    {
        return $this->previousState;
    }

    public function getNextState()
    {
        return $this->nextState;
    }

    public function setOrder(Order $order)
    {
        $this->order = $order;
        return $this;
    }

    public function setPreviousState($previousState)
    {
        $this->previousState = $previousState;
        return $this;
    }

    public function setNextState($nextState = null)
    {
        $this->nextState = $nextState;
        return $this;
    }


}
