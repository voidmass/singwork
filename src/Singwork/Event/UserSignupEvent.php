<?php

namespace Singwork\Event;

use Symfony\Component\EventDispatcher\Event;
use Singwork\Model\Entities\User;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserLoginEvent
 *
 * @author matej.smisek
 */
class UserSignupEvent extends Event
{

    const NAME = 'singwork.user.signup';

    /**
     *
     * @var User
     */
    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

}
