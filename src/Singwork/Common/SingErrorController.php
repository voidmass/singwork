<?php
namespace Singwork\Common;

use Singwork\Content\Controller;
use Singwork\Util\Util;
use Singwork\Util\Server;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SingErrorController
 *
 * @author Matej Smisek
 */
class SingErrorController extends Controller {
    
    public function preInit()  {}
    public function postInit() {}
   
    public function run() {
        $this->_view->home = 'http://'.$this->core()->getConfig()['website']['url'].'/';
        $this->_view->error = $this->prepareError();
        if ($this->type == '404') {
            Util::httpHeader('404 Not Found', 404);
            if (file_exists($this->core()->getPublicFolder().'/'.$this->core()->getConfig()['error']['404']) && !empty($this->core()->getConfig()['error']['404'])) {
                $this->_view->setPage($this->core()->getConfig()['error']['404']);
            } else {
                $this->_view->setPage($this->core()->getFrameworkPath().'/_default/404.phtml');
            }
            $this->_view->url = Server::get('REQUEST_URI');                        
        } else {        
            Util::httpHeader('500 Internal Server Error', 500);
            if (file_exists($this->core()->getPublicFolder().'/'.$this->core()->getConfig()['error']['error']) && !empty($this->core()->getConfig()['error']['error'])) {
               
                $this->_view->setPage($this->core()->getConfig()['error']['error']);
            } else {
                $this->_view->setPage($this->core()->getFrameworkPath().'/_default/error.phtml');
            }
        }
    }
    protected function prepareError() {
        $return = '<div class="alert callout">'.core()->exception->getMessage().'</div>
       <table class="warning callout">';
        foreach (core()->exception->getTrace() as $k => $v) {
              $return .= '<tr>
                <td>'.($v['class']==''?'':$v['class'].'->').$v['function'].'(';
                foreach ($v['args'] as $key => $arg) {
                    $return .= $arg;
                    if ($key !== count($v['args'])-1) {
                    $return .= ',';
                    }
                }
                $return .= ')</td>
                <td>'.$v['line'].'</td>
                <td>'.$v['file'].'</td>
              </tr>';
//              echo print_r($v,true).'<br />';
             
        }
        $return .= '</table>';
        return $return;
    }
            
}
