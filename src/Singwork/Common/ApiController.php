<?php
namespace Singwork\Common;

use Singwork\Content\Controller;
use Singwork\Table\ApiKey;
use Singwork\Exception\SetupException;
use Singwork\Exception\RuntimeSingException;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ApiController
 *
 * @author Matej Smisek
 */
class ApiController extends Controller {

    /**
     *
     * @var Api
     */
    protected $_class;
    /**
     *
     * @var Table_ApiKey
     */
    protected $_t_api;

    public function __construct() {
        parent::__construct();
        $this->_t_api = new ApiKey();
    }
    
    public function preInit() {
        parent::preInit();
        $this->disableRendering();
        
    }

    public function run() {
        $url = core()->getUrlProcessor()->getUrl();
        try {
            $this->verifyKey();
            if (count($url) < 3) {
                throw new SetupException('Wrong API Url -- Not enough commands');
            }
            $className = $url[1]->getUrl() . 'Api';
            if (!file_exists(core()->getWebsitePath() . '/_api/' . $className . '.php') && !file_exists(core()->getFrameworkPath() . '/_application/_api/' . $className . '.php')) {
                throw new SetupException('API ' . $className . ' does not exists. Path: ' . core()->getWebsitePath() . '/_api/' . $className . '.php', 500);
            }
            $this->_class = new $className();
            $this->_class->execute($url[2]->getUrl());
        } catch (\Exception $e) {
            $this->outputJson($e->getMessage(),'error');
        }
    }
    protected function verifyKey() {
        if (empty($_POST['api_key'])) {
            throw new RuntimeSingException('No key defined in API request');
        }
        $key = $this->_t_api->getIpKey($_SERVER['REMOTE_ADDR'], $_POST['api_key']);
        if ($key === false) {
            throw new RuntimeSingException('Wrong API key');
        }
    }
    
    
    
}
