<?php

namespace Singwork\Common;

use Singwork\Content\Controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SingDeepControllerController
 *
 * @author Matej Smisek
 */
class SingDeepController extends Controller {

    public function preInit() {
        $this->_view->setPage($this->core()->getFrameworkPath() . '/_default/404.phtml');
        if ($_POST['method'] === 'ajax') {
            $this->disableLayout();
        }
    }

    public function postInit() {
        
    }

    public function run() {
        $method = $this->_param[0] . 'Action';
        if (!method_exists($this, $method)) {
            $method = $this->_param['action'] . 'Action';
            if (!method_exists($this, $method)) {
                $this->error('No such method ->' . $method);
                return;
            }
        }
        $this->$method();
    }

    public function deepgalleryAction() {
        $s = new \Singwork\Database\Select('deep_gallery', ['deep_gallery' => ['type'], 'deep_gallery_item' => ['url', 'thumbnail', 'title', 'text', 'options']]);
        $s->leftJoin('deep_gallery_item', 'id', 'gallery_id');
        $s->where('`deep_gallery_item`.`visible` = 1');
        $s->where(' AND ');
        $s->where('`deep_gallery`.`id` = "' . $_POST['id'] . '"');
        $s->order('order', 'ASC', 'deep_gallery_item');
        $data = [];
        $r = $s->execute();
        while ($d = $r->fetch()) {
            $d['options'] = json_decode($d['options']);
            $data [] = $d;
        }
        $this->data($data);
        exit;
        return;
    }

    public function apiAction() {
        
    }

}
