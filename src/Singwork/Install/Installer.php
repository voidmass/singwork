<?php
namespace Singwork\Install;
/**
 * Description of Installer
 *
 * @author Matej Smisek
 */
class Installer {
    
    public function __construct() {
        
    }
    public function install() {
        
    }
    
    public function prepareFolderStructure() {
        
    }
    
    public function copyFiles() {
        
    }
    
    public function createTables() {
        
    }
}
