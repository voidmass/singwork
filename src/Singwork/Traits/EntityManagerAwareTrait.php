<?php
namespace Singwork\Traits;

use Doctrine\ORM\EntityManager;
/**
 * Description of EntityManagerAware
 *
 * @author matej.smisek
 */
trait EntityManagerAwareTrait
{
    /**
     * 
     * @return EntityManager
     */
    protected function entityManager($identifier = null): EntityManager
    {
        return $this->container->get('manager_container')->getManager($identifier);
    }

    /**
     * Shortcut for entityManager
     * 
     * @return EntityManager
     */
    protected function em($identifier = null)
    {
        return $this->entityManager($identifier);
    }
}
