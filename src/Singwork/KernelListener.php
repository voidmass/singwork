<?php

namespace Singwork;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\EventListener\ExceptionListener;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Singwork\Content\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Description of KernelListener
 *
 * @author Balgor
 */
class KernelListener implements EventSubscriberInterface
{

    /**
     *
     * @var ContainerInterface
     */
    protected $container;

    /**
     *
     * @var Response
     */
    protected $response;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => 'onKernelController',
            KernelEvents::VIEW => 'onKernelView',
            KernelEvents::EXCEPTION => 'onKernelException'
        );
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $request = $event->getRequest();
        $controller = $event->getController()[0];

        if ($controller instanceof Content\Controller) {
            $this->container->get('event_dispatcher')->addSubscriber($controller);
        }

        if (null !== $form = $request->attributes->get('form')) {

            $formData = Form\FormData::fromFields($request->request->get('type'), $request->request->get('fields'));
            if ($controller instanceof Content\Controller) {
                $result = $controller->prepareForm($formData, $form, $request->request->get('scope'));
                if ($result instanceof Response) {
                    $this->response = $result;
                    $event->setController([$this, 'returnResponse']);
                }
            }
        }
    }

    public function returnResponse()
    {
        return $this->response;
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        if ($event->getRequest()->getRequestFormat() === 'json') {
            $event->setResponse(new JsonResponse($event->getControllerResult()));
            return;
        }

        $view = new View($event->getRequest()->attributes->get('template'), $event->getControllerResult(), $this->container);
        $response = new Response($view->render());

        if ($event->getRequest()->getRequestFormat() === 'xml') {
            $response->headers->set('Content-Type', 'text/xml');
        }
        $event->setResponse($response);
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
//        $event->setResponse(new Response(@d($event->getException())));
//        return;
        $exception = $event->getException();
        $request = $event->getRequest();

//        $this->logException($exception, sprintf('Uncaught PHP Exception %s: "%s" at %s line %s', get_class($exception), $exception->getMessage(), $exception->getFile(), $exception->getLine()));

        $request = $this->duplicateRequest($exception, $request);

        try {
            $response = $event->getKernel()->handle($request, HttpKernelInterface::SUB_REQUEST, false);
        } catch (\Exception $e) {
//            $this->logException($e, sprintf('Exception thrown when handling an exception (%s: %s at %s line %s)', get_class($e), $e->getMessage(), $e->getFile(), $e->getLine()));

            $wrapper = $e;

            while ($prev = $wrapper->getPrevious()) {
                if ($exception === $wrapper = $prev) {
                    throw $e;
                }
            }

            $prev = new \ReflectionProperty('Exception', 'previous');
            $prev->setAccessible(true);
            $prev->setValue($wrapper, $exception);

            throw $e;
        }

        $event->setResponse($response);
    }

    /**
     * Clones the request for the exception.
     *
     * @param \Exception $exception The thrown exception
     * @param Request    $request   The original request
     *
     * @return Request $request The cloned request
     */
    protected function duplicateRequest(\Exception $exception, Request $request)
    {

        $request = $request->duplicate(null, null, $this->createAttributes($exception));
        $request->setMethod('GET');

        return $request;
    }

    protected function createAttributes(\Exception $exception)
    {
        $attributes = [];
        if ($exception instanceof NotFoundHttpException) {
            $attributes['_controller'] = $_ENV['config']['error']['404']['controller'] ?? [$this, 'defaultError'];
            $attributes['template'] = $_ENV['config']['error']['404']['template'] ?? '404.twig';
        } else {
            $attributes['_controller'] = $_ENV['config']['error']['error']['controller'] ?? [$this, 'defaultError'];
            $attributes['template'] = $_ENV['config']['error']['error']['template'] ?? 'error.twig';
        }

        $attributes['exception'] = $exception;
        return $attributes;
    }

    public function defaultError(Request $request)
    {
        return [
            'title' => 'ERROR',
            'tsack' => $request->get('exception')->getTraceAsString(),
            'stacktrace' => $request->get('exception')->getMessage(),
        ];
    }

}
