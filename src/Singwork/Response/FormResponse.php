<?php
namespace Singwork\Response;

use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Description of FormResponse
 *
 * @author matej.smisek
 */
class FormResponse extends JsonResponse
{

    
    const VALID = 'valid';
    const INVALID = 'invalid';
    
    public function __construct($status, $message = '', $actions = [], $fields = [])
    {                
        parent::__construct(['status' => $status, 'message' => $message, 'actions' => $actions, 'fields' => $errors]);                
    }
}
