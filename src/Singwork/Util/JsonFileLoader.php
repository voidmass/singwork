<?php

namespace Singwork\Util;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Exception\FileLoaderLoadException;

/**
 * Description of JsonFileLoader
 *
 * @author Balgor
 */

class JsonFileLoader
{
    protected static $EXTENSIONS = [
        'json'
    ];
    public static function load($filename, $paths) {
        if (!self::supports($filename)) {
            throw new FileLoaderLoadException('Wrong extension');
        }
        if (!is_array($paths)) {
            $paths = [$paths];
        }
        $locator = new FileLocator($paths);        
        return json_decode(file_get_contents($locator->locate($filename, null, true)), true);
    }
    
    public static function supports($filename) {
        return is_string($filename) && in_array(strtolower(pathinfo(
            $filename,
            PATHINFO_EXTENSION
        )),self::$EXTENSIONS);
    }
}
