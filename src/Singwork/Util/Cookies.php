<?php
namespace Singwork\Util;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cookies
 *
 * @author matej.smisek
 */
class Cookies
{
    /**
     *
     * @var array
     */
    protected $_setCookies = [];
    
    
    public function __construct()
    {
        
    }

    public function set($name, $value, $expiration = 0) {
        $this->_setCookies[$name] = ['content' => $value, 'expiration' => time() + $expiration];
    }
    public function getSetCookies() {
        return $this->_setCookies;
    }
}
