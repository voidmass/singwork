<?php

namespace Singwork\Util;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Encryption
 *
 * @author Matej Smisek
 */
class Encryption
{

    protected $_pubkey = '';
    protected $_privkey = '';
    protected $_public = '/var/www/framework/ssl/public.pem';
    protected $_private = '/var/www/framework/ssl/private.pem';
//    protected $_public = '/var/www/framework/ssl/new/csr.pem';
//    protected $_private = '/var/www/framework/ssl/new/key.pem';

    public function __construct()
    {
        $fp = fopen($this->_public, "r");
        $key_string = fread($fp, 8192);
        fclose($fp);
        $this->_pubkey = openssl_get_publickey($key_string);

        $fp = fopen($this->_private, "r");
        $key_string2 = fread($fp, 8192);
        fclose($fp);
        $this->_privkey = openssl_get_privatekey($key_string2);
    }

    public function encrypt($data)
    {       
        //pack('V', strlen($data)) . $data
        $encrypted = '';
        if (openssl_public_encrypt($data, $encrypted, $this->_pubkey)) {
            $data = base64_encode($encrypted);
//            $data = $encrypted;
        } else {
            throw new \Exception('Unable to encrypt data. Perhaps it is bigger than the key size?');
        }
        return $data;
    }

    public function decrypt($data)
    {
        $decrypted = '';
        $dataDecoded = base64_decode($data);
        if (openssl_private_decrypt($dataDecoded, $decrypted, $this->_privkey)) {
//            $header = unpack('Vsize', $decrypted);
//            $data = substr($decrypted, 4, $header['size'] + 4);
            return $decrypted;
        } else {
            $data = '';
        }
        return $data;
    }

}
