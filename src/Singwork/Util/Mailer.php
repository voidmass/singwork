<?php

namespace Singwork\Util;

use Singwork\Table;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Singwork\Model\Entities\User;
use Singwork\Model\Entities\MailServer;
use Singwork\Model\Entities\UserEmailServer;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mailer
 *
 * @author Matej Smisek
 */
class Mailer implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    protected $_swift_mailer;
    protected $_swift_transport;
      

    public function __construct()
    {
        $this->_swift_transport = new \Swift_SmtpTransport($_ENV['config']['email']['server'], $_ENV['config']['email']['port']);
        $this->_swift_transport
                ->setEncryption($_ENV['config']['email']['encryption'])
                ->setUsername($_ENV['config']['email']['username'])
                ->setPassword($_ENV['config']['email']['password']);

        $this->_swift_mailer = new \Swift_Mailer($this->_swift_transport);
    }

    public function send(\Swift_Message $message)
    {
        $message->setFrom($_ENV['config']['email']['email'], $_ENV['config']['email']['name']);
        if (!empty($_ENV['config']['email']['dkim'])) {
//            $this->dkim($message);
        }
        return $this->_swift_mailer->send($message);
    }

    protected function dkim(\Swift_Message $message)
    {
        $signer = new \Swift_Signers_DKIMSigner(file_get_contents($_ENV['config']['email']['dkim']['key']), $_ENV['config']['email']['dkim']['domain'], $_ENV['config']['email']['dkim']['selector']);
        $signer->ignoreHeader('Return-Path');
        $signer->setBodyCanon("relaxed");
        $message->attachSigner($signer);
    }
    
    
    public function sendAsUser(\Swift_Message $message, User $user) {
        $server = $user->getEmailServer()->getOutboundServer();
        $transport = new \Swift_SmtpTransport($server->getUrl(), $server->getPort());
        $transport
                ->setEncryption($server->getEncryption())
                ->setUsername($user->getEmail())
                ->setPassword($user->getEmailServer()->getPassword());
//                ->setPassword($this->container->get('encrypter')->decrypt($user->getEmailServer()->getPassword()));
        
        $mailer = new \Swift_Mailer($transport);
        
        $message->setFrom($user->getEmail(), $user->getFullName());
        return $mailer->send($message);
    }
}
