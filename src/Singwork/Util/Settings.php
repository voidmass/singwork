<?php
namespace Singwork\Startup;

use Singwork\Table;


/**
 * Description of Settings
 *
 * @author Matej Smisek
 */
class Settings {
    
    private $_settings;
    
    public function __construct($connection = 'main') {
        $r = (new Table\Settings($connection))->getAll();
        while ($d = $r->fetch()) {
            $this->_settings[$d['id']] = $d['value'];
        }
    }
    
    public function getValue($id) {
        return $this->_settings[$id];
    }
    
    public function getALl() {
        return $this->_settings;
    }

}

?>
