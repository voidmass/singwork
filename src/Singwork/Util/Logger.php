<?php 
namespace Singwork\Util;


use Singwork\Util\Util;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Logger
 *
 * @author Matej Smisek
 */
class Logger {
    public static $LOG = [
        'FATAL' => 1,       // Red (dark) / black
        'ERROR' => 2,       // Red
        'WARNING' => 3,     // Yellow
        'INFO'  => 4,       // Green
        'DEBUG' => 5,       // Blue
        'VERBOSE' => 6      // PURPLE
    ];
    public static $COLOR = [
        'FATAL' => 'bg_red',       // Red (dark) / black
        'ERROR' => 'red',       // Red
        'WARNING' => 'yellow',     // Yellow
        'INFO'  => 'green',       // Green
        'DEBUG' => 'blue',     // Blue
        'VERBOSE' => 'purple'     //purple
    ];
    protected $_level;
    
    protected $_file;
            
    protected $_stream;
    
    public function __construct($level = null, $file = null) {
        if ($level == null) {
            $this->_level = (core()->getConfig()['logging']['level'] != ''?core()->getConfig()['logging']['level']:'ERROR');
        } else {
            $this->_level = $level;
        }
        if ($file == null) {
            $this->_file = PUBLIC_PATH.(core()->getConfig()['logging']['file'] != ''?core()->getConfig()['logging']['file']:'/default.log');
        } else {
            $this->_file = $file;
        }
        $this->checkFile();
        $this->_execution_id = md5(time()+rand(0, 40000));
    }
    protected function checkFile() {        
        $this->_stream = fopen($this->_file, 'a');       
    }
    public function log($message, $level = 'ERROR') {
        if (self::$LOG[$this->_level] >= self::$LOG[$level]) {
            $line = explode('\n', $message);
            foreach ($line as $l) {
                $this->writeToLogNoColor($l, $level);
            }
        }
    }
    protected function writeToLog($message, $level) {     
       fwrite($this->_stream, sprintf(Util::getSprintfFormat(self::$COLOR[$level]),'['.date('d-m-Y H:i:s',time()).'] ['.$_SERVER['REMOTE_ADDR'].'] ['.strtoupper($level).'] '.$message)."\n");
    }
    protected function writeToLogNoColor($message, $level) {     
       fwrite($this->_stream, '['.date('d-m-Y H:i:s',time()).'] ['.$this->_execution_id.'] ['.$_SERVER['REMOTE_ADDR'].'] ['.strtoupper($level).'] '.$message."\n");
    }
    public function close() {
        fclose($this->_stream);
    }
}
