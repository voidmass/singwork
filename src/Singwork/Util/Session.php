<?php 
namespace Singwork\Util;

use Singwork\Util\Util;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Session
 *
 * @author Matej Smisek
 */
class Session {

    /**
     *
     * @var string
     */
    protected $_name;

    public function __construct($name, $autostart = true) {
        $this->_name = $this->prepareSessionName($name);
        if ($autostart === true) {
            $this->initialize();
        }
    }

    private function prepareSessionName($name) {
        return str_replace(['.', '/', '-', '_', ',', '\\', ':'], '', $name);
    }
    protected function getLifetime() {
        if (core()->getConfig()['session'] != '') {            
            return Util::decodeTime(core()->getConfig()['session']['lifetime']);
        } else {
            return 1*24*60*60;            
        }
    }
    public function initialize() {
        $secure = FALSE;
        $httponly = true;
        if (ini_set('session.use_only_cookies', 1) === FALSE) {
            throw new \Exception("Invalid Cookie setting", 500, null);
        }
        $lifetime = $this->getLifetime();
        ini_set('session.gc_maxlifetime', $lifetime);
        $param = session_get_cookie_params();       
        session_set_cookie_params($lifetime, $param["path"], $param["domain"], $secure, $httponly);

        session_name($this->_name);
        session_start();            // Start the PHP session 
        $_SESSION['website_name'] = core()->getConfig()['website']['name'];
        //session_regenerate_id();
    }

    public function destroy() {
        $_SESSION = array();
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - (10*24*60*60), $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
        session_destroy();
    }

    public function close() {
        session_commit();
    }

}

?>
