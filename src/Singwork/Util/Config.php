<?php 
namespace Singwork\Util;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Config
 *
 * @author Matej Smisek
 */
class Config {
    
    private $_config;
    
    private $_default;
    
    public function __construct($website, $framework) {
        $this->_config  = parse_ini_file($website."/_config/configuration.ini", true);
        $this->_default = parse_ini_file($framework."/_config/default.ini", true);
    }

}

?>
