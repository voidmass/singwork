<?php

namespace Singwork\Util;

use Singwork\Exception\QueryException;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Util
 *
 * @author Matej Smisek
 */
class Util
{

    public static $CRAWLERS = [
        'Google' => 'Google',
        'MSN' => 'msnbot',
        'Rambler' => 'Rambler',
        'Yahoo' => 'Yahoo',
        'AbachoBOT' => 'AbachoBOT',
        'accoona' => 'Accoona',
        'AcoiRobot' => 'AcoiRobot',
        'ASPSeek' => 'ASPSeek',
        'CrocCrawler' => 'CrocCrawler',
        'Dumbot' => 'Dumbot',
        'FAST-WebCrawler' => 'FAST-WebCrawler',
        'GeonaBot' => 'GeonaBot',
        'Gigabot' => 'Gigabot',
        'Lycos spider' => 'Lycos',
        'MSRBOT' => 'MSRBOT',
        'Altavista robot' => 'Scooter',
        'AltaVista robot' => 'Altavista',
        'ID-Search Bot' => 'IDBot',
        'eStyle Bot' => 'eStyle',
        'Scrubby robot' => 'Scrubby',
    ];

    public static function escapeColumn($column)
    {
        $column = trim($column);
        self::checkColumn($column);
        return '`' . $column . '`';
    }

    public static function eC($column)
    {
        return self::escapeColumn($column);
    }

    public static function checkColumn($column)
    {
        if ($column == null) {
            throw new \Exception("Bad expression - Empty column", 300, null);
        }
        if (!preg_match('/^[a-zA-Z_][a-zA-Z0-9_]*$/', $column)) {
            throw new \Exception("Bad expression - Wrong column name", 300, null);
        }
        return $column;
    }

    protected static function IS_NUMERIC($value)
    {
        if (!is_numeric($value)) {
            return false;
        }
        if (substr($value, 0, 1) == "0" || substr($value, 0, 1) == "+" || substr($value, 0, 1) == "-") {
            return false;
        }
        return true;
    }

    public static function escapeValue($value, $empty_check = false)
    {
        if (empty(trim($value)) && $empty_check === true) {
            throw new QueryException('Variable empty');
        }
        $value = core()->getConnection()->getDb()->real_escape_string(trim($value));
        if (!self::IS_NUMERIC($value)) {
            if ($value == null) {
                $value = 'NULL';
            } else {
                $value = '"' . $value . '"';
            }
        }
        return $value;
    }

    /**
     * Alias for Util::escapeValue
     * @param type $value
     * @param type $empty_check
     * @return String
     * @see escapeValue()
     */
    public static function eV($value, $empty_check = false)
    {
        return self::escapeValue($value, $empty_check);
    }

    public static function eF($table, $column)
    {
        return self::escapeColumn($table) . '.' . self::escapeColumn($column);
    }

    public static function simpleEscape($value, $empty_check = false)
    {
        if (empty(trim($value)) && $empty_check === true) {
            throw new QueryException('Variable empty');
        }
        return core()->getConnection()->getDb()->real_escape_string(trim($value));
    }

    public static function formatExpression($column, $value, $use_like = false, $table = null)
    {
        $value = Util::escapeValue($value);
        if ($value === null || $value == "NULL") {
            if ($use_like) {
                $value = ' IS NULL';
            } else {
                $value = ' = NULL';
            }
        } else {
            if (is_numeric($value) || !$use_like) {
                $value = ' = ' . $value;
            } else {
                $value = ' LIKE ' . $value;
            }
        }
        if ($table == null) {
            $table = "";
        } else {
            $table = Util::escapeColumn($table) . '.';
        }
        return $table . Util::escapeColumn($column) . $value;
    }

    public static function injectDate($source, $date)
    {
        $m = array();
        if (preg_match_all('/{date:(.*)}/', $source, $m, PREG_SET_ORDER)) {
            foreach ($m as $val) {
                $source = str_replace('{date:' . $val[1] . '}', date($val[1], $date), $source);
            }
        }
        return $source;
    }

    public static function removeSpecChars($text)
    {
        $conv = Array(
            'ä' => 'a',
            'Ä' => 'A',
            'á' => 'a',
            'Á' => 'A',
            'à' => 'a',
            'À' => 'A',
            'ã' => 'a',
            'Ã' => 'A',
            'â' => 'a',
            'Â' => 'A',
            'č' => 'c',
            'Č' => 'C',
            'ć' => 'c',
            'Ć' => 'C',
            'ď' => 'd',
            'Ď' => 'D',
            'ě' => 'e',
            'Ě' => 'E',
            'é' => 'e',
            'É' => 'E',
            'ë' => 'e',
            'Ë' => 'E',
            'è' => 'e',
            'È' => 'E',
            'ê' => 'e',
            'Ê' => 'E',
            'í' => 'i',
            'Í' => 'I',
            'ï' => 'i',
            'Ï' => 'I',
            'ì' => 'i',
            'Ì' => 'I',
            'î' => 'i',
            'Î' => 'I',
            'ľ' => 'l',
            'Ľ' => 'L',
            'ĺ' => 'l',
            'Ĺ' => 'L',
            'ń' => 'n',
            'Ń' => 'N',
            'ň' => 'n',
            'Ň' => 'N',
            'ñ' => 'n',
            'Ñ' => 'N',
            'ó' => 'o',
            'Ó' => 'O',
            'ö' => 'o',
            'Ö' => 'O',
            'ô' => 'o',
            'Ô' => 'O',
            'ò' => 'o',
            'Ò' => 'O',
            'õ' => 'o',
            'Õ' => 'O',
            'ő' => 'o',
            'Ő' => 'O',
            'ř' => 'r',
            'Ř' => 'R',
            'ŕ' => 'r',
            'Ŕ' => 'R',
            'š' => 's',
            'Š' => 'S',
            'ś' => 's',
            'Ś' => 'S',
            'ť' => 't',
            'Ť' => 'T',
            'ú' => 'u',
            'Ú' => 'U',
            'ů' => 'u',
            'Ů' => 'U',
            'ü' => 'u',
            'Ü' => 'U',
            'ù' => 'u',
            'Ù' => 'U',
            'ũ' => 'u',
            'Ũ' => 'U',
            'û' => 'u',
            'Û' => 'U',
            'ý' => 'y',
            'Ý' => 'Y',
            'ž' => 'z',
            'Ž' => 'Z',
            'ź' => 'z',
            'Ź' => 'Z',
            ',' => ' ',
            '.' => ' ',
            ':' => ' ',
            ';' => ' '
        );

        return strtr($text, $conv);
    }

    public static function redirect($url, $code = 302)
    {
        if ($code == null) {
            $code = 302;
        }
        if (substr($url, -1) == '/') {
            $url = 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://' . core()->getConfig()['website']['url'] . $url;
        }
        header('Location: ' . $url, true, $code);
        die();
    }

    public static function getSprintfFormat($format)
    {
        $f = ['bold' => "\033[1m%s\033[0m",
            'dark' => "\033[2m%s\033[0m",
            'italic' => "\033[3m%s\033[0m",
            'underline' => "\033[4m%s\033[0m",
            'blink' => "\033[5m%s\033[0m",
            'reverse' => "\033[7m%s\033[0m",
            'concealed' => "\033[8m%s\033[0m",
            // foreground colors
            'black' => "\033[30m%s\033[0m",
            'red' => "\033[31m%s\033[0m",
            'green' => "\033[32m%s\033[0m",
            'yellow' => "\033[33m%s\033[0m",
            'blue' => "\033[34m%s\033[0m",
            'magenta' => "\033[35m%s\033[0m",
            'cyan' => "\033[36m%s\033[0m",
            'white' => "\033[37m%s\033[0m",
            // background colors
            'bg_black' => "\033[40m%s\033[0m",
            'bg_red' => "\033[41m%s\033[0m",
            'bg_green' => "\033[42m%s\033[0m",
            'bg_yellow' => "\033[43m%s\033[0m",
            'bg_blue' => "\033[44m%s\033[0m",
            'bg_magenta' => "\033[45m%s\033[0m",
            'bg_cyan' => "\033[46m%s\033[0m",
            'bg_white' => "\033[47m%s\033[0m"
        ];
        return $f[$format];
    }

    public static function decodeTime($time)
    {
        
        preg_match('#(?:(\d+)M)?(?:(\d+)w)?(?:(\d+)d)?(?:(\d+)h)?(?:(\d+)m)?(?:(\d+)s)?#', trim($time), $m);
        
        return ((((((((((intval($m[1]) * 4) + intval($m[2])) * 7) + intval($m[3])) * 24) + intval($m[4])) * 60) + intval($m[5])) * 60) + intval($m[6]));
    }

    private static function walk($arr, $key)
    {
        $ret = array();
        foreach ($arr as $k => $v) {
            if (is_array($v)) {
                $ret[$k] = self::walk($v, $key);
            } else {
                $ret[$k][$key] = $v;
            }
        }
        return $ret;
    }

    public static function reorganizeFiles($ar)
    {
        $arr = array();
        foreach ($ar as $name => $values) {

            // init for array_merge
            if (!isset($arr[$name])) {
                $arr[$name] = array();
            }

            if (!is_array($values['error'])) {
                // normal syntax
                $arr[$name] = $values;
            } else {
                // html array feature
                foreach ($values as $attribute_key => $attribute_values) {
                    $arr[$name] = array_merge_recursive($arr[$name], self::walk($attribute_values, $attribute_key));
                }
            }
        }
        return $arr;
    }

    public static function httpHeader($message, $code = 200)
    {
        header(filter_input(INPUT_SERVER, 'SERVER_PROTOCOL', FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH) . ' ' . $message, true, $code);
    }

    public static function crawlerDetect($USER_AGENT)
    {
        if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider|google/i', $_SERVER['HTTP_USER_AGENT'])) {
            return true;
        } else {
            return false;
        }
    }

    public static function prepareStringForUrl($string)
    {
        return preg_replace('/-$/ui', '', preg_replace('/-{2,}/ui', '-', str_replace(' ', '-', strtolower(self::removeSpecChars($string)))));
    }

    /**
     * Converts date from anything to basic formats
     * Enter ISO for ISO format
     * Enter CZ for czech format
     * Enter CZ-TIME for czech format inlcuding time
     * 
     * @param String $date
     * @param String $format
     */
    public static function convertDate($date, $format = 'ISO')
    {
        if (is_numeric($date)) {
            $timestamp = $date;
        } elseif ($date instanceof \DateTime) {
            $timestamp = $date->getTimestamp();
        } else {
            $timestamp = strtotime(str_replace(' ', '', $date));
            if ($timestamp === false) {
                return 'null';
            }
        }
        switch ($format) {
            case 'ISO':
                return date('Y-m-d', $timestamp);
            case 'ISOFULL':
                return date('Y-m-d H:i:s', $timestamp);
            case 'CZ':
            default:
                return date('d. m. Y', $timestamp);
            case 'CZ-TIME':
                return date('d. m. Y H:i', $timestamp);
            case 'CZ-TIME-SECONDS':
                return date('d. m. Y H:i:s', $timestamp);
            case 'CZ-TIME-SECONDS-SHORT':
                return date('d. m. H:i:s', $timestamp);
            case 'CZ-TIME-ONLY':
                return date('H:i:s', $timestamp);
            case 'CZ-TIME-ONLY-SHORT':
                return date('H:i', $timestamp);
        }
    }

    public static function openJsonFile($path)
    {
        return json_decode(file_get_contents($path), true);
    }

    public static function defaultValue($value, $default)
    {
        if ($value === null) {
            return $default;
        }
        return $value;
    }

    public static function getNeedlesFromHaystack($haystack, $needles)
    {
        if (empty($needles)) {
            return $haystack;
        } else {
            $needle = array_splice($needles, 0, 1)[0];
            return self::getNeedlesFromHaystack($haystack[$needle], $needles);
        }
    }

    public static function humanReadableSize($kilobytes)
    {
        if ($kilobytes >= 1048576) {
            return round($kilobytes / 1024 / 1024, 2) . ' GB';
        } elseif ($kilobytes >= 1024) {
            return round($kilobytes / 1024, 2) . ' MB';
        } else {
            return $kilobytes . ' kB';
        }
    }

    public static function findChangesInArray($old, $new)
    {
        return ['removed' => array_diff($old, $new), 'added' => array_diff($new, $old)];
    }

    public static function searchEntityArrayById($array, $id)
    {
        foreach ($array as $entity) {
            if ($entity->getId() === $id) {
                return $entity;
            }
        }
        return null;
    }

    public static function isValidEmail($string) {
        return (filter_var(trim($string), FILTER_VALIDATE_EMAIL) && preg_match('/@.+\./', trim($string)));
    }
}