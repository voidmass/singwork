<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Singwork\Util;

/**
 * Description of EntityArrayFilter
 *
 * @author matej.smisek
 */
class EntityArrayFilter
{

    protected $_filter;

    public function __construct($filter)
    {
        $this->_filter = $filter;
    }
    /**
     * 
     * @param type $filter
     * @return $this
     */
    public function setFilter($filter)
    {
        $this->_filter = $filter;
        return $this;
    }
    /**
     * 
     * @param type $filter
     * @return $this
     */
    public function prependToFilter($filter)
    {
        $this->_filter = array_merge($filter, $this->_filter);
        return $this;
    }
    /**
     * 
     * @param type $filter
     * @return $this
     */
    public function appendToFilter($filter)
    {
        $this->_filter = array_merge($this->_filter, $filter);
        return $this;
    }

    public function filter(array $data)
    {
        $output = [];
        foreach ($data as $entity) {
            $output[$entity->getId()] = $this->filterRow($entity);
        }
        return $output;
    }

    public function filterRow(\Singwork\Model\Entity $entity)
    {
        $output = [];
        foreach ($this->_filter as $key => $action) {
            if (is_callable($action)) {
                $output[$key] = call_user_func($action, $entity);
            } else {
                $output[$key] = self::getFormattedCell('generic', $entity->getProperty($key));
            }
        }
        return $output;
    }

    public static function getFormattedCell($type, $content, $data = [], $class = [], $variables = [])
    {
        return [
            'type' => $type,
            'content' => $content,
            'data' => $data,
            'class' => $class,
            'variables' => $variables
        ];
    }

    public static function getControlCell()
    {
        return [
            'type' => 'control'
        ];
    }

    public static function getSelectCell()
    {
        return [
            'type' => 'select'
        ];
    }

}
