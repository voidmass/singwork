<?php

namespace Singwork\Util;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Poster  CURLOPT_CUSTOMREQUEST
 *
 * @author Matej Smisek
 */
class Poster
{

    private $_lastCode = 0;
    private $_debug = '';
    protected $_unsafeSsl;

    public function __construct($unsafeSsl = false)
    {
        $this->_unsafeSsl = $unsafeSsl;
    }

    public function sendPost($url, $post = [])
    {
        $curl_connection = curl_init($url);

        curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl_connection, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0");
        curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post);
//        curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, true);
//        $header[] = "Accept-Language: cs-cz,cs;q=0.5";
//        curl_setopt($curl_connection, CURLOPT_HTTPHEADER, $header);
        $return = curl_exec($curl_connection);
        curl_close($curl_connection);
        return $return;
    }

    public function send($method, $url, $data = false, $type = false, $auth = false, $headers = false)
    {
        $curl_connection = curl_init();
        $curl_headers = [];

        switch ($method) {
            case 'GET':
                if ($data !== false) {
//                    var_dump($data);
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
                break;
            case 'POST':
                curl_setopt($curl_connection, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($curl_connection, CURLOPT_POST, true);
                if ($data !== false) {
                    curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $data);
                    $curl_headers[] = 'Content-Length: ' . strlen($data);
                }

                break;
            case 'PUT':
                curl_setopt($curl_connection, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($curl_connection, CURLOPT_PUT, true);
                if ($data !== false) {
                    curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $data);
                    $curl_headers[] = 'Content-Length: ' . strlen($data);
                }
                break;
            case 'PATCH':
                curl_setopt($curl_connection, CURLOPT_CUSTOMREQUEST, 'PATCH');
                if ($data !== false) {
                    curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $data);
                    $curl_headers[] = 'Content-Length: ' . strlen($data);
                }
                break;
            case 'DELETE':
                curl_setopt($curl_connection, CURLOPT_CUSTOMREQUEST, 'DELETE');
                if ($data !== false && !empty($data)) {
                    curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $data);
                    $curl_headers[] = 'Content-Length: ' . strlen($data);
                }
                break;
        }


        if ($type == 'json') {
            $curl_headers[] = 'Content-Type: application/json';
        }

        curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl_connection, CURLOPT_URL, $url);
        curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
//        curl_setopt($curl_connection, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0");
        curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
        if ($this->_unsafeSsl === true) {
            curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
        }
//        $header[] = "Accept-Language: cs-cz,cs;q=0.5";
//        curl_setopt($curl_connection, CURLOPT_HTTPHEADER, $header);
        if ($auth !== false) {
            curl_setopt($curl_connection, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl_connection, CURLOPT_USERPWD, $auth['user'] . ":" . $auth['password']);
        }

        if ($headers !== false) {
            foreach ($headers as $header => $value) {
                $curl_headers[] = $header . ': ' . $value;
            }
        }
        if (!empty($curl_headers)) {
            curl_setopt($curl_connection, CURLOPT_HTTPHEADER, $curl_headers);
        }

//          curl_setopt($curl_connection, CURLOPT_VERBOSE, true);

        curl_setopt($curl_connection, CURLINFO_HEADER_OUT, true);

        $return = curl_exec($curl_connection);
        $this->_lastCode = curl_getinfo($curl_connection, CURLINFO_HTTP_CODE);
        $this->_debug = curl_getinfo($curl_connection);
        if (curl_errno($curl_connection) !== 0) {
            return curl_error($curl_connection);
        }
        curl_close($curl_connection);
        return $return;
    }

    public function getLastCode()
    {
        return $this->_lastCode;
    }

    public function getDebug()
    {
        return $this->_debug;
    }

}
