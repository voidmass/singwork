<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Singwork\Factory;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Singwork\Traits\EntityManagerAwareTrait;
/**
 * Description of Factory
 *
 * @author matej.smisek
 */
class FactoryStore implements ContainerAwareInterface
{

    use ContainerAwareTrait;
    use EntityManagerAwareTrait;

    /**
     *
     * @var array
     */
    protected $factories = [];

    public function get($id)
    {

        return new $this->factories[$id]($this->container);
    }

    public function registerFactory($id, $class)
    {
        $this->factories[$id] = $class;
        return $this;
    }

}
