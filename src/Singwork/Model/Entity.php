<?php

namespace Singwork\Model;

use Doctrine\Common\Collections\ArrayCollection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Model
 *
 * @author matej.smisek
 */
abstract class Entity
{

    public function getPropertyFromDbName($name)
    {
        $fragments = \explode('_', $name);
        $property = '';
        foreach ($fragments as $fragment) {
            $property .= \ucfirst($fragment);
        }
        $property = \lcfirst($property);
        return $this->{$property};
    }

    public function getProperty($name)
    {
        $reflect = new \ReflectionClass($this);
        $method = 'get' . \ucfirst($name);
        if ($reflect->hasMethod($method)) {
            return $this->{$method}();
        }
        return $this->getPropertyFromDbName($name);
    }

    public function hasProperty($name)
    {
        $reflect = new \ReflectionClass($this);
        $method = 'get' . $name;
        return $reflect->hasMethod($method);
    }

}
