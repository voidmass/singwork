<?php

namespace Singwork\Model\Entities;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of UserEmailPermission
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_mail_server_domain")
 */
class MailServerDomain extends Entity
{
    
    /**
     * @Id @Column(type="bigint") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $domain;
    
    /**
     * Many Groups have Many Users.
     * @ManyToMany(targetEntity="MailServer", mappedBy="domains")
     * @var ArrayCollection|MailServer[]
     */
    protected $mailServers;
    
    public function __construct($domain)
    {
        $this->domain = $domain;
        $this->mailServers = new ArrayCollection();
    }

    
    public function getId()
    {
        return $this->id;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function getMailServers()
    {
        return $this->mailServers;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    public function addMailServer(MailServer $mailServer)
    {
        $this->mailServers[] = $mailServer;
        return $this;
    }



}
