<?php

namespace Singwork\Model\Entities\Shop;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_payment_type")
 */
class PaymentType extends Entity
{

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $title;

    /**
     * @Column(type="text")
     * @var string
     */
    protected $description;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $type;
    /**
     * @Column(type="float")
     * @var double
     */
    protected $price = 0;

    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $visible = true;

    /**
     * @Column(type="smallint")
     * @var int
     */
    protected $order = 1;

    /**
     * 
     * @ManyToMany(targetEntity="DeliveryType", mappedBy="paymentTypes")
     * @var ArrayCollection Description
     */
    protected $deliveryTypes;

    public function __construct($title, $description, $type, $price = 0)
    {
        $this->title = $title;
        $this->description = $description;
        $this->type = $type;
        $this->price = $price;
        $this->deliveryTypes = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getType()
    {
        return $this->type;
    }

   

    public function getPrice()
    {
        return $this->price;
    }

    public function getVisible()
    {
        return $this->visible;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getDeliveryTypes()
    {
        return $this->deliveryTypes;
    }

    
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

   

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }

    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    
    public function addDeliveryType(DeliveryType $type)
    {
        
        $this->deliveryTypes[] = $type;
    }

}
