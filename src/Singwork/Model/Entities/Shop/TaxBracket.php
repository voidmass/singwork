<?php

namespace Singwork\Model\Entities\Shop;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_tax_bracket")
 */
class TaxBracket extends Entity
{

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @Column(type="float")
     * @var double
     */
    protected $value;  
    
     /**
     * One Product has Many Features.
     * @OneToMany(targetEntity="Product", mappedBy="taxBracket")
     * @var Product
     */
    protected $products;

    public function __construct($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
        $this->products = new ArrayCollection();
    }


 
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }


    
    

}