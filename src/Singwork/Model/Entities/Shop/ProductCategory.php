<?php

namespace Singwork\Model\Entities\Shop;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_product_categories")
 */
class ProductCategory extends Entity
{ 
    /**
     * 
     * @Id @ManyToOne(targetEntity="Product", inversedBy="productCategories")
     * @JoinColumn(name="product_id", referencedColumnName="id")
     * @var Product
     */
    protected $product;
    /**
     * 
     * @Id @ManyToOne(targetEntity="Category", inversedBy="productCategories")
     * @JoinColumn(name="category_id", referencedColumnName="id")
     * @var Category
     */
    protected $category;
    
    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $mainCategory;
    
    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $visible = true;

    
    public function __construct(Product $product, Category $category, $mainCategory = false, $visible = true)
    {
        $this->product = $product;
        $this->category = $category;
        $this->mainCategory = $mainCategory;
        $this->visible = $visible;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function isMainCategory()
    {
        return $this->mainCategory;
    }

    public function isVisible()
    {
        return $this->visible;
    }

    public function setProduct(Product $product)
    {
        $this->product = $product;
        return $this;
    }

    public function setCategory(Category $category)
    {
        $this->category = $category;
        return $this;
    }

    public function setMainCategory($mainCategory)
    {
        $this->mainCategory = $mainCategory;
        return $this;
    }

    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }


}
