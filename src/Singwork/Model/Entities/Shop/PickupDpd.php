<?php

namespace Singwork\Model\Entities\Shop;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_pickup_dpd")
 */
class PickupDpd extends Entity
{

    /**
     * @Id @Column(type="string")
     * @var string
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $company;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $street;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $city;
    /**
     * @Column(type="string", name="house_number")
     * @var string
     */
    protected $houseNumber;
    /**
     * @Column(type="string", name="zip_code")
     * @var string
     */
    protected $zipCode;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $phone;            
    /**
     * @Column(type="float")
     * @var float
     */
    protected $longitude;
    /**
     * @Column(type="float")
     * @var float
     */
    protected $latitude;

    /**
     * @Column(type="boolean", name="card_allowed")
     * @var boolean
     */
    protected $cardAllowed;

    /**
     * @Column(type="text")
     * @var string
     */
    protected $hours; 
    
    public function __construct($id, $company, $street, $city, $houseNumber, $zipCode, $phone, $cardAllowed, $hours, $longitude, $latitude)
    {
        $this->id = $id;
        $this->company = $company;
        $this->street = $street;
        $this->city = $city;
        $this->houseNumber = $houseNumber;
        $this->zipCode = $zipCode;
        $this->phone = $phone;
        $this->cardAllowed = $cardAllowed;
        $this->hours = $hours;
        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    public function getZipCode()
    {
        return $this->zipCode;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function isCardAllowed()
    {
        return $this->cardAllowed;
    }

    public function getHours()
    {
        return json_decode($this->hours , true);
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;
        return $this;
    }

    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function setCardAllowed($cardAllowed)
    {
        $this->cardAllowed = $cardAllowed;
        return $this;
    }

    public function setHours($hours)
    {
        $this->hours = $hours;
        return $this;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }


}
