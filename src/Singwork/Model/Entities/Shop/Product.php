<?php

namespace Singwork\Model\Entities\Shop;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_product")
 */
class Product extends Entity
{

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $shortName;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $catalog;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $ean;

    /**
     * @Column(type="float")
     * @var double
     */
    protected $weight;
    
    /**
     * @Column(type="float")
     * @var double
     */
    protected $stock;

    /**
     * @Column(type="text")
     * @var string
     */
    protected $description;

    /**
     * @Column(type="text", name="summary")
     * @var string
     */
    protected $summary;

    /**
     * @Column(type="float", name="base_price")
     * @var int
     */
    protected $basePrice;

    /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="TaxBracket", inversedBy="products")
     * @JoinColumn(name="tax_bracket_id", referencedColumnName="id")
     * @var TaxBracket
     */
    protected $taxBracket;

    /**
     * @Column(type="string", nullable=true, name="base_package")
     * @var string
     */
    protected $basePackage;

    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $visible = true;

    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $heureka = false;

    /**
     * @Column(type="boolean", name="remote_shop")
     * @var boolean
     */
    protected $remoteShop = false;

    /**
     * @Column(type="text", nullable=true)
     * @var string
     */
    protected $keywords;

    /**
     * @Column(type="text", nullable=true, name="meta_description")
     * @var string
     */
    protected $metaDescription;

    /**
     * @Column(type="text", nullable=true, name="meta_title")
     * @var string
     */
    protected $metaTitle;

    /**
     * @Column(type="text", name="url_name")
     * @var string
     */
    protected $urlName;

    /**
     * 
     * @OneToMany(targetEntity="ProductCategory", mappedBy="product", cascade={"persist", "remove"})
     * @var ArrayCollection|ProductCategory[]
     */
    protected $productCategories;

    /**
     * 
     * @OneToMany(targetEntity="ProductImage", mappedBy="product", cascade={"persist", "remove"})
     * @var ArrayCollection|ProductImage[]
     */
    protected $images;
    
    /**
     * 
     * @OneToMany(targetEntity="ProductVariant", mappedBy="product", cascade={"persist", "remove"})
     * @var ArrayCollection|ProductVariant[]
     */
    protected $variants;

    /**
     * 
     * @ManyToMany(targetEntity="Feature", inversedBy="products", cascade={"persist", "remove"})
     * @JoinTable(name="singwork_shop_product_feature")
     * @var ArrayCollection
     */
    protected $features;

    /**
     * 
     * @ManyToMany(targetEntity="Promotion", inversedBy="products", cascade={"persist", "remove"})
     * @JoinTable(name="singwork_shop_product_promotion")
     * @var ArrayCollection
     */
    protected $promotions;

    public function __construct($name, $shortName, $catalog, $description, $basePrice, $basePackage)
    {
        $this->name = $name;
        $this->shortName = $shortName;
        $this->catalog = $catalog;
        $this->description = $description;
        $this->basePrice = $basePrice;
        $this->basePackage = $basePackage;
        $this->productCategories = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->features = new ArrayCollection();
        $this->promotions = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCatalog()
    {
        return $this->catalog;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getBasePrice()
    {
        return $this->basePrice;
    }

    public function getBasePackage()
    {
        return $this->basePackage;
    }

    public function getVisible()
    {
        return $this->visible;
    }

    public function getKeywords()
    {
        return $this->keywords;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function getSortedImages()
    {
        return $this->images->matching(Criteria::create()->orderBy(["order" => Criteria::ASC]));
    }
    /**
     * 
     * @return ProductImage
     */
    public function getMainImage()
    {
        $image = $this->getSortedImages()->first();
        if ($image === false) {
            $image = null;
        }
        return $image;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setCatalog($catalog)
    {
        $this->catalog = $catalog;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setBasePrice($basePrice)
    {
        $this->basePrice = $basePrice;
        return $this;
    }

    public function setBasePackage($basePackage)
    {
        $this->basePackage = $basePackage;
        return $this;
    }

    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }

    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }

    public function getShortName()
    {
        return $this->shortName;
    }

    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
        return $this;
    }

    public function addImage(ProductImage $image)
    {
        $this->images[] = $image;
        $image->setProduct($this);
    }

    public function addCategory(Category $category, $isMainCategory = false)
    {
        $productCategory = new ProductCategory($this, $category, $isMainCategory);
        if ($isMainCategory === true) {
            foreach ($this->productCategories as $productCategoryCurrent) {
                $productCategoryCurrent->setMainCategory(false);
            }
        }
        $this->productCategories[] = $productCategory;
        $category->addProductCategory($productCategory);
    }

    public function setMainCategory(Category $category)
    {
        foreach ($this->productCategories as $productCategoryCurrent) {
            $productCategoryCurrent->setMainCategory(false);
        }
        $this->productCategories->matching(Criteria::create()->where(Criteria::expr()->eq("category", $category)))->current()->setMainCategory(true);
        return $this;
    }

    public function getMainCategory(): Category
    {
        return $this->productCategories->matching(Criteria::create()->where(Criteria::expr()->eq("mainCategory", true)))->first()->getCategory();
    }
    /**
     * 
     * @return ArrayCollection|Category[]
     */
    public function getCategories()
    {
        $categories = new ArrayCollection();
        foreach ($this->productCategories as $productCategory) {
            $categories[] = $productCategory->getCategory();
        }
        return $categories;
    }

    public function getFeatures()
    {
        return $this->features;
    }

    public function addFeature(Feature $feature)
    {
        $this->features[] = $feature;
        $feature->addProduct($this);
        return $this;
    }

    public function addPromotion(Promotion $promotion)
    {
        $this->promotions->add($promotion);
    }

    public function getProductCategories()
    {
        return $this->productCategories;
    }

    public function getPromotions()
    {
        return $this->promotions;
    }

    /**
     * 
     * @return Promotion[]
     */
    public function getActivePromotions()
    {
//        return $this->promotions;
//        $now = (new \DateTime('now'))->format('Y-m-d H:i:s');
        return $this->promotions->matching(Criteria::create()->where(Criteria::expr()->eq("active", true)));
//        return $this->promotions->matching(Criteria::create()->where(Criteria::expr()->gte("startd_date", '5')));
//        return $this->promotions->matching(Criteria::create()->where(Criteria::expr()->andX(Criteria::expr()->orX(Criteria::expr()->eq("active", true), Criteria::expr()->gt('startd_date', $now), Criteria::expr()->lt('end_date', $now)))));
    }

    public function getPromotionEffects()
    {
        $now = (new \DateTime('now'))->getTimestamp();
        $effects = [
            Promotion::TYPE_SALE => 0
        ];
        /* @var $promotion Promotion */
        foreach ($this->getActivePromotions() as $promotion) {
            if ($promotion->getStartDate()->getTimestamp() < $now && $now < $promotion->getEndDate()->getTimestamp()) {
                $effects[$promotion->getType()] += $promotion->getAmount();
            }
        }
        return $effects;
    }

    public function getCalculatedPrice()
    {
        return round($this->basePrice * (1 - ($this->getPromotionEffects()[Promotion::TYPE_SALE] / 100)));
    }

    public function getSaleAmount()
    {
        return $this->getPromotionEffects()[Promotion::TYPE_SALE];
    }

    public function getRealPrice()
    {
        return $this->getCalculatedPrice();
    }

    public function isOnSale()
    {
        return $this->basePrice != $this->getCalculatedPrice();
    }

    public function getSummary()
    {
        return $this->summary;
    }

    public function setSummary($summary)
    {
        $this->summary = $summary;
        return $this;
    }

    public function getEan()
    {
        return $this->ean;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getTaxBracket(): TaxBracket
    {
        return $this->taxBracket;
    }

    public function setEan($ean)
    {
        $this->ean = $ean;
        return $this;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    public function setTaxBracket(TaxBracket $taxBracket)
    {
        $this->taxBracket = $taxBracket;
        return $this;
    }

    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }

    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
        return $this;
    }

    public function getUrlName()
    {
        return $this->urlName;
    }

    public function setUrlName($urlName)
    {
        $this->urlName = $urlName;
        return $this;
    }

    public function getHeureka()
    {
        return $this->heureka;
    }

    public function getRemoteShop()
    {
        return $this->remoteShop;
    }

    public function setHeureka($heureka)
    {
        $this->heureka = $heureka;
        return $this;
    }

    public function setRemoteShop($remoteShop)
    {
        $this->remoteShop = $remoteShop;
        return $this;
    }
    
    /**
     * 
     * @return ArrayCollection|ProductVariant[]
     */
    public function getVariants()
    {
        return $this->variants;
    }

    public function addVariant(ProductVariant $variant)
    {
        $this->variants[] = $variant;
        $variant->setProduct($this);
        return $this;
    }

    public function getStock()
    {
        return $this->stock;
    }

    public function setStock($stock)
    {
        $this->stock = $stock;
        return $this;
    }


}
