<?php
namespace Singwork\Model\Entities\Shop;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\Criteria;
/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_product_variant")
 */
class ProductVariant extends Entity
{
    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id; 

    /**
     * 
     * @ManyToOne(targetEntity="Product", inversedBy="variants")
     * @JoinColumn(name="product_id", referencedColumnName="id")
     * @var Product
     */
    protected $product;
    
    /**
     * @Column(type="string")
     * @var string
     */
    protected $catalog;
    
    /**
     * @Column(type="string")
     * @var string
     */
    protected $ean;
    
    /**
     * @Column(type="float", nullable=true)
     * @var double
     */
    protected $weight;
    
    /**
     * @Column(type="float", name="price")
     * @var int
     */
    protected $price;
    
    /**
     * @Column(type="string", nullable=true, name="package")
     * @var string
     */
    protected $package;
    
    /**
     * 
     * @OneToMany(targetEntity="ProductImage", mappedBy="variant", cascade={"persist", "remove"})
     * @var ArrayCollection|ProductImage[]
     */
    protected $images;
    
    /**
     * @Column(type="float")
     * @var double
     */
    protected $stock;
    
    public function __construct($catalog, $price, $package)
    {
        $this->catalog = $catalog;
        $this->price = $price;
        $this->package = $package;
        $this->images = new ArrayCollection();
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getCatalog()
    {
        return $this->catalog;
    }

    public function getEan()
    {
        return $this->ean;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getPackage()
    {
        return $this->package;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setProduct(Product $product)
    {
        $this->product = $product;
        return $this;
    }

    public function setCatalog($catalog)
    {
        $this->catalog = $catalog;
        return $this;
    }

    public function setEan($ean)
    {
        $this->ean = $ean;
        return $this;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function setPackage($package)
    {
        $this->package = $package;
        return $this;
    }

    public function getCalculatedPrice()
    {
        return round($this->price * (1 - ($this->getProduct()->getPromotionEffects()[Promotion::TYPE_SALE] / 100)));
    }
    
    public function getImages()
    {
        return $this->images;
    }

    public function addImage(ProductImage $image)
    {
        $this->images[] = $image;
        $image->setVariant($image);
        return $this;
    }
    public function getSortedImages()
    {
        return $this->images->matching(Criteria::create()->orderBy(["order" => Criteria::ASC]));
    }
    /**
     * 
     * @return ProductImage
     */
    public function getMainImage()
    {
        $image = $this->getSortedImages()->first();
        if ($image === false) {
            $image = null;
        }
        return $image;
    }

    public function getStock()
    {
        return $this->stock;
    }

    public function setStock($stock)
    {
        $this->stock = $stock;
        return $this;
    }


    
    
}
