<?php

namespace Singwork\Model\Entities\Shop;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_promotion")
 */
class Promotion extends Entity
{

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @Column(type="datetime", name="start_date")
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @Column(type="datetime", name="end_date")
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $type;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $amountType;

    /**
     * @Column(type="float", name="amount")
     * @var float
     */
    protected $amount = 0;

    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $active = true;

    /**
     * 
     * @ManyToMany(targetEntity="Product", mappedBy="promotions")
     * @var ArrayCollection
     */
    protected $products;
    /**
     * Many Groups have Many Users.
     * @ManyToMany(targetEntity="Tag", mappedBy="promotions")
     * @var ArrayCollection
     */
    private $tags;
    
    const TYPE_SALE = "sale";
    const TYPE_HIGHLIGHT = "highlight";
    const TYPE_SELLER = "seller";
    const TYPE_SEASON = "season";
    const AMOUNT_PERCENTAGE = "percentage";
    const AMOUNT_ABSOLUTE = "absolute";

    public function __construct($name, \DateTime $startDate, \DateTime $endtDate, $type, $amountType, $amount)
    {
        $this->name = $name;
        $this->startDate = $startDate;
        $this->endtDate = $endtDate;
        $this->type = $type;
        $this->amountType = $amountType;
        $this->amount = $amount;
        $this->products = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function addProduct(Product $product)
    {
        $this->products->add($product);
        $product->addPromotion($this);
    }
    public function addTag(Tag $tag)
    {
        $this->tags->add($tag);
        $tag->addPromotion($this);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getAmountType()
    {
        return $this->amountType;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    public function setEndDate(\DateTime $endtDate)
    {
        $this->endDate = $endtDate;
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function setAmountType($amountType)
    {
        $this->amountType = $amountType;
        return $this;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }
    public function getTags()
    {
        return $this->tags;
    }


}
