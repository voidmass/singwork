<?php

namespace Singwork\Model\Entities\Shop;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_feature")
 */
class Feature extends Entity
{

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $shortName;  

    /**
     * @Column(type="text")
     * @var string
     */
    protected $description;
    /**
     * 
     * @ManyToMany(targetEntity="Product", mappedBy="features")
     * @var ArrayCollection
     */
    protected $products;   
 
    
    
    public function __construct($name, $shortName, $description)
    {
        $this->name = $name;
        $this->shortName = $shortName;
        $this->description = $description;
        $this->products = new ArrayCollection();
    }
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getShortName()
    {
        return $this->shortName;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getProducts(): ArrayCollection
    {
        return $this->products;
    }
    
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function addProduct(Product $product)
    {
        $this->products[] = $product;
        return $this;
    }
    

}