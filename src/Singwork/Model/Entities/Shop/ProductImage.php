<?php
namespace Singwork\Model\Entities\Shop;

use Singwork\Model\Entity;
/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_product_image")
 */
class ProductImage extends Entity
{
    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id; 
    /**
     * @Column(type="string")
     * @var string
     */
    protected $uri;
    
    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $tag;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $thumbnail;
    /**
     * @Column(type="smallint", name="ordering")
     * @var int
     */
    protected $order = 1;    
    /**
     * 
     * @ManyToOne(targetEntity="Product", inversedBy="images")
     * @JoinColumn(name="product_id", referencedColumnName="id", nullable=true)
     * @var Product
     */
    private $product;
    
    /**
     * 
     * @ManyToOne(targetEntity="ProductVariant", inversedBy="images")
     * @JoinColumn(name="variant_id", referencedColumnName="id", nullable=true)
     * @var Product
     */
    private $variant;
    

    public function __construct($uri, $thumbnail, Product $product = null)
    {
        $this->uri = $uri;
        $this->thumbnail = $thumbnail;
        $this->product = $product;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setUri($uri)
    {
        $this->uri = $uri;
        return $this;
    }

    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    public function setProduct(Product $product = null)
    {
        $this->product = $product;
        return $this;
    }

    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }
    public function getTag()
    {
        return $this->tag;
    }

    public function setTag($tag)
    {
        $this->tag = $tag;
        return $this;
    }

    public function getVariant()
    {
        return $this->variant;
    }

    public function setVariant(Product $variant = null)
    {
        $this->variant = $variant;
        return $this;
    }



}
