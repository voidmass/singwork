<?php

namespace Singwork\Model\Entities\Shop;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_tag")
 */
class Tag extends Entity
{

    /**
     * @Id @Column(type="string")
     * @var string
     */
    protected $id;

    /**
     * Many Users have Many Groups.
     * @ManyToMany(targetEntity="Promotion", inversedBy="tags")
     * @JoinTable(name="singwork_shop_promotion_tag")
     * @var ArrayCollection
     */
    protected $promotions;

    public function __construct($id)
    {
        $this->id = $id;
        $this->articles = new ArrayCollection();
    }

    public function addPromotion(Promotion $promotion)
    {
        $this->promotions->add($promotion);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPromotions()
    {
        return $this->promotions;
    }

    public function getActivePromotions()
    {
        $now = (new \DateTime('now'))->getTimestamp();
        $promotions = new ArrayCollection();
        $collection = $this->promotions->matching(Criteria::create()->where(Criteria::expr()->eq("active", true)));
        foreach ($collection as $promotion) {
            if ($promotion->getStartDate()->getTimestamp() < $now && $now < $promotion->getEndDate()->getTimestamp()) {
                $promotions->add($promotion);
            }
        }
        return $promotions;
    }

}
