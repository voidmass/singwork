<?php

namespace Singwork\Model\Entities\Shop;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_product_category")
 */
class Category extends Entity
{

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $shortName;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $url;

    /**
     * @Column(type="text")
     * @var string
     */
    protected $description;
    /**
     * @Column(type="text", name="meta_description")
     * @var string
     */
    protected $metaDescription;

    /**
     * @Column(type="smallint")
     * @var int
     */
    protected $order = 0;

    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $visible = true;
    /**
     * @Column(type="string", name="heureka_name")
     * @var string
     */
    protected $heurekaName;
    /**
     * @Column(type="string", name="public_shop_name")
     * @var string
     */
    protected $publicShopName;
    /**
     * 
     * @OneToMany(targetEntity="ProductCategory", mappedBy="category")
     * @var ArrayCollection
     */
    protected $productCategories;
    
    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $directRoute;

    public function __construct($name, $shortName, $description)
    {
        $this->name = $name;
        $this->shortName = $shortName;
        $this->description = $description;
        $this->productCategories = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getVisible()
    {
        return $this->visible;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }
    public function getShortName()
    {
        return $this->shortName;
    }

    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
        return $this;
    }

    
        
    public function addProductCategory(ProductCategory $productCategory)
    {
        $this->productCategories[] = $productCategory;
    }

    public function addProduct(Product $product, $setAsMainCategory = false)
    {
        $product->addCategory($this, $setAsMainCategory);
    }

    /**
     * 
     * @return ArrayCollection
     */
    public function getProducts($onlyVisible = false)
    {
        $products = new ArrayCollection();
        if ($onlyVisible === true) {
            $productCategories = $this->productCategories->matching(Criteria::create()->where(Criteria::expr()->eq("visible", true)));
        } else {
            $productCategories = $this->productCategories;
        }
        foreach ($productCategories as $productCategory) {
            $products[] = $productCategory->getProduct();
        }
        return $products;
    }
    
    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }
    public function getHeurekaName()
    {
        return $this->heurekaName;
    }

    public function getPublicShopName()
    {
        return $this->publicShopName;
    }

    public function setHeurekaName($heurekaName)
    {
        $this->heurekaName = $heurekaName;
        return $this;
    }

    public function setPublicShopName($publicShopName)
    {
        $this->publicShopName = $publicShopName;
        return $this;
    }
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
        return $this;
    }


    public function getProductCategories(): ArrayCollection
    {
        return $this->productCategories;
    }

    public function getDirectRoute()
    {
        return $this->directRoute;
    }

    public function setDirectRoute($directRoute)
    {
        $this->directRoute = $directRoute;
        return $this;
    }



}
