<?php
namespace Singwork\Model\Entities;

use Singwork\Model\Entity;
/**
 * Description of UserFlag
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_user_flag")
 */
class UserFlag extends Entity
{

    /**
     * @Id @Column(type="bigint") @GeneratedValue
     * @var int
     */
    protected $id;   
    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     * @var User Description
     */
    protected $user;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $flag;    
    /**
     * @Column(type="integer")
     * @var int
     */
    protected $polarity;
            
    
    public function __construct(User $user, $flag, $polatiry)
    {
        $this->user = $user;
        $this->flag = $flag;
        $this->polarity = $polatiry;
    }

    public function getId()
    {
        return $this->id;
    }


    public function getUser(): User
    {
        return $this->user;
    }

    public function getFlag()
    {
        return $this->flag;
    }

    public function getPolarity()
    {
        return $this->polarity;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function setFlag($flag)
    {
        $this->flag = $flag;
        return $this;
    }

    public function setPolarity($polarity)
    {
        $this->polarity = $polarity;
        return $this;
    }



}
