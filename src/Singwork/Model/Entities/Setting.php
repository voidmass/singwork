<?php
namespace Singwork\Model\Entities;
use Singwork\Model\Entity;
/**
 * Description of ApiKey
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_setting")
 */
class Setting extends Entity
{

    /**
     * @Id @Column(type="string")
     * @var string
     */
    protected $id;
    /**
     * @Column(type="text")
     * @var string
     */
    protected $value; 
   
    public function __construct($id, $value)
    {
        $this->id = $id;
        $this->value = $value;
    }
    public function getId()
    {
        return $this->id;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }





}
