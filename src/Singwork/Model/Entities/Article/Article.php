<?php

namespace Singwork\Model\Entities\Article;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_artciles_article")
 */
class Article extends Entity
{

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $title;

    /**
     * @Column(type="text", nullable=true)
     * @var string
     */
    protected $body;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $slug;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $author;

    /**
     * @Column(type="datetime", name="published_date", nullable=true)
     * @var \DateTime
     */
    protected $publishedDate;

    /**
     * @Column(type="datetime", name="created_date")
     * @var \DateTime
     */
    protected $createdDate;

    /**
     * Many Groups have Many Users.
     * @ManyToMany(targetEntity="Tag", mappedBy="articles")
     * @var ArrayCollection
     */
    private $tags;
    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $visible = false;

    public function __construct($title, $body, User $author)
    {
        $this->title = $title;
        $this->body = $body;
        $this->author = $author;
        $this->createdDate = new \DateTime('now');
    }

    public function addTag(Tag $tag)
    {
        $this->tags->add($tag);
        $tag->addArticle($this);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function getPublishedDate()
    {
        return $this->publishedDate;
    }

    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    public function setPublishedDate($publishedDate)
    {
        $this->publishedDate = $publishedDate;
        return $this;
    }

    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }
    public function getVisible()
    {
        return $this->visible;
    }

    public function setVisible($visible)
    {
        $this->visible = $visible;
        return $this;
    }


}
