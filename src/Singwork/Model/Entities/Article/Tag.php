<?php
namespace Singwork\Model\Entities\Article;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_articles_tag")
 */
class Tag extends Entity
{
    /**
     * @Id @Column(type="string")
     * @var string
     */
    protected $id;
    /**
     * Many Users have Many Groups.
     * @ManyToMany(targetEntity="Article", inversedBy="tags")
     * @JoinTable(name="singwork_articles_article_tag")
     * @var ArrayCollection
     */
    protected $articles;
    
    
    public function __construct($id)
    {
        $this->id = $id;
        $this->articles = new ArrayCollection();
        
    }
    
    public function addArticle(Article $article)
    {
        $this->articles->add($article);
        return $this;
    }
    public function getId()
    {
        return $this->id;
    }

    public function getArticles()
    {
        return $this->articles;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setArticles($articles)
    {
        $this->articles = $articles;
        return $this;
    }


}
