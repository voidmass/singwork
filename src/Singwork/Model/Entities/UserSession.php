<?php
namespace Singwork\Model\Entities;
/**
 * Description of UserSession
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_user_session")
 */
class UserSession
{

    /**
     * @Id @Column(type="string", name="user_id")
     * @var string
     */
    protected $userId;
    /**
     * @Id @Column(type="string", name="session_id")
     * @var string
     */
    protected $sessionId;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $creation;
    /**
     * @Column(type="datetime", name="last_update")
     * @var \DateTime
     */
    protected $lastUpdate;    
    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $online;
    /**
     * @Column(type="string", name="auth_key")
     * @var string
     */
    protected $authKey; 
    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $agent; 
    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $ip; 
            
 
    public function __construct($userId, $sessionId, \DateTime $creation, \DateTime $lastUpdate, $online, $authKey, $agent, $ip)
    {
        $this->userId = $userId;
        $this->sessionId = $sessionId;
        $this->creation = $creation;
        $this->lastUpdate = $lastUpdate;
        $this->online = $online;
        $this->authKey = $authKey;
        $this->agent = $agent;
        $this->ip = $ip;
    }

    
    public function getUserId()
    {
        return $this->userId;
    }

    public function getSessionId()
    {
        return $this->sessionId;
    }

    public function getCreation(): \DateTime
    {
        return $this->creation;
    }

    public function getLastUpdate(): \DateTime
    {
        return $this->lastUpdate;
    }

    public function getOnline()
    {
        return $this->online;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function getAgent()
    {
        return $this->agent;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    public function setCreation(\DateTime $creation)
    {
        $this->creation = $creation;
        return $this;
    }

    public function setLastUpdate(\DateTime $lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;
        return $this;
    }

    public function setOnline($online)
    {
        $this->online = $online;
        return $this;
    }

    public function setAuthKey($authKey)
    {
        $this->authKey = $authKey;
        return $this;
    }

    public function setAgent($agent)
    {
        $this->agent = $agent;
        return $this;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }


}
