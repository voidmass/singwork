<?php

namespace Singwork\Model\Entities;

use Singwork\Model\Entity;

/**
 * Description of UserEmailPermission
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_user_email_permission")
 */
class UserEmailPermission extends Entity
{

    /**
     * @Id @Column(type="string")
     * @var string
     */
    protected $email;

    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $permission;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $modified;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $token;
    
    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $verified = false;

    public function __construct($email, $permission, \DateTime $modified, $token)
    {
        $this->email = $email;
        $this->permission = $permission;
        $this->modified = $modified;
        $this->token = $token;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPermission()
    {
        return $this->permission;
    }

    public function isAllowed()
    {
        return $this->permission;
    }

    public function getModified(): \DateTime
    {
        return $this->modified;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setPermission($permission)
    {
        $this->permission = $permission;
        return $this;
    }

    public function setModified(\DateTime $modified)
    {
        $this->modified = $modified;
        return $this;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }
    public function isVerified()
    {
        return $this->verified;
    }

    public function setVerified($verified)
    {
        $this->verified = $verified;
        return $this;
    }


}
