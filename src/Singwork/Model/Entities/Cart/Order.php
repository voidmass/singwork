<?php

namespace Singwork\Model\Entities\Cart;
use Doctrine\Common\Collections\ArrayCollection;
use Singwork\Model\Entity;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_order")
 */
class Order extends Entity
{

    /**
     * @Id @Column(type="string") 
     * @var string
     */
    protected $id;

    /**
     * @Column(type="text", nullable=true) 
     * @var string
     */
    protected $state;

    /**
     * @Column(type="float", name="products_price") 
     * @var double
     */
    protected $productsPrice;

    /**
     * @Column(type="float", name="shipping_price") 
     * @var double
     */
    protected $shippingPrice;

    /**
     * @OneToMany(targetEntity="OrderComment", mappedBy="order", cascade={"persist", "remove"})
     * @var ArrayCollection
     */
    protected $comments;

    /**
     * @OneToOne(targetEntity="Cart", inversedBy="order", cascade={"persist", "remove"})
     * @var Cart
     */
    protected $cart;

    /**
     * @Column(type="datetime", name="last_reminder", nullable=true) 
     * @var DateTime
     */
    protected $lastReminder;

    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $gibonScheduled = false;
    
    /**
     * @Column(type="string", nullable=true, name="gibon_status")
     * @var string
     */
    protected $gibonStatus;
    
    /**
     * @Column(type="string", nullable=true, name="invoice_id")
     * @var string
     */
    protected $invoiceId;
    
    /**
     * @Column(type="string", nullable=true, name="document_type")
     * @var string
     */
    protected $documentType;
    
    
    const PLACED = 'placed';
    const PROCESSED = 'processed';
    const PAYMENT_PENDING = 'payment_pending';
    const SENT = 'sent';
    const PICKUP = 'pickup';
    const COMPLETED = 'completed';
    const CANCELED = 'canceled';
    const EXPIRED = 'expired';

    public function __construct($id, $productsPrice, $shippingPrice, Cart $cart)
    {
        $this->id = $id;
        $this->state = self::PLACED;
        $this->productsPrice = $productsPrice;
        $this->shippingPrice = $shippingPrice;
        $this->cart = $cart;
        $this->lastReminder = new \DateTime('now');
        $this->comments = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getProductsPrice()
    {
        return $this->productsPrice;
    }

    public function getShippingPrice()
    {
        return $this->shippingPrice;
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    public function setProductsPrice($productsPrice)
    {
        $this->productsPrice = $productsPrice;
        return $this;
    }

    public function setShippingPrice($shippingPrice)
    {
        $this->shippingPrice = $shippingPrice;
        return $this;
    }

    public function setCart(Cart $cart)
    {
        $this->cart = $cart;
        return $this;
    }

    public function getLastReminder(): DateTime
    {
        return $this->lastReminder;
    }

    public function setLastReminder(DateTime $lastReminder)
    {
        $this->lastReminder = $lastReminder;
        return $this;
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function addComment(OrderComment $comment)
    {
        $this->comments[] = $comment;
        return $this;
    }
    
    public function isGibonScheduled()
    {
        return $this->gibonScheduled;
    }

    public function getGibonStatus()
    {
        return $this->gibonStatus;
    }

    public function setGibonScheduled($gibonScheduled)
    {
        $this->gibonScheduled = $gibonScheduled;
        return $this;
    }

    public function setGibonStatus($gibonStatus)
    {
        $this->gibonStatus = $gibonStatus;
        return $this;
    }

    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;
        return $this;
    }

    public function getDocumentType()
    {
        return $this->documentType;
    }

    public function setDocumentType($documentType)
    {
        $this->documentType = $documentType;
        return $this;
    }


}
