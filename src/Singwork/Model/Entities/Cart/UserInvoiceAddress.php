<?php

namespace Singwork\Model\Entities\Cart;

use Singwork\Model\Entity;
use Singwork\Model\Entities\User;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_user_invoice_address")
 */
class UserInvoiceAddress extends Entity
{

    /**
     * @Id @Column(type="integer") @GeneratedValue 
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string") 
     * @var string
     */
    protected $street;

    /**
     * @Column(type="string") 
     * @var string
     */
    protected $city;

    /**
     * @Column(type="string") 
     * @var string
     */
    protected $zipcode;

    /**
     * @Column(type="string") 
     * @var string
     */
    protected $country;

    /**
     * @OneToOne(targetEntity="Singwork\Model\Entities\User", inversedBy="invoiceAddress")
     * @var User
     */
    protected $user;

    public function __construct($street, $city, $zipcode, $country, User $user)
    {
        $this->street = $street;
        $this->city = $city;
        $this->zipcode = $zipcode;
        $this->country = $country;
        $this->user = $user;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getZipcode()
    {
        return $this->zipcode;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function setAddress($street, $city, $zipcode, $country)
    {
        $this->setStreet($street)->setCity($city)->setZipcode($zipcode)->setCountry($country);
        return $this;
    }

    public function __clone()
    {
        if ($this->id) {
            $this->setId(null);
        }
    }

}
