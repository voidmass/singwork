<?php

namespace Singwork\Model\Entities\Cart;

use Singwork\Model\Entity;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_cart_address_invoice")
 */
class CartInvoiceAddress extends Entity
{

    /**
     * @Id @Column(type="integer") @GeneratedValue 
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string") 
     * @var string
     */
    protected $street;

    /**
     * @Column(type="string") 
     * @var string
     */
    protected $city;

    /**
     * @Column(type="string") 
     * @var string
     */
    protected $zipcode;
    
    /**
     * @Column(type="string") 
     * @var string
     */
    protected $country;

    /**
     * @OneToOne(targetEntity="Cart", inversedBy="invoiceAddress")
     * @var Cart
     */
    protected $cart;

    public function __construct($street, $city, $zipcode, $country, Cart $cart)
    {
        $this->street = $street;
        $this->city = $city;
        $this->zipcode = $zipcode;
        $this->country = $country;
        $this->cart = $cart;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getZipcode()
    {
        return $this->zipcode;
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
        return $this;
    }

    public function setCart(Cart $cart)
    {
        $this->cart = $cart;
        return $this;
    }
    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }


}
