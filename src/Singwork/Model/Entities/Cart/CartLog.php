<?php

namespace Singwork\Model\Entities\Cart;

use Singwork\Model\Entity;
use Singwork\Model\Entities\Cart\Cart;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_cart_log")
 */
class CartLog extends Entity
{

    /**
     * @Id @Column(type="integer") @GeneratedValue 
     * @var int
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="Cart", inversedBy="logs")
     * @var Cart
     */
    protected $cart;

    /**
     * @Column(type="string") 
     * @var string
     */
    protected $action;
    
  

    /**
     * @Column(type="text", nullable=true, name="diff") 
     * @var string
     */
    protected $diff;

    /**
     * @Column(type="datetime") 
     * @var DateTime
     */
    protected $time;
        
    
    public function __construct(Cart $cart, $action, $diff, \DateTime $time)
    {
        $this->cart = $cart;
        $this->action = $action;
        $this->diff = $diff;
        $this->time = $time;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getTime(): \DateTime
    {
        return $this->time;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setCart(Cart $cart)
    {
        $this->cart = $cart;
        return $this;
    }

    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    public function setTime(\DateTime $time)
    {
        $this->time = $time;
        return $this;
    }

    public function getDiff()
    {
        return $this->diff;
    }

    public function setDiff($diff)
    {
        $this->diff = $diff;
        return $this;
    }
    


}
