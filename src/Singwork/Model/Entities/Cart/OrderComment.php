<?php

namespace Singwork\Model\Entities\Cart;

use Singwork\Model\Entity;
use Singwork\Model\Entities\User;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_order_comment")
 */
class OrderComment extends Entity
{

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string", name="author_id")
     * @var string
     */
    protected $authorId;
    /**
     * @Column(type="string", name="author_name")
     * @var string
     */
    protected $authorName;
    /**
     * @ManyToOne(targetEntity="Order", inversedBy="comments")
     * @var Order
     */
    protected $order;

    /**
     * @Column(type="datetime", nullable=true, name="datetime") 
     * @var DateTime
     */
    protected $datetime;

    /**
     * @Column(type="text", nullable=true) 
     * @var string
     */
    protected $text;

    
    public function __construct(User $author, Order $order, $text)
    {
        $this->order = $order;
        $this->text = $text;
        $this->datetime = new \DateTime('now');
        $this->authorId = $author->getId();
        $this->authorName = $author->getName().' '.$author->getSurname();
    }
    public function getId()
    {
        return $this->id;
    }

    public function getAuthor(): \Singwork\Model\Entities\User
    {
        return $this->author;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getDatetime()
    {
        return $this->datetime;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
        return $this;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }
    public function getAuthorId()
    {
        return $this->authorId;
    }

    public function getAuthorName()
    {
        return $this->authorName;
    }

    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
        return $this;
    }

    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;
        return $this;
    }



}
