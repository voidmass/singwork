<?php

namespace Singwork\Model\Entities\Cart;

use Singwork\Model\Entity;
use Singwork\Model\Entities\Shop\Product;
/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_shop_cart_product")
 */
class CartProduct extends Entity
{

    /**
     * @Id @Column(type="integer") @GeneratedValue 
     * @var int
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="Cart", inversedBy="products")
     * @var Cart
     */
    protected $cart;

    /**
     * @ManyToOne(targetEntity="Singwork\Model\Entities\Shop\Product")
     * @var \Singwork\Model\Entities\Shop\Product
     */
    protected $product;
    
    /**
     * @ManyToOne(targetEntity="Singwork\Model\Entities\Shop\ProductVariant")
     * @JoinColumn(name="product_variant_id", referencedColumnName="id", nullable=true)
     * @var \Singwork\Model\Entities\Shop\ProductVariant
     */
    protected $productVariant;
    
    /**
     * @Column(type="float") 
     * @var double
     */
    protected $price;

    /**
     * @Column(type="float", nullable=true) 
     * @var double
     */
    protected $discount;

    /**
     * @Column(type="integer") 
     * @var int
     */
    protected $amount;

    /**
     * @Column(type="datetime", name="time_added") 
     * @var DateTime
     */
    protected $timeAdded;

    public function __construct(Product $product, $price, $discount, $amount, Cart $cart = null)
    {
        $this->cart = $cart;
        $this->product = $product;
        $this->price = $price;
        $this->discount = $discount;
        $this->amount = $amount;
        $this->timeAdded = new \DateTime('now');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getTimeAdded(): DateTime
    {
        return $this->timeAdded;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setCart(Cart $cart = null)
    {
        $this->cart = $cart;
        return $this;
    }

    public function setProduct(Product $product)
    {
        $this->product = $product;
        return $this;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    public function setTimeAdded(DateTime $timeAdded)
    {
        $this->timeAdded = $timeAdded;
        return $this;
    }

    public function getTotalPrice()
    {
        /*if ($this->productVariant !== null) {
            return round($this->productVariant->getPrice() * $this->amount);
        }*/
        return round($this->price * $this->amount);
    }
    /**
     * 
     * @return ProductVariant
     */
    public function getProductVariant()
    {
        return $this->productVariant;
    }

    public function setProductVariant(\Singwork\Model\Entities\Shop\ProductVariant $productVariant = null)
    {
        $this->productVariant = $productVariant;
        return $this;
    }

    public function getProductCatalog() {
        if ($this->productVariant) {
            return $this->productVariant->getCatalog();
        } else {
            return $this->product->getCatalog();
        }
    }
    public function flatten()
    {
        return [
            'name' => $this->getProduct()->getName(),
            'catalog_number' => $this->productVariant === null ? $this->product->getCatalog() : $this->productVariant->getCatalog(),
            'category' => [
                'name' => $this->getProduct()->getMainCategory()->getName(),
                'short_name' => $this->getProduct()->getMainCategory()->getShortName()
            ],
            'unit_price' => $this->productVariant === null ? $this->product->getBasePrice() : $this->productVariant->getPrice(),
            'amount' => $this->getAmount(),
            'total_price' => $this->getTotalPrice(),
            'image' => $this->getProduct()->getMainImage()->getUri()
        ];
    }

    public function __clone()
    {
        if ($this->id) {
            $this->setId(null);
        }
    }

}
