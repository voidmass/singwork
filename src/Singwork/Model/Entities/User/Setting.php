<?php

namespace Singwork\Model\Entities\User;

use Singwork\Model\Entity;
use Singwork\Model\Entities\User;

/**
 * Description of ApiKey
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_user_setting")
 */
class Setting extends Entity
{

    /**
     * @Id @Column(type="string")
     * @var string
     */
    protected $id;
    
     /**
     * @Id
     * @ManyToOne(targetEntity="Singwork\Model\Entities\User", inversedBy="settings")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     * @var User
     */
    protected $user;
    
    /**
     * @Column(type="text")
     * @var string
     */
    protected $value;

    

    public function __construct($id, User $user, $value)
    {
        $this->id = $id;
        $this->user = $user;
        $this->value = $value;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }



   

 
   

}
