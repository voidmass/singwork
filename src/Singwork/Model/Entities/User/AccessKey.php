<?php

namespace Singwork\Model\Entities\User;

use Singwork\Model\Entity;
use Singwork\Model\Entities\User;

/**
 * Description of ApiKey
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_user_access_key")
 */
class AccessKey extends Entity
{

    /**
     * @Id @Column(type="string")
     * @var string
     */
    protected $id;

    /**
     * @Column(type="text")
     * @var string
     */
    protected $token;

    /**
     * @Column(type="text")
     * @var string
     */
    protected $refresh;

    /**
     * @Id @ManyToOne(targetEntity="Singwork\Model\Entities\User", inversedBy="accessKeys")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     * @var User
     */
    protected $user;

    /**
     * @Column(type="datetime", name="valid_until", nullable=true)
     * @var \DateTime
     */
    protected $validUntil;

    public function __construct($id, $token)
    {
        $this->id = $id;
        $this->token = $token;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getValidUntil(): \DateTime
    {
        return $this->validUntil;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    
    public function setValidUntil(\DateTime $validUntil)
    {
        $this->validUntil = $validUntil;
        return $this;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getRefresh()
    {
        return $this->refresh;
    }

    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    public function setRefresh($refresh)
    {
        $this->refresh = $refresh;
        return $this;
    }

}
