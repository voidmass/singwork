<?php
namespace Singwork\Model\Entities;
/**
 * Description of UserPasswordReset
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_user_password_reset")
 */
class UserPasswordReset
{

    /**
     * @Id @Column(type="bigint") @GeneratedValue
     * @var int
     */
    protected $id;   
    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     * @var User Description
     */
    protected $user;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $token;    
    /**
     * @Column(type="datetime", name="date")
     * @var \DateTime
     */
    protected $date;
            
    
    public function __construct(User $user, $token, \DateTime $date)
    {
        $this->user = $user;
        $this->token = $token;
        $this->date = $date;
    }

    public function getId()
    {
        return $this->id;
    }


    public function getToken()
    {
        return $this->token;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }


    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
        return $this;
    }
    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }




}
