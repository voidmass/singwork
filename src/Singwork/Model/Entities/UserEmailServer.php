<?php

namespace Singwork\Model\Entities;
use Singwork\Model\Entities\MailServer;
/**
 * Description of UserPasswordReset
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_user_email_server")
 */
class UserEmailServer
{

    /**
     * @Id @Column(type="bigint") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @OneToOne(targetEntity="Singwork\Model\Entities\User", mappedBy="emailServer")
     * @var User
     */
    protected $user;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $password;

    /**
     * @ManyToOne(targetEntity="MailServer")
     * @JoinColumn(name="inbound_server_id", referencedColumnName="id")
     * @var MailServer
     */
    protected $inboundServer;
    /**
     * @ManyToOne(targetEntity="MailServer")
     * @JoinColumn(name="outbound_server_id", referencedColumnName="id")
     * @var MailServer
     */
    protected $outboundServer;
    
    /**
     * @Column(type="integer", name="last_message_id")
     * @var int
     */
    protected $lastMessageId;
    
    /**
     * @Column(type="integer", name="last_sent_message_id")
     * @var int
     */
    protected $lastSentMessageId;
    
    public function __construct(User $user, $password, MailServer $inboundServer, MailServer $outboundServer)
    {
        $this->user = $user;
        $this->password = $password;
        $this->inboundServer = $inboundServer;
        $this->outboundServer = $outboundServer;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getPassword()
    {
        return $this->password;
    }
    /**
     * 
     * @return MailServer
     */
    public function getInboundServer(): MailServer
    {
        return $this->inboundServer;
    }
    /**
     * 
     * @return MailServer
     */
    public function getOutboundServer(): MailServer
    {
        return $this->outboundServer;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setInboundServer(MailServer $inboundServer)
    {
        $this->inboundServer = $inboundServer;
        return $this;
    }

    public function setOutboundServer(MailServer $outboundServer)
    {
        $this->outboundServer = $outboundServer;
        return $this;
    }

    public function getLastMessageId()
    {
        return $this->lastMessageId;
    }

    public function setLastMessageId($lastMessageId)
    {
        $this->lastMessageId = $lastMessageId;
        return $this;
    }

    public function getLastSentMessageId()
    {
        return $this->lastSentMessageId;
    }

    public function setLastSentMessageId($lastSentMessageId)
    {
        $this->lastSentMessageId = $lastSentMessageId;
        return $this;
    }


    
    
}
