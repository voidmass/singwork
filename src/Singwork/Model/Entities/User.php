<?php

namespace Singwork\Model\Entities;

use Ramsey\Uuid\Uuid;
use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of User
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_user")
 */
class User extends Entity
{

    /**
     * @Id @Column(type="string") 
     * @var string
     */
    protected $id;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $name;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $surname;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $password;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $type;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $status;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $email;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $phone;

    /**
     * @Column(type="datetime", name="last_login", nullable=true)
     * @var \DateTime
     */
    protected $lastLogin;

    /**
     * @Column(type="datetime", name="register_date")
     * @var \DateTime
     */
    protected $registerDate;

    /**
     * @Column(type="string", name="company_ic", nullable=true)
     * @var string
     */
    protected $companyId;

    /**
     * @Column(type="string", name="company_dic", nullable=true)
     * @var string
     */
    protected $companyTaxId;

    /**
     * @Column(type="string", name="company_name", nullable=true)
     * @var string
     */
    protected $companyName;

    /**
     * @Column(type="datetime", name="modified_email", nullable=true)
     * @var \DateTime
     */
    protected $modifiedEmail;

    /**
     * @Column(type="boolean", name="verified_email", nullable=true)
     * @var boolean
     */
    protected $verifiedEmail;

    /**
     * @Column(type="text", nullable=true)
     * @var string
     */
    protected $avatar;
    
    /**
     * @Column(type="text", nullable=true)
     * @var string
     */
    protected $deliveryNote;
    
    /**
     * @Column(type="text", nullable=true)
     * @var string
     */
    protected $documentType;

    /**
     * @OneToOne(targetEntity="Singwork\Model\Entities\Cart\UserInvoiceAddress", mappedBy="user", cascade={"persist", "remove"})
     * @var Cart\UserInvoiceAddress
     * 
     */
    protected $invoiceAddress;

    /**
     * @OneToMany(targetEntity="Singwork\Model\Entities\UserEmailVerification", mappedBy="user", cascade={"persist", "remove"})
     * @var ArrayCollection
     * 
     */
    protected $emailVerification;

    /**
     * @OneToMany(targetEntity="Singwork\Model\Entities\User\AccessKey", mappedBy="user", cascade={"persist", "remove"}, indexBy="id")
     * @var ArrayCollection
     * 
     */
    protected $accessKeys;
    
    /**
     * @OneToMany(targetEntity="Singwork\Model\Entities\User\Setting", mappedBy="user", cascade={"persist", "remove"}, indexBy="id")
     * @var ArrayCollection
     * 
     */
    protected $settings;

    /**
     * 
     * @OneToMany(targetEntity="UserFlag", mappedBy="user", cascade={"persist", "remove"})
     * @var ArrayCollection
     */
    private $flags;

    /**
     * 
     * @OneToMany(targetEntity="UserPasswordReset", mappedBy="user", cascade={"persist", "remove"})
     * @var ArrayCollection
     */
    private $passwordResets;

    /**
     * 
     * @OneToMany(targetEntity="Singwork\Model\Entities\Cart\Cart", mappedBy="user", cascade={"persist","remove"})
     * @var ArrayCollection
     */
    private $carts;

    /**
     * @OneToOne(targetEntity="Singwork\Model\Entities\UserEmailServer", inversedBy="user", cascade={"persist", "remove"})
     * @JoinColumn(name="server_id", referencedColumnName="id", nullable=true) 
     * @var UserEmailServer
     * 
     */
    protected $emailServer;

    public function __construct($type)
    {
        $this->id = Uuid::uuid4();
        $this->type = $type;
        $this->flags = new ArrayCollection();
        $this->carts = new ArrayCollection();
        $this->passwordResets = new ArrayCollection();
        $this->emailVerification = new ArrayCollection();
        $this->accessKeys = new ArrayCollection();
    }

    public static function createTemporary(\DateTime $lastLogin, \DateTime $registerDate)
    {

        $user = new static(self::TEMPORARY);
        $user->setLastLogin($lastLogin);
        $user->setRegisterDate($registerDate);
        return $user;
    }

    public static function createRegular($username, $password_hash, \DateTime $lastLogin, \DateTime $registerDate)
    {

        $user = new static(self::REGULAR);
        $user->setEmail($username);
        $user->setPassword($password_hash);
        $user->setLastLogin($lastLogin);
        $user->setRegisterDate($registerDate);
        $user->setVerifiedEmail(false);
        $user->setModifiedEmail($registerDate);
        return $user;
    }

    public static function createInvite($username, $hash)
    {

        $user = new static(self::INVITE);
        $user->setEmail($username);
        $user->setPassword($hash);
        $user->setRegisterDate(new \DateTime());
        $user->setVerifiedEmail(false);
        $user->setModifiedEmail(new \DateTime());
        return $user;
    }

    /**
     * 
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 
     * @return String
     */
    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getLastLogin(): \DateTime
    {
        return $this->lastLogin;
    }

    /**
     * 
     * @return \DateTime
     */
    public function getRegisterDate(): \DateTime
    {
        return $this->registerDate;
    }

    public function getCompanyId()
    {
        return $this->companyId;
    }

    public function getCompanyTaxId()
    {
        return $this->companyTaxId;
    }

    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function getVerifiedEmail()
    {
        return $this->verifiedEmail;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function setLastLogin(\DateTime $lastLogin)
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }

    public function setRegisterDate(\DateTime $registerDate)
    {
        $this->registerDate = $registerDate;
        return $this;
    }

    public function setCompanyId($companyIc)
    {
        $this->companyId = $companyIc;
        return $this;
    }

    public function setCompanyTaxId($companyDic)
    {
        $this->companyTaxId = $companyDic;
        return $this;
    }

    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
        return $this;
    }

    public function setVerifiedEmail($verifiedEmail)
    {
        $this->verifiedEmail = $verifiedEmail;
        return $this;
    }

    public function updateTime()
    {
        $this->lastLogin = new \DateTime();
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * 
     * @return ArrayCollection
     */
    public function getFlags()
    {
        return $this->flags;
    }

    /**
     * 
     * @param ArrayCollection $flags
     * @return $this
     */
    public function setFlags($flags)
    {
        $this->flags = $flags;
        return $this;
    }

    public function getInvoiceAddress()
    {
        return $this->invoiceAddress;
    }

    public function setInvoiceAddress(Cart\UserInvoiceAddress $invoiceAddress = null)
    {
        $this->invoiceAddress = $invoiceAddress;
        return $this;
    }

    public function getEmailVerification()
    {
        return $this->emailVerification;
    }

    public function addEmailVerification(UserEmailVerification $userEmailVerification)
    {
        $this->emailVerification[] = $userEmailVerification;
        $userEmailVerification->setUser($this);
        return $this;
    }

    public function getModifiedEmail(): \DateTime
    {
        return $this->modifiedEmail;
    }

    public function setModifiedEmail(\DateTime $modifiedEmail = null)
    {
        $this->modifiedEmail = $modifiedEmail;
        return $this;
    }

    public function setCompany($name, $id, $taxId)
    {
        $this->setCompanyName($name)->setCompanyId($id)->setCompanyTaxId($taxId);
        return $this;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function getPasswordResets()
    {
        return $this->passwordResets;
    }

    public function getCarts()
    {
        return $this->carts;
    }

    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    public function getFullName()
    {
        return $this->name . ' ' . $this->surname;
    }

    public function addFlag($flag, $polarity)
    {
        $this->flags[] = new UserFlag($this, $flag, $polarity);
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getEmailServer()
    {
        return $this->emailServer;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function setEmailServer(UserEmailServer $emailServer = null)
    {
        $this->emailServer = $emailServer;
        return $this;
    }
    /**
     * 
     * @param type $id
     * @return User\AccessKey
     */
    public function getAccessKey($id)
    {
        return $this->accessKeys->get($id);
    }
    public function getAccessKeys()
    {
        return $this->accessKeys;
    }

    public function addAccessKey(User\AccessKey $accessKey)
    {
        $this->accessKeys[] = $accessKey;
        $accessKey->setUser($this);
        return $this;
    }
    /**
     * 
     * @param type $id
     * @return User\Setting
     */
    public function getSetting($id)
    {
        return $this->settings->get($id);
    }
    /**
     * 
     * @return ArrayCollection
     */
    public function getSettings()
    {
        return $this->settings;
    }

    public function addSetting(User\Setting $setting)
    {
        $this->settings[] = $setting;
        $setting->setUser($this);
        return $this;
    }
    public function getDeliveryNote()
    {
        return $this->deliveryNote;
    }

    public function setDeliveryNote($deliveryNote)
    {
        $this->deliveryNote = $deliveryNote;
        return $this;
    }

    public function getDocumentType()
    {
        return $this->documentType;
    }

    public function setDocumentType($documentType)
    {
        $this->documentType = $documentType;
        return $this;
    }

            const REGULAR = "regular";
    const TEMPORARY = "temporary";
    const INVITE = "invite";

}
