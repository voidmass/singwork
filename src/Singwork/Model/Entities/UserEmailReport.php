<?php

namespace Singwork\Model\Entities;

use Singwork\Model\Entity;

/**
 * Description of UserEmailPermission
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_user_email_report")
 */
class UserEmailReport extends Entity
{
    
    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $email;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $source;

    /**
     * @Column(type="datetime")
     * @var \DateTime
     */
    protected $datetime;

    public function __construct($email, $source, \DateTime $datetime)
    {
        $this->email = $email;
        $this->source = $source;
        $this->datetime = $datetime;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getSource()
    {
        return $this->source;
    }

    public function getDatetime(): \DateTime
    {
        return $this->datetime;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    public function setDatetime(\DateTime $datetime)
    {
        $this->datetime = $datetime;
        return $this;
    }



}
