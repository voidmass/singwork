<?php
namespace Singwork\Model\Entities;
use Singwork\Model\Entity;
/**
 * Description of ApiKey
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_api_key")
 */
class ApiKey extends Entity
{

    /**
     * @Id @Column(type="bigint") @GeneratedValue
     * @var int
     */
    protected $id;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $ip; 
    /**
     * @Column(type="string", name="`key`")
     * @var string
     */
    protected $key; 
    /**
     * @Column(type="text", name="`comment`")
     * @var string
     */
    protected $comment; 

    
    public function __construct($ip, $key, $comment = null)
    {
        $this->ip = $ip;
        $this->key = $key;
        $this->comment = $comment;
    }

    
    public function getId()
    {
        return $this->id;
    }
    public function getIp()
    {
        return $this->ip;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }


}
