<?php
namespace Singwork\Model\Entities;
use Singwork\Model\Entity;
/**
 * Description of ApiKey
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_sequence")
 */
class Sequence extends Entity
{

    /**
     * @Id @Column(type="string")
     * @var string
     */
    protected $id;
    /**
     * @Column(type="integer")
     * @var int
     */
    protected $currentId; 
    /**
     * @Column(type="string")
     * @var string
     */
    protected $format; 
    /**
     * @Column(type="string")
     * @var string
     */
    protected $reset; 

    
    public function __construct($id, $format, $reset)
    {
        $this->id = $id;
        $this->format = $format;
        $this->reset = $reset;
        $this->currentId = 0;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCurrentId()
    {
        return $this->currentId;
    }

    public function getFormat()
    {
        return $this->format;
    }

    public function getReset()
    {
        return $this->reset;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setCurrentId($currentId)
    {
        $this->currentId = $currentId;
        return $this;
    }

    public function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }

    public function setReset($reset)
    {
        $this->reset = $reset;
        return $this;
    }




}
