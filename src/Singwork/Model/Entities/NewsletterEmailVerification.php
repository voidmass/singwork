<?php

namespace Singwork\Model\Entities;

/**
 * Description of UserPasswordReset
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_newsletter_email_verification")
 */
class NewsletterEmailVerification
{

    /**
     * @Id @Column(type="bigint") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $email;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $token;

    /**
     * @Column(type="datetime", name="created_date")
     * @var \DateTime
     */
    protected $createdDate;



    public function __construct($email, $token)
    {     
        $this->token = $token;
        $this->email = $email;
        $this->createdDate = new \DateTime('now');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
        return $this;
    }



}
