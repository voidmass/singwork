<?php

namespace Singwork\Model\Entities;

use Singwork\Model\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of UserEmailPermission
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_mail_server")
 */
class MailServer extends Entity
{
    const IMAP = 'IMAP';
    const POP3 = 'POP3';
    const SMTP = 'SMTP';
    
    const SSL = 'SSL';
    const STARTSSL = 'TLS';
    /**
     * @Id @Column(type="bigint") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $type;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $url;
    
    /**
     * @Column(type="integer")
     * @var int
     */
    protected $port;
 
    /**
     * @Column(type="string")
     * @var string
     */
    protected $encryption;
    
    /**
     * 
     * @ManyToMany(targetEntity="MailServerDomain", inversedBy="mailServers")
     * @JoinTable(name="singwork_mail_server_domains")
     * 
     * @var ArrayCollection|MailServerDomain[]
     */
    protected $domains;
    
    public function __construct($type, $url, $port, $encryption)
    {
        $this->type = $type;
        $this->url = $url;
        $this->port = $port;
        $this->encryption = $encryption;
        $this->domains = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function getEncryption()
    {
        return $this->encryption;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    public function setEncryption($encryption)
    {
        $this->encryption = $encryption;
        return $this;
    }
    
    public function getDomains()
    {
        return $this->domains;
    }

    public function addDomain(MailServerDomain $domain)
    {
        $this->domains[] = $domain;
        $domain->addMailServer($this);
        return $this;
    }
    
    public function getServerString() {
        return '{' . $this->url . ':' . $this->port . '/' . strtolower($this->type) . '/' . strtolower($this->encryption) . '}';
    }


}
