<?php

namespace Singwork\Model\Entities;

use Singwork\Model\Entity;

/**
 * Description of ApiKey
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_app_event")
 */
class AppEvent extends Entity
{

    /**
     * @Id @Column(type="bigint") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @Column(type="datetime", name="time")
     * @var \DateTime
     */
    protected $time;

    /**
     * @Column(type="text")
     * @var string
     */
    protected $data;

    /**
     * 
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     * @var User
     */
    protected $user;

    public function __construct($name, $data = '', $user = null)
    {
        $this->name = $name;
        $this->time = new \DateTime('now');
        $this->data = $data;
        $this->user = $user;
    }
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getTime(): \DateTime
    {
        return $this->time;
    }

    public function getData()
    {
        return $this->data;
    }
    /**
     * 
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setTime(\DateTime $time)
    {
        $this->time = $time;
        return $this;
    }

    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }



}
