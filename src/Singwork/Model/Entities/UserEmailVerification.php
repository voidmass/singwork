<?php

namespace Singwork\Model\Entities;

/**
 * Description of UserPasswordReset
 *
 * @author Balgor
 * @Entity 
 * @Table(name="singwork_user_email_verification")
 */
class UserEmailVerification
{

    /**
     * @Id @Column(type="bigint") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="Singwork\Model\Entities\User", inversedBy="emailVerification")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     * @var User
     */
    protected $user;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $token;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $email;

    /**
     * @Column(type="datetime", name="verified_date", nullable=true)
     * @var \DateTime
     */
    protected $verifiedDate;

    /**
     * @Column(type="datetime", name="last_sent", nullable=true)
     * @var \DateTime
     */
    protected $lastSent;

    public function __construct(User $user, $token, $email)
    {
        $this->user = $user;
        $this->token = $token;
        $this->email = $email;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user = null)
    {
        $this->user = $user;
        return $this;
    }

    public function getVerifiedDate()
    {
        return $this->verifiedDate;
    }

    public function setVerifiedDate($verifiedDate)
    {
        $this->verifiedDate = $verifiedDate;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getLastSent()
    {
        return $this->lastSent;
    }

    public function setLastSent($lastSent)
    {
        $this->lastSent = $lastSent;
        return $this;
    }

}
