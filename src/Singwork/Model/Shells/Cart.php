<?php

namespace Singwork\Model\Shells;


/**
 * Description of User
 *
 * @author Balgor
 */
class Cart
{

    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id = 0;

    /**
     * @Column(type="datetime", name="created_time") 
     * @var DateTime
     */
    protected $createdTime;

    /**
     * @Column(type="datetime", nullable=true, name="closed_time") 
     * @var DateTime
     */
    protected $closedTime;

    /**
     * @Column(type="string") 
     * @var string
     */
    protected $state;

    /**
     * @Column(type="string", nullable=true) 
     * @var string
     */
    protected $name;

    /**
     * @Column(type="string", nullable=true) 
     * @var string
     */
    protected $surname;

    /**
     * @Column(type="string", nullable=true) 
     * @var string
     */
    protected $email;

    /**
     * @Column(type="string", nullable=true) 
     * @var string
     */
    protected $phone;

    /**
     * @Column(type="string", nullable=true, name="company_name") 
     * @var string
     */
    protected $companyName;

    /**
     * @Column(type="string", nullable=true, name="company_id") 
     * @var string
     */
    protected $companyId;

    /**
     * @Column(type="string", nullable=true, name="company_tax_id") 
     * @var string
     */
    protected $companyTaxId;

    /**
     * @Column(type="string", nullable=true) 
     * @var string
     */
    protected $note;

    /**
     * @ManyToOne(targetEntity="Singwork\Model\Entities\User", inversedBy="carts", cascade={"persist"})
     * @JoinColumn(name="user_id", referencedColumnName="id")
     * @var \Singwork\Model\Entities\User
     */
    protected $user;

    /**
     * 
     * @ManyToOne(targetEntity="Singwork\Model\Entities\Shop\DeliveryType")
     * @JoinColumn(name="delivery_type_id", referencedColumnName="id")
     * @var \Singwork\Model\Entities\Shop\DeliveryType
     */
    protected $delivery;

    /**
     * 
     * @ManyToOne(targetEntity="Singwork\Model\Entities\Shop\PaymentType")
     * @JoinColumn(name="payment_type_id", referencedColumnName="id")
     * @var \Singwork\Model\Entities\Shop\PaymentType
     */
    protected $payment;

    /**
     * @OneToOne(targetEntity="Order", mappedBy="cart", cascade={"persist", "remove"})
     * 
     */
    protected $order;

    /**
     * @OneToMany(targetEntity="CartProduct", mappedBy="cart", cascade={"persist", "remove"})
     * @var ArrayCollection|CartProduct[]
     */
    protected $products;

    /**
     * @OneToOne(targetEntity="CartShippingAddress", mappedBy="cart", cascade={"persist", "remove"})
     * @var CartShippingAddress
     */
    protected $shippingAddress;

    /**
     * @OneToOne(targetEntity="CartInvoiceAddress", mappedBy="cart", cascade={"persist", "remove"})
     * @var CartInvoiceAddress
     * 
     */
    protected $invoiceAddress;

    /**
     * @OneToMany(targetEntity="CartLog", mappedBy="cart", cascade={"persist", "remove"})
     * @var ArrayCollection
     */
    protected $logs;

    public function __construct(\Singwork\Model\Entities\User $user)
    {
        $this->createdTime = new \DateTime('now');
        $this->state = 'open';
        $this->user = $user;
        $this->products = [];
        $this->logs = [];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    public function getClosedTime()
    {
        return $this->closedTime;
    }

    public function getState()
    {
        return $this->state;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getCompanyName()
    {
        return $this->companyName;
    }

    public function getCompanyId()
    {
        return $this->companyId;
    }

    public function getCompanyTaxId()
    {
        return $this->companyTaxId;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getDelivery()
    {
        return $this->delivery;
    }

    public function getPayment()
    {
        return $this->payment;
    }

    public function getOrder()
    {
        return $this->order;
    }

    /**
     * 
     * @return ArrayCollection|CartProduct[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    public function getInvoiceAddress()
    {
        return $this->invoiceAddress;
    }

    public function getLogs()
    {
        return $this->logs;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setCreatedTime(\DateTime $createdTime)
    {
        $this->createdTime = $createdTime;
        return $this;
    }

    public function setClosedTime(\DateTime $closedTime)
    {
        $this->closedTime = $closedTime;
        return $this;
    }

    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
        return $this;
    }

    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;
        return $this;
    }

    public function setCompanyTaxId($companyTaxId)
    {
        $this->companyTaxId = $companyTaxId;
        return $this;
    }

    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    public function setUser(\Singwork\Model\Entities\User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function setDelivery(\Singwork\Model\Entities\Shop\DeliveryType $delivery)
    {
        $this->delivery = $delivery;
        return $this;
    }

    public function setPayment(\Singwork\Model\Entities\Shop\PaymentType $payment)
    {
        $this->payment = $payment;
        return $this;
    }

    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    public function setShippingAddress(CartShippingAddress $shippingAddress = null)
    {
        $this->shippingAddress = $shippingAddress;
        return $this;
    }

    public function setInvoiceAddress(CartInvoiceAddress $invoiceAddress = null)
    {
        $this->invoiceAddress = $invoiceAddress;
        return $this;
    }

    public function getProductPrice()
    {
        $price = 0;
        return $price;
    }

    public function getShippingPrice()
    {
        return ($this->delivery === null ? 0 : $this->delivery->getPrice()) + ($this->payment === null ? 0 : $this->payment->getPrice());
    }

    public function getTotalPrice()
    {
        return 0;
    }

    public function addProduct(CartProduct $product)
    {
        $this->products->add($product);
        $product->setCart($this);
    }

    public function removeProduct(CartProduct $product)
    {
        $this->products->removeElement($product);
        $product->setCart(null);
    }

    //TODO filter by product id
    public function getProductByTemplate($product)
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq("product", $product));
        return $this->products->matching($criteria)->first();
    }

    public function getProductCount()
    {
        $count = 0;
        return $count;
    }

    protected function convertProductsToArray()
    {
        return $this->products;
    }

    public function getJsonData()
    {
        return [
            'item_count' => $this->getProductCount(),
            'product_price' => $this->getProductPrice(),
            'total_price' => $this->getTotalPrice(),
            'products' => $this->convertProductsToArray()
        ];
    }

    

    const OPEN = "open";
    const CLOSED = "closed";
    const ABANDONED = "abandoned";

}
