<?php

/**
 * 
 * @return Core
 */
function core() {
    return Singwork\Core::getInstance();
}

function slog($message, $level = 'ERROR') {
    core()->getLogger()->log($message, $level);
}

function slog_fata($message) {
    slog($message, 'FATAL');
}

function slog_error($message) {
    slog($message, 'ERROR');
}

function slog_warn($message) {
    slog($message, 'WARNING');
}

function slog_info($message) {
    slog($message, 'INFO');
}

function slog_debug($message) {
    slog($message, 'DEBUG');
}

function slog_verbose($message) {
    slog($message, 'VERBOSE');
}

function t($word, $silent = false) {
    return core()->getController()->_t->getWord($word, $silent);
}

function config($query) {
    $exploded = explode('.', $query);
    return singwork_config_walk($_ENV['config'], $exploded);
}
function singwork_config_walk($current, $keys) {
    if (empty($keys)) {
        return $current;
    }
    $new = $current[array_shift($keys)];
    return singwork_config_walk($new, $keys);
}
